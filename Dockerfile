FROM jboss/wildfly:8.2.1.Final
LABEL maintainer="anders.harrisson@esss.se"

USER root
RUN groupmod -g 500 jboss && \
    usermod -u 500 -g jboss jboss && \
    chown jboss:jboss ${JBOSS_HOME}
USER jboss

# PostgreSQL jdbc driver module
ENV DATABASE_DRIVER_VERSION 42.2.0
ENV DATABASE_DRIVER postgresql-${DATABASE_DRIVER_VERSION}.jar
ADD --chown=jboss:jboss https://artifactory.esss.lu.se/artifactory/repo1/org/postgresql/postgresql/${DATABASE_DRIVER_VERSION}/${DATABASE_DRIVER} /opt/jboss/wildfly/standalone/deployments/

# GELF logging module
ADD --chown=jboss:jboss https://artifactory.esss.lu.se/artifactory/repo1/biz/paluch/logging/logstash-gelf/1.12.0/logstash-gelf-1.12.0-logging-module.zip /tmp/gelf-logging-module.zip
RUN unzip -q /tmp/gelf-logging-module.zip -d /tmp/ && mv /tmp/logstash-gelf-*/* /opt/jboss/wildfly/modules/ && rmdir /tmp/logstash-gelf-* && rm /tmp/gelf-logging-module.zip

# Set Environment variables used in JAVA_OPTS
ENV JBOSS_MODULES_SYSTEM_PKGS=org.jboss.byteman \
    JAVA_XMS=1024m \
    JAVA_XMX=2048m \
    JAVA_METASPACE=96M \
    JAVA_MAX_METASPACE=256m
ENV JAVA_OPTS="-server -Xms${JAVA_XMS} -Xmx${JAVA_XMX} -XX:MetaspaceSize=${JAVA_METASPACE} -XX:MaxMetaspaceSize=${JAVA_MAX_METASPACE} -XX:+UseG1GC -XX:+UseStringDeduplication -XX:MaxGCPauseMillis=200 -XX:ParallelGCThreads=20 -XX:ConcGCThreads=5 -Djava.net.preferIPv4Stack=true -Djboss.modules.system.pkgs=${JBOSS_MODULES_SYSTEM_PKGS} -Djava.awt.headless=true"

# Standalone configuration
COPY --chown=jboss:jboss standalone.xml /opt/jboss/wildfly/standalone/configuration/

# Deployment unit
COPY target/iocfactory-*.war /opt/jboss/wildfly/standalone/deployments/
