/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package db.migration;

import org.flywaydb.core.api.migration.jdbc.BaseJdbcMigration;
import org.flywaydb.core.api.migration.jdbc.JdbcMigration;
import se.esss.ics.iocfactory.model.VersionConverters;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;
import java.util.TreeMap;

public class V2__Named_Module_Versions extends BaseJdbcMigration
        implements JdbcMigration {
    @Override
    public void migrate(final Connection conn) throws Exception {
        migrateDeviceConfiguration(conn);
        migrateConfiguration(conn);
    }

    private static void migrateDeviceConfiguration(final Connection conn) throws SQLException {
        final Map<Long, Long> moduleVersions = fetchModuleVersions(conn);
        alterDeviceConfiguration(conn);

        final Statement stmt = conn.createStatement();
        for (Map.Entry<Long, Long> entry : moduleVersions.entrySet()) {
            stmt.addBatch(
                    String.format("UPDATE device_configuration SET module_version = '%s' WHERE id = %d",
                            VersionConverters.moduleVersionToString(entry.getValue()),
                            entry.getKey())
            );
        }
        stmt.executeBatch();
        stmt.close();
    }

    private static void migrateConfiguration(final Connection conn) throws SQLException {
        final Map<Long, Long> envVersions = fetchEnvVersions(conn);
        alterConfiguration(conn);

        final Statement stmt = conn.createStatement();
        for (Map.Entry<Long, Long> entry : envVersions.entrySet()) {
            stmt.addBatch(
                    String.format("UPDATE configuration SET env_version = '%s' WHERE id = %d",
                            VersionConverters.moduleVersionToString(entry.getValue()),
                            entry.getKey())
            );
        }
        stmt.executeBatch();
        stmt.close();
    }

    private static void alterDeviceConfiguration(final Connection conn) throws SQLException {
        final Statement stmt = conn.createStatement();
        stmt.execute("ALTER TABLE device_configuration DROP COLUMN module_version");
        stmt.execute("ALTER TABLE device_configuration ADD COLUMN module_version VARCHAR(255)");
        stmt.close();
    }
    private static void alterConfiguration(final Connection conn) throws SQLException {
        final Statement stmt = conn.createStatement();
        stmt.execute("ALTER TABLE configuration DROP COLUMN env_version");
        stmt.execute("ALTER TABLE configuration ADD COLUMN env_version VARCHAR(255)");
        stmt.close();
    }

    private static Map<Long, Long> fetchModuleVersions(final Connection conn) throws SQLException {
        final Map<Long, Long> moduleVersions = new TreeMap<>();
        final Statement stmt = conn.createStatement();
        final ResultSet rs = stmt.executeQuery("SELECT id, module_version FROM device_configuration");

        while(rs.next()) {
            moduleVersions.put(rs.getLong("id"), rs.getLong("module_version"));
        }
        rs.close();
        return moduleVersions;
    }

    private static Map<Long,Long> fetchEnvVersions(final Connection conn) throws SQLException {
        final Map<Long, Long> envVersions = new TreeMap<>();
        final Statement stmt = conn.createStatement();
        final ResultSet rs = stmt.executeQuery("SELECT id, env_version FROM configuration");

        while(rs.next()) {
            envVersions.put(rs.getLong("id"), rs.getLong("env_version"));
        }
        rs.close();
        return envVersions;
    }
}
