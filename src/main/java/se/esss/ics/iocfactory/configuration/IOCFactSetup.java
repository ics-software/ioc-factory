/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.configuration;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;


/**
 * Class that holds the IOCFactory setup configuration.
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 *
 */
@Entity
@Table(name = "iocfact_setup")
public class IOCFactSetup implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "epics_base_directory")
    private String epicsBaseDirectory;

    @Column(name = "sandbox_base_directory")
    private String sandboxBaseDirectory;

    @Column(name = "epicse3_base_directory")
    private String epicsE3BaseDirectory;

    @Column(name = "ioc_device_types")
    private String iocDeviceTypes;

    @Column(name = "ccdb_dependency_propery")
    private String ccdbDependencyPropery;

    @Column(name = "ccdb_os_property")
    private String ccdbOsProperty;

    @Column(name = "ccdb_snippet_property")
    private String ccdbSnippetProperty;

    @Column(name = "ccdb_hostname_property")
    private String ccdbHostnameProperty;

    @Column(name = "ccdb_macro_prefix_property")
    private String ccdbMacroPrefixProperty;

    @Version
    @Column(name = "optimistic_version")
    private Long optimisticVersion;

    public Long getId() { return id; }

    public String getEpicsBaseDirectory() { return epicsBaseDirectory; }
    public void setEpicsBaseDirectory(String epicsBaseDirectory) { this.epicsBaseDirectory = epicsBaseDirectory; }

    public String getSandboxBaseDirectory() { return sandboxBaseDirectory; }
    public void setSandboxBaseDirectory(String sandboxBaseDirectory) {
        this.sandboxBaseDirectory = sandboxBaseDirectory;
    }

    public String getEpicsE3BaseDirectory() { return epicsE3BaseDirectory; }
    public void setEpicsE3BaseDirectory(String epicsBaseDirectory) { this.epicsE3BaseDirectory = epicsBaseDirectory; }

    public String getIocDeviceTypes() { return iocDeviceTypes; }
    public void setIocDeviceTypes(String iocDeviceTypes) { this.iocDeviceTypes = iocDeviceTypes; }

    public String getCcdbDependencyPropery() { return ccdbDependencyPropery; }
    public void setCcdbDependencyPropery(String ccdbDependencyPropery) {
        this.ccdbDependencyPropery = ccdbDependencyPropery;
    }

    public String getCcdbOsProperty() { return ccdbOsProperty; }
    public void setCcdbOsProperty(String ccdbOsPropery) { this.ccdbOsProperty = ccdbOsPropery; }

    public String getCcdbSnippetProperty() { return ccdbSnippetProperty; }
    public void setCcdbSnippetProperty(String ccdbSnippetProperty) { this.ccdbSnippetProperty = ccdbSnippetProperty; }

    public String getCcdbHostnameProperty() { return ccdbHostnameProperty; }
    public void setCcdbHostnameProperty(String ccdbHostnameProperty) {
        this.ccdbHostnameProperty = ccdbHostnameProperty;
    }

    public String getCcdbMacroPrefixProperty() {
        return ccdbMacroPrefixProperty;
    }

    public void setCcdbMacroPrefixProperty(String ccdbMacroPrefixProperty) {
        this.ccdbMacroPrefixProperty = ccdbMacroPrefixProperty;
    }
}
