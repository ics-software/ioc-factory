/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package se.esss.ics.iocfactory.dao;

import se.esss.ics.iocfactory.model.OSAlias;
import se.esss.ics.iocfactory.model.dto.OSAliasDto;

import java.util.List;

/**
 * @author Imre Toth <imre.toth@ess.eu>
 **/
public interface OSAliasDao {

    List<OSAlias> getAllOSAlias();
    List<OSAlias> searchForEEEOSName(String e3Name);
    List<OSAlias> searchForE3OSName(String eeeName);

    OSAlias createAlias(String e3OSName, String eeeOSName);
    void updateAlias(OSAliasDto dto);
    void deleteAlias(OSAlias alias);
    OSAlias searchById(Long aliasId);
}
