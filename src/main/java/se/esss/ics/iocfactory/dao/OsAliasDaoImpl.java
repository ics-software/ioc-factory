/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package se.esss.ics.iocfactory.dao;

import se.esss.ics.iocfactory.model.OSAlias;
import se.esss.ics.iocfactory.model.dto.OSAliasDto;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * @author Imre Toth <imre.toth@ess.eu>
 **/
@Stateless
public class OsAliasDaoImpl implements OSAliasDao {

    @PersistenceContext
    private EntityManager em;

    /**
     * Queries all OS alias entries from DB table
     * @return List of all OS aliases
     */
    @Override
    public List<OSAlias> getAllOSAlias() {

        return em.createQuery(
                "SELECT os FROM OSAlias AS os")
                .getResultList();
    }

    /**
     * Queries for aliases with a specific e3 OS name
     * @param e3Name the e3 OS name that has to be looked up for
     * @return list of OS aliases that has specific e3 OS name
     */
    @Override
    public List<OSAlias> searchForEEEOSName(String e3Name) {

        return em.createQuery(
                "SELECT os FROM OSAlias AS os WHERE os.E3OSName = :pE3Name")
                .setParameter("pE3Name", e3Name)
                .getResultList();
    }

    /**
     * Queries for aliases with a specific eee OS name
     * @param eeeName the eee OS name that has to be looked up for
     * @return list of OS aliases that has specific eee OS name
     */
    @Override
    public List<OSAlias> searchForE3OSName(String eeeName) {

        return em.createQuery(
                "SELECT os FROM OSAlias AS os WHERE os.EEEOSName = :pEEEName")
                .setParameter("pEEEName", eeeName)
                .getResultList();
    }

    /**
     * Creates and stores an OSAlias entity in DB with given parameters
     * @param eeeOSName the eee OS alias name
     * @param e3OSName the e3 OS alias name
     * @return the OSAlias entity if storing was success
     */
    @Override
    public OSAlias createAlias(String eeeOSName, String e3OSName) {
        OSAlias result = new OSAlias(eeeOSName, e3OSName);

        em.persist(result);

        return result;
    }

    /**
     * Updates an OS Alias entity in DB with the given parameters
     * @param dto contains the ID of entity in DB, the (new)EEE, and (new)E3 OS alias name
     */
    @Override
    public void updateAlias(OSAliasDto dto) {
        em.createQuery("UPDATE OSAlias SET EEEOSName = :pEEEOSName, E3OSName = :pE3OsName WHERE id = :pId")
                .setParameter("pEEEOSName", dto.getEeeOsName())
                .setParameter("pE3OsName", dto.getE3OsName())
                .setParameter("pId", dto.getId())
                .executeUpdate();
    }

    /**
     * Deletes an OS Alias entity from DB
     * @param alias the OS alias entity that has to be deleted from DB
     */
    @Override
    public void deleteAlias(OSAlias alias) {
        em.remove(alias);
    }

    /**
     * Searches for OS alias entity in DB with specific ID
     * @param aliasId the ID of the searched Alias entity in the DB
     * @return NULL, or searched OSAlias entity
     */
    @Override
    public OSAlias searchById(Long aliasId) {

        return em.find(OSAlias.class, aliasId);
    }
}
