/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.model;

/**
 * Interface used for enums and messages representing consistency status for various entities
 *
 * @author <a href=mailto:miroslav.pavleski@cosylab.com>Miroslav Pavleski</a>
 *
 */
public class ConsistencyStatus {
    protected ConsistencyStatusType type;
    protected String desc;
    protected ConsistencyMsgLevel level;

    public ConsistencyStatus(ConsistencyStatusType type) {
        this.type = type;
        this.desc = type.getDesc();
        this.level = type.getLevel();
    }
    
    public ConsistencyStatus(ConsistencyStatusType type, String desc) {
        this.type = type;
        this.desc = desc;
        this.level = type.getLevel();
    }

    public ConsistencyStatus(String desc, ConsistencyMsgLevel level) {
        this.type = null;
        this.desc = desc;
        this.level = level;
    }

    /**
     * Type of this {@link ConsistencyStatus} item.
     * 
     * @return the type of the status item or null if the item has no type (for example, messages are such items). 
     */
    public ConsistencyStatusType getType() {
       return type; 
    }
    
    /**
     * User friendly description of this {@link ConsistencyStatus} item
     * 
     * @return description
     */
    public String getDesc() {
        return desc;
    }

    /**
     * Level of this {@link ConsistencyStatus} item
     * 
     * @return consistencyMsgLevel
     */
    public ConsistencyMsgLevel getLevel() {
        return level;
    }
}
