/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * A set that has only two operations allowed, add and clear
 *
 * Maintains whether there has been an error consistency status added
 *
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
public class ConsistencyStatusSet implements Serializable {
    private final Set<ConsistencyStatus> csSet = new HashSet<>();
    private boolean error = false;

    /**
     * Add consistency status type to the set. See {@link Set#add(java.lang.Object)}
     * 
     * @param valType
     */
    public void add(ConsistencyStatusType valType) {
        csSet.add(new ConsistencyStatus(valType));
        if (!error) {
            error = valType.getLevel() == ConsistencyMsgLevel.ERROR;
        }
    }
    
    /**
     * Add element to the set see {@link Set#add(java.lang.Object)}
     *
     * @param val
     */
    public void add(ConsistencyStatus val) {
        csSet.add(val);
        if (!error) {
            error = val.getLevel() == ConsistencyMsgLevel.ERROR;
        }
    }

    /**
     * see {@link Set#clear()}
     */
    public void clear() {
        csSet.clear();
        error = false;
    }

    /**
     * Returns true if any of the elements of the set has {@link ConsistencyMsgLevel#ERROR} level
     * 
     * @return true if error, else false
     */
    public boolean isError() {
        return error;
    }

    /**
     * see {@link Set#size()}
     * 
     * @return size
     */
    public int size() {
        return csSet.size();
    }

    /**
     * see {@link Set#isEmpty()}
     * 
     * @return ture if empty, else false
     */
    public boolean isEmpty() {
        return csSet.isEmpty();
    }

    /**
     * see {@link Set#contains(java.lang.Object)}
     * 
     * @param csType consistency status type to check
     * @return true if set contains consistency status type, else false
     */
    public boolean contains(ConsistencyStatusType csType) {
        return csSet.stream().anyMatch(cs -> Objects.equals(cs.getType(), csType));
    }

    /**
     * see {@link Set#containsAll(java.util.Collection)}
     * 
     * @param csTypes consistency status types to check
     * @return true if set contains all consistency status types, else false
     */
    public boolean containsAll(Collection<ConsistencyStatusType> csTypes) {
        return csSet.stream().map(cs -> cs.getType()).collect(Collectors.toSet()).containsAll(csTypes);
    }

    /**
     * see {@link Set#stream()}
     * 
     * @return stream of the set
     */
    public Stream<ConsistencyStatus> stream() {
        return csSet.stream();
    }

    /**
     * see {@link Set#forEach(java.util.function.Consumer)}
     * 
     * @param action
     */
    public void forEach(Consumer<? super ConsistencyStatus> action) {
        csSet.forEach(action);
    }
}
