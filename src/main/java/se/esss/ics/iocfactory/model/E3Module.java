package se.esss.ics.iocfactory.model;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
@JsonPropertyOrder({"name", "moduleVersion", "epicsVersion", "requireVersion", "os", "dependencies"})
public class E3Module extends Module {
    /**
     * Require version for dummy {@link E3Module} objects.
     */
    public static final String EMPTY_REQUIRE_VERSION = "0.0.0";

    private final String requireVersion;

    /**
     * Constructs an undefined module, used for displaying a module on UI for devices with invalid modules
     */
    public E3Module() {
        this(null);
    }

    /**
     * Constructs non-existing E3 module, used for displaying a module on UI for devices with invalid modules
     *
     * @param name
     */
    public E3Module(String name) {
        super(name);
        this.requireVersion = EMPTY_REQUIRE_VERSION;
    }


    /**
     * Construct a new {@link E3Module} value object
     *
     * @param name           name of the module
     * @param version        version of the module
     * @param epicsVersion   EPICS base version of the module
     * @param requireVersion EPICS E3 base require version of the module
     * @param os             operating system
     * @param dependencies   a list of immediate dependencies
     * @param snippets       a list of snippets
     */
    public E3Module(String name, String version, Long epicsVersion, String requireVersion, String os,
                    List<ModuleDependency> dependencies, List<Snippet> snippets) {
        super(name, version, epicsVersion, os, dependencies, snippets);
        this.requireVersion = requireVersion;
    }

    /**
     * Construct a new {@link E3Module} value object from a {@link Module} object
     *
     * @param module {@link Module} object
     * @param requireVersion EPICS E3 base require version of the module
     */
    public E3Module(Module module, String requireVersion) {
        super(module.getName(), module.getModuleVersion(), module.getEpicsVersion(), module.getOs(),
                module.getDependencies(), module.getSnippets());
        this.requireVersion = requireVersion;
    }

    public String getRequireVersion() {
        return requireVersion;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), this.requireVersion);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        if (!super.equals(obj)) return false;

        final E3Module other = (E3Module) obj;
        return Objects.equals(this.requireVersion, other.requireVersion);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(String.format("Module: %s Version: %s%n"
                        + "\tBase: %s%n"
                        + "\tRequire version: %s%n"
                        + "\tOS: %s%n"
                        + "\tDependencies:%n", super.getName(),
                super.getModuleVersion(),
                VersionConverters.getEpicsConverter().toString(super.getEpicsVersion()),
                requireVersion,
                super.getOs()));
        super.getDependencies().forEach(dep -> sb.append(String.format("\t\t%s%n", dep.toString())));

        sb.append(String.format("\tSnippets:%n"));
        super.getSnippets().forEach(snippet ->
                sb.append(String.format("\t\t%s - %s", snippet.toString(),
                        snippet.getPlaceholders().stream().map(Object::toString).collect(Collectors.joining(", "))))
        );

        return sb.toString();
    }
}
