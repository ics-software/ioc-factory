package se.esss.ics.iocfactory.model;

import com.google.common.base.Objects;
import org.apache.commons.lang3.StringUtils;
import se.esss.ics.iocfactory.service.OSAliasService;
import se.esss.ics.iocfactory.util.ModuleVersionsComparator;

import javax.inject.Named;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
@Named
public class E3Repository extends Repository {
    private static final Logger LOG = Logger.getLogger(E3Repository.class.getName());

    protected Map<Long, Set<String>> baseRequireMap = new HashMap<>();
    /**
     * Adds an E3 module to the repository
     *
     * @param module
     *            the {@link E3Module} to be added
     */
    public void addModule(E3Module module) {
        final Long epicsVersion = module.getEpicsVersion();

        if(!baseRequireMap.containsKey(epicsVersion)) {
            baseRequireMap.put(epicsVersion, new HashSet<>());
        }
        baseRequireMap.get(epicsVersion).add(module.getRequireVersion());
        // Encountered a new OS, add it to the set of known OSes
        osVersions.add(module.getOs());

        moduleNames.add(module.getName());

        // Add to the modules map
        addModuleToModulesByName(module);
    }

    /**
     * Given a specification of a module in terms of name, OS, EPICS base version and require version, return a {@link List} for all
     * conforming modules
     *
     * @param moduleName
     *            specification for a module name
     * @param os
     *            specification for a module Operating System
     * @param epicsVersion
     *            specification for a module EPICS base version
     * @param requireVersion
     *            specification for a module EPICS E3 require version
     * @return a list of modules matching the specification
     */
    public List<E3Module> getModulesForSpec(final String moduleName, final String os, final Long epicsVersion,
                                          final String requireVersion) {
        return filterModulesByNameOsEpicsRequireVersion(moduleName, os, epicsVersion, requireVersion).
                collect(Collectors.toList());
    }

    private Stream<E3Module> filterModulesByNameOsEpicsRequireVersion(final String moduleName, final String os,
                                                             final Long epicsVersion, final String requireVersion) {
        if (moduleName == null) {
            return Stream.empty();
        }

        List<Module> modulesForName = modulesByName.get(moduleName);
        if (modulesForName == null) {
            return Stream.empty();
        }

        final List<E3Module> moduleList = modulesForName.stream()
                .map(module -> (E3Module)module)
                .collect(Collectors.toList());
        LOG.fine(String.format("filterModulesByNameOsEpicsVersion moduleList for spec %s, %s, %s is %s", moduleName,
                os, VersionConverters.epicsVersionToString(epicsVersion),
                moduleList.stream()
                        .map(module -> String.format("Name: %s, OS: %s, EPICS: %s, Requires version: %s Version: %s", module.getName(),
                                module.getOs(), VersionConverters.epicsVersionToString(module.getEpicsVersion()),
                                module.getRequireVersion(),
                                module.getModuleVersion()))
                        .collect(Collectors.joining("; "))));
        return moduleList.stream().filter(
                mod -> StringUtils.equals(mod.getOs(), os)
                        && Objects.equal(mod.getEpicsVersion(), epicsVersion)
        && Objects.equal(mod.getRequireVersion(), requireVersion));
    }

    @Override
    public Optional<Module> getLatestModuleForSpec(final String moduleName, final String os, final Long epicsVersion,
                                                   final ModuleVersionsComparator.ModuleComparator comparator) {
        Optional<Module> module = super.getLatestModuleForSpec(moduleName, os, epicsVersion, comparator);
        if (!module.isPresent()) {
            OSAliasService osAliasService = osAliasServiceInstance.get();
            for (String e3Os : osAliasService.searchForE3OSName(os)) {
                Optional<Module> moduleByE3 = super.getLatestModuleForSpec(moduleName, e3Os, epicsVersion, comparator);
                if (moduleByE3.isPresent()) {
                    return moduleByE3;
                }
            }
        }
        return module;
    }

    public Optional<E3Module> getModuleFromRepo(E3Module module) {
        return getE3ModuleForSpecOsAliasing(module.getName(), module.getOs(), module.getEpicsVersion(), module.getRequireVersion(),
                module.getModuleVersion());
    }

    public Optional<E3Module> getE3ModuleForSpecOsAliasing(final String moduleName, final String os, final Long epicsVersion,
                                               final String requireVersion, final String moduleVersion) {
        Optional<E3Module> module = getE3ModuleForSpec(moduleName, os, epicsVersion, requireVersion, moduleVersion);
        if (!module.isPresent()) {
            OSAliasService osAliasService = osAliasServiceInstance.get();
            for (String e3Os : osAliasService.searchForE3OSName(os)) {
                Optional<E3Module> moduleByE3 = getE3ModuleForSpec(moduleName, e3Os, epicsVersion, requireVersion, moduleVersion);
                if (moduleByE3.isPresent()) {
                    return moduleByE3;
                }
            }
        }
        return module;
    }

    /**
     * Retrieves a scanned {@link Module} instance given an exact module specification in terms name, OS, EPICS version
     * and module version, if it exists
     *
     * @param moduleName
     *            specification for a module name
     * @param os
     *            specification for the module Operating System
     * @param epicsVersion
     *            specification for the module EPICS base version
     * @param requireVersion
     *            specification for a module EPICS E3 require version
     * @param moduleVersion
     *            specification or the module version
     * @return an {@link Optional} containing a scanned {@link Module} instance
     */
    public Optional<E3Module> getE3ModuleForSpec(final String moduleName, final String os, final Long epicsVersion,
                                               final String requireVersion, final String moduleVersion) {
        if (epicsVersion == null || moduleVersion == null || requireVersion == null || moduleName == null) {
            return Optional.empty();
        }

        List<E3Module> modules = filterModulesByNameOsEpicsRequireVersion(moduleName, os, epicsVersion, requireVersion).
                collect(Collectors.toList());
        if (Module.LATEST_MODULE_VERSION.equals(moduleVersion)) {
            if (modules.isEmpty()) {
                return Optional.empty();
            } else {
                return Optional.of(createLatest(moduleName, os, epicsVersion, requireVersion));
            }
        }

        return modules.stream().filter(
                mod -> ModuleVersionsComparator.compare(mod.getModuleVersion(), moduleVersion) == 0).findAny();
    }

    /**
     * Gives back all require versions belonging to an EPICS base version
     * @param epicsVersion specification for a EPICS base version
     * @return a {@link Set} of require versions
     */
    public Set<String> getBaseRequires(Long epicsVersion) {
        return baseRequireMap.getOrDefault(epicsVersion, Collections.emptySet());
    }

    private static E3Module createLatest(final String moduleName, final String os, final Long epicsVersion,
                                         final String  requireVersion) {
        return new E3Module(moduleName, Module.LATEST_MODULE_VERSION, epicsVersion, requireVersion, os,
                Collections.emptyList(), Collections.emptyList());
    }
}
