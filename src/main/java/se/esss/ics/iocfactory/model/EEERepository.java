package se.esss.ics.iocfactory.model;
import javax.inject.Named;
import java.util.logging.Logger;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
@Named
public class EEERepository extends Repository {
    private static final Logger LOG = Logger.getLogger(EEERepository.class.getName());

    /**
     * Adds an EEE module to the repository
     *
     * @param module
     *            the {@link Module} to be added
     */
    public void addModule(Module module) {
        final Long epicsVersion = module.getEpicsVersion();
        if (!epicsVersions.contains(epicsVersion)) {
            messages.add(new RepositoryMessage(ConsistencyMsgLevel.ERROR,
                    "Unknown module EPICS version, such base does not exist in repo.", ""));
            return;
        }

        // Encountered a new OS, add it to the set of known OSes
        osVersions.add(module.getOs());
        moduleNames.add(module.getName());

        // Add to the modules map
        addModuleToModulesByName(module);
    }
}
