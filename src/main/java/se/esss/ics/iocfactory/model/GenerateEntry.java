/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * Entity bean that represents an event of generation of IOC onto a environment
 *
 * By design is in denormalized form (without foreign key links to other entities).
 *
 * Rationale: The referenced entities might get removed, we still want to keep the audit trail.
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
@Entity
@Table(name = "generate_entry", indexes = { @Index(columnList = "config_id") })
@SuppressWarnings({
    // Methods should not have too many parameters
    "squid:S00107"
})
public class GenerateEntry implements Serializable, HasConsistencyStatus {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Basic
    @Column(name = "ioc_name")
    private String iocName;

    @Basic
    @Column(name = "os")
    private String os;

    @Basic
    @Column(name = "hostname")
    private String hostname;

    @Basic
    @Column(name = "config_id")
    private Long configId;

    @Basic
    @Column(name = "config_rev")
    private Integer configRevision;

    @Basic
    @Column(name = "is_latest_revision")
    private Boolean isLatestRevision;

    @Basic
    @Column(name = "gen_user")
    private String user;

    @Basic
    @Column(name = "gen_timestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date generatedDate;

    @Basic
    @Column(name = "environment")
    private String environment;

    @Basic
    @Column(name = "production")
    private Boolean production = false;

    @Basic
    @Column(name = "target_path")
    private String targetPath;

    @Basic
    @Column(name = "gen_hash", length = 40)
    private String generatedHash;
    
    @Column(name = "dump", columnDefinition = "TEXT")
    private String dump;
    
    @Basic
    @Column(name = "description", columnDefinition = "TEXT")
    private String desc;

    @Transient
    private ConsistencyStatusSet consistencyStatus = new ConsistencyStatusSet();

    protected GenerateEntry() { }

    /**
     * Constructs a new {@link GenerateEntry} populating all fields using this constructor
     *
     * @param iocName
     * @param os
     * @param hostname
     * @param configId
     * @param configRevision
     * @param user
     * @param generatedDate
     * @param environment
     * @param production
     * @param targetPath
     * @param generatedHash
     * @param dump
     */
    public GenerateEntry(String iocName, String os, String hostname, Long configId, Integer configRevision,
            String user, Date generatedDate, String environment, Boolean production,
            String targetPath, String generatedHash, String dump, String desc) {
        this.iocName = iocName;
        this.os = os;
        this.hostname = hostname;
        this.configId = configId;
        this.configRevision = configRevision;
        this.isLatestRevision = false;
        this.user = user;
        this.generatedDate = generatedDate != null ? new Date(generatedDate.getTime()) : null;
        this.environment = environment;
        this.production = production;
        this.targetPath = targetPath;
        this.generatedHash = generatedHash;
        this.dump = dump;
        this.desc = desc;
    }

    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }

    public String getIocName() { return iocName; }
    public void setIocName(String iocName) { this.iocName = iocName; }

    public String getOs() { return os; }
    public void setOs(String os) { this.os = os; }

    public String getHostname() { return hostname; }
    public void setHostname(String hostname) { this.hostname = hostname; }

    public Long getConfigId() { return configId; }
    public void setConfigId(Long configId) { this.configId = configId; }

    public Integer getConfigRevision() { return configRevision; }
    public void setConfigRevision(Integer configRevision) { this.configRevision = configRevision; }

    public Boolean getIsLatestRevision() { return isLatestRevision; }
    public void setIsLatestRevision(Boolean isLatestRevision) { this.isLatestRevision = isLatestRevision; }

    public String getUser() { return user; }
    public void setUser(String user) { this.user = user; }

    public Date getGeneratedDate() { return generatedDate != null ? new Date(generatedDate.getTime()) : null; }
    public void setGeneratedDate(Date generatedDate) {
        this.generatedDate = generatedDate != null ? new Date(generatedDate.getTime()) : null;
    }

    public String getEnvironment() { return environment; }
    public void setEnvironment(String environment) { this.environment = environment; }

    public Boolean getProduction() { return production; }
    public void setProduction(Boolean production) { this.production = production; }

    public String getTargetPath() { return targetPath; }
    public void setTargetPath(String targetPath) { this.targetPath = targetPath; }

    public String getGeneratedHash() { return generatedHash; }
    public void setGeneratedHash(String generatedHash) { this.generatedHash = generatedHash; }

    public String getDesc() { return desc; }
    public void setDesc(String desc) { this.desc = desc; }

    public String getDump() { return dump; } 
    public void setDump(String dump) { this.dump = dump; }
    
    @Override
    public ConsistencyStatusSet getConsistencyStatus() { return consistencyStatus; }
}
