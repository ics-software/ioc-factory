/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.model;

import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import se.esss.ics.iocfactory.service.GenerateEntryService;
import se.esss.ics.iocfactory.service.GenerateEntryConsistencyStatus;
import se.esss.ics.iocfactory.service.GenerateEntryFilterValues;
import se.esss.ics.iocfactory.service.GenerateEntrySortColumns;
import se.esss.ics.iocfactory.ui.ConsistencyStatusChecker;
import se.esss.ics.iocfactory.util.LazyLoadUtil;

/**
 *
 * Lazy data model for GenerateEntry entries -- lazy connection with Primefaces.
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
@Named
public class GenerateEntryLazyDataModel extends LazyDataModel<GenerateEntry> {

    private static final long serialVersionUID = 2465733903992953339L;
    private static final Logger LOG = Logger.getLogger(GenerateEntryLazyDataModel.class.getName());

    private static final String CONFIG_REVISION = "configRevision";
    private static final String IS_LATEST_REVISION = "isLatestRevision";
    private static final String PRODUCTION = "production";

    @Inject
    private transient GenerateEntryService generateEntryService;

    @Inject
    private transient ConsistencyStatusChecker csChecker;

    @Override
    public GenerateEntry getRowData(String rowkey) {
        return generateEntryService.getIocEntryById(Long.parseLong(rowkey));
    }

    @Override
    public Object getRowKey(GenerateEntry entry) {
        return entry.getId();
    }

    /***
     * Main lazy loading method.
     * RowCount property must be set to tell datatables count of all rows in a table.
     * Returns limited list of rows.
     */
    @Override
    public List<GenerateEntry> load(
            int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {

        final GenerateEntryFilterValues filterVal = handleFiltration(filters);
        final Long allRowsCount = generateEntryService.getIocEntryCount(filterVal);
        this.setRowCount(allRowsCount.intValue());
        final List<GenerateEntry> entries =
                generateEntryService.getIocEntries(
                        first,
                        pageSize,
                        handleSortValues(sortField),
                        LazyLoadUtil.castFromPfSortOrder(sortOrder),
                        filterVal);
        // could be changed to handle differences on services layer.
        entries.forEach(ge -> csChecker.handleConfigDiffersOrDeleted(ge));
        LOG.fine(
                String.format(
                        "All rows count: %s, page size: %s, fetch entites: %s",
                        allRowsCount,
                        pageSize,
                        entries.size()));
        return entries;
    }

    /***
     * Casts sort columns to GenerateEntrySortColumns type
     *
     * @param sortField PF lazydatamodel column for ordering
     * @return GenerateEntrySortColumns column for ordering
     */

    private GenerateEntrySortColumns handleSortValues(String sortField) {
        if (sortField == null) {
            return null;
        }

        switch (sortField) {
        case "iocName":
            return GenerateEntrySortColumns.IOCNAME;
        case "os":
            return GenerateEntrySortColumns.OS;
        case "hostname":
            return GenerateEntrySortColumns.HOSTNAME;
        case "desc":
            return GenerateEntrySortColumns.DESC;
        case "user":
            return GenerateEntrySortColumns.USER;
        case "targetPath":
            return GenerateEntrySortColumns.TARGETPATH;
        case "isProduction":
        case PRODUCTION:
            return GenerateEntrySortColumns.PRODUCTION;
        case CONFIG_REVISION:
            return GenerateEntrySortColumns.CONFIGREVISION;
        case IS_LATEST_REVISION:
            return GenerateEntrySortColumns.ISLATESTREVISION;
        case "generatedDate":
            return GenerateEntrySortColumns.GENERATEDDATE;
        case "environment":
            return GenerateEntrySortColumns.ENVIROMENT;
        default:
            return null;
        }
    }

    /***
     * Maps Primefaces table column filter values to GenerateEntryFilterValues type.
     * This way table names are independent of underlying implementation.
     *
     * @param filters Map of filter name and value pair
     * @return wrapped filter values (nullable)
     */
    private GenerateEntryFilterValues handleFiltration(Map<String, Object> filters) {
        if (filters.size() < 1) {
            return null;
        }
        return new GenerateEntryFilterValues(
                // string
                LazyLoadUtil.getFilterValueToString("iocName", filters),
                LazyLoadUtil.getFilterValueToString("os", filters),
                LazyLoadUtil.getFilterValueToString("hostname", filters),
                LazyLoadUtil.getFilterValueToString("targetPath", filters),
                LazyLoadUtil.getFilterValueToString("desc", filters),
                LazyLoadUtil.getFilterValueToString("user", filters),
                LazyLoadUtil.getFilterValueToString("environment", filters),
                // int
                LazyLoadUtil.getFilterValueToString(CONFIG_REVISION, filters) == null ? null
                        : Integer.parseInt(LazyLoadUtil.getFilterValueToString(CONFIG_REVISION, filters)),
                // date
                LazyLoadUtil.getFilterValueToDate("generatedDate", filters),
                // boolean
                LazyLoadUtil.getFilterValueToString(PRODUCTION, filters) == null ? null
                        : Boolean.parseBoolean(LazyLoadUtil.getFilterValueToString(PRODUCTION, filters)),
                LazyLoadUtil.getFilterValueToString(IS_LATEST_REVISION, filters) == null ? null
                        : Boolean.parseBoolean(LazyLoadUtil.getFilterValueToString(IS_LATEST_REVISION, filters)),
                // eval
                LazyLoadUtil.getFilterValueToString("configDiff", filters) == null ? null
                        : GenerateEntryConsistencyStatus.valueOf(
                                LazyLoadUtil.getFilterValueToString("configDiff", filters)));
    }
}
