/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * A value object representing IOC Device as reported by the CCDB
 *
 * @author <a href=mailto:miroslav.pavleski@cosylab.com>Miroslav Pavleski</a>
 *
 */
public class IOCDevice implements Serializable {
    private final String nameId;
    private final String name;
    private final String type;
    private final String moduleName;
    private final List<String> snippets;
    private final Map<String, String> predefinedMacroValues;
    private final String desc;
    private final int numControlledBy;

    /**
     * IOC Device constructor
     *
     * @param nameId name id of the device
     * @param name name of the device
     * @param type the device type as reported by CCDB
     * @param moduleName the module name specified using an appropriate property
     * @param snippets  the list of snippets names using an appropriate property
     * @param desc the description of the device slot
     * @param numControlledBy Number of slots this device is controlled by
     */
    public IOCDevice(
            String nameId, String name, String type, String moduleName, List<String> snippets,
            Map<String, String> predefinedMacroValues, String desc, int numControlledBy) {
        this.nameId = nameId;
        this.name = name;
        this.type = type;
        this.moduleName = moduleName;
        this.snippets = snippets;
        this.predefinedMacroValues = predefinedMacroValues;
        this.desc = desc;
        this.numControlledBy = numControlledBy;
    }

    public String getNameId() { return nameId; }
    public String getName() { return name; }
    public String getType() { return type; }
    public String getModuleName() { return moduleName; }
    public List<String> getSnippets() { return snippets; }
    public Map<String, String> getPredefinedMacroValues() { return predefinedMacroValues; }
    public String getDesc() { return desc; }
    public int getNumControlledBy() { return numControlledBy; }

    @Override
    public int hashCode() {
        return nameId != null ?
                Objects.hashCode(this.nameId) :
                Objects.hashCode(this.name);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        return nameId != null && ((IOCDevice) obj).nameId != null ?
                Objects.equals(this.nameId, ((IOCDevice) obj).nameId) :
                Objects.equals(this.name, ((IOCDevice) obj).name);
    }


}
