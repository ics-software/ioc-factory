/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.model;

import java.io.Serializable;
import se.esss.ics.macrolib.MacroOccurrence;

/**
 * Represents an expanded macro value and additional data. An immutable value object.
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
public class MacroExpansion implements Serializable {
    private final String expandedValue;
    private final MacroOccurrence occurrence;
    private final String errorMsg;


    /**
     * Constructs a {@link MacroExpansion} by specifying all fields
     *
     * @param expandedValue string expanded value
     * @param occurrence reference to a {@link MacroOccurrence} that indicates where is the macro occurrence for this
     * macro expansion in the source snippet
     * @param errorMsg possible error message if expansion failed for any reason
     */
    public MacroExpansion(String expandedValue, MacroOccurrence occurrence, String errorMsg) {
        this.expandedValue = expandedValue;
        this.occurrence = occurrence;
        this.errorMsg = errorMsg;
    }

    /**
     * Constructs a {@link MacroExpansion} by specifying the expanded value and the occurrence reference.
     * <br/>
     * See {@link MacroExpansion#MacroExpansion(java.lang.String,
     * se.esss.ics.macrolib.MacroOccurrence, java.lang.String)}
     * @param expandedValue
     * @param occurrence
     */
    public MacroExpansion(String expandedValue, MacroOccurrence occurrence) { this(expandedValue, occurrence, null); }

    /**
     * Constructs a {@link MacroExpansion} by only specifying the expanded value streeng
     *
     * @param expandedValue
     */
    public MacroExpansion(String expandedValue) { this(expandedValue, null, null); }

    public String getExpandedValue() { return expandedValue; }
    public MacroOccurrence getOccurrence() { return occurrence; }
    public String getError() { return errorMsg; }
    public boolean isError() { return errorMsg != null; }
    public boolean isDefalutValue() { return occurrence != null; }
}
