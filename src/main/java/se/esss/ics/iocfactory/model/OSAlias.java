package se.esss.ics.iocfactory.model;

/**
 * @author Imre Toth <imre.toth@ess.eu>
 **/

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "os_alias")
public class OSAlias {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Basic
    @Column(name = "eee_os_name")
    private String EEEOSName;

    @Basic
    @Column(name = "e3_os_name")
    private String E3OSName;

    public OSAlias() {
    }

    public OSAlias(String eeeName, String e3Name) {
        this.E3OSName = e3Name;
        this.EEEOSName = eeeName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEEEOSName() {
        return EEEOSName;
    }

    public void setEEEOSName(String EEEOSName) {
        this.EEEOSName = EEEOSName;
    }

    public String getE3OSName() {
        return E3OSName;
    }

    public void setE3OSName(String e3OSName) {
        E3OSName = e3OSName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OSAlias osAlias = (OSAlias) o;
        return Objects.equals(id, osAlias.id) &&
                Objects.equals(EEEOSName, osAlias.EEEOSName) &&
                Objects.equals(E3OSName, osAlias.E3OSName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, EEEOSName, E3OSName);
    }
}
