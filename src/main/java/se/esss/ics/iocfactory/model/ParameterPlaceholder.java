/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.model;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import se.esss.ics.macrolib.MacroEntry;

/**
 * An immutable value class that represents a macro parameter placeholder as read from the repository.
 *
 * Contains the name, type, description and possible macro entry references
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
public class ParameterPlaceholder implements Serializable {
    private final String name;
    private final ParameterType type;
    private final String desc;
    private final Set<PlaceholderTrait> traits;
    private MacroEntry macroEntry = null;

    /**
     * Constructs a {@link ParameterPlaceholder} by specifying all fields
     *
     * @param name the name of the macro parameter
     * @param type the type of the macro parameter
     * @param desc the description of the macro parameter
     * @param traits traits of this placeholder
     */
    public ParameterPlaceholder(String name, ParameterType type, String desc, Set<PlaceholderTrait> traits) {
        this.name = name;
        this.type = type;
        this.desc = desc;
        excludeModuleDirs(name, traits);
        this.traits = traits;
    }

    public String getName() { return name; }
    public ParameterType getType() { return type; }
    public Set<PlaceholderTrait> getTraits() { return traits; }
    public String getDesc() { return desc; }
    
    public MacroEntry getMacroEntry() { return macroEntry; }
    public void setMacroEntry(MacroEntry macroEntry) { this.macroEntry = macroEntry; }

    @Override
    public String toString() {
        return String.format("%s:%s%s", name, type.toString(),
                StringUtils.isEmpty(desc) ? "" : " - " + desc);
    }

    @Override
    public int hashCode() { return Objects.hash(StringUtils.upperCase(name), type); }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ParameterPlaceholder other = (ParameterPlaceholder) obj;
        return StringUtils.equalsIgnoreCase(this.name, other.name) && this.type == other.type && 
                Objects.equals(this.traits, other.traits);
    }

    private void excludeModuleDirs(final String name, final Set<PlaceholderTrait> traits) {
        if (StringUtils.endsWith(name, "_DIR") && !traits.contains(PlaceholderTrait.NON_RUNTIME)) {
            traits.add(PlaceholderTrait.RUNTIME);
        }
    }
}
