/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package se.esss.ics.iocfactory.model.dto;

import se.esss.ics.iocfactory.model.OSAlias;

/**
 * @author Imre Toth <imre.toth@ess.eu>
 **/
public class OSAliasDto {

    private final Long id;
    private final String eeeOsName;
    private final String e3OsName;

    public OSAliasDto(Long id, String eeeOsName, String e3OsName) {
        this.id = id;
        this.eeeOsName = eeeOsName;
        this.e3OsName = e3OsName;
    }

    public OSAliasDto(OSAlias osAlias) {
        this.id = osAlias.getId();
        this.eeeOsName = osAlias.getEEEOSName();
        this.e3OsName = osAlias.getE3OSName();
    }

    public Long getId() {
        return id;
    }

    public String getEeeOsName() {
        return eeeOsName;
    }

    public String getE3OsName() {
        return e3OsName;
    }
}
