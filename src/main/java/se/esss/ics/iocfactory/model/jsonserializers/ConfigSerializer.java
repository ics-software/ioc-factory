/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.model.jsonserializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import java.io.IOException;
import se.esss.ics.iocfactory.model.Configuration;
import se.esss.ics.iocfactory.model.Parameter;


/**
 *
 * @author mpavleski
 */
public class ConfigSerializer extends JsonSerializer<Configuration> {

    private EpicsVersionSerializer epicsVerSerializer = new EpicsVersionSerializer();

    @Override
    public void serialize(Configuration config, JsonGenerator gen, SerializerProvider sp)
            throws IOException, JsonProcessingException {


        if (config != null && config.getIoc() != null) {
            gen.writeStartObject();

            gen.writeObjectField("ioc", config.getIoc().getName());
            gen.writeObjectField("revision", config.getRevision());
            gen.writeObjectField("os", config.getOs());
            gen.writeFieldName("epicsVersion");
            epicsVerSerializer.serialize(config.getEpicsVersion(), gen, sp);
            gen.writeObjectField("envVersion", config.getEnvVersion());
            gen.writeObjectField("procServPort", config.getProcServPort());

            gen.writeFieldName("environment");
            gen.writeStartObject();
            for (Parameter p : config.getGlobals()) {
                gen.writeObject(p);
            }
            gen.writeEndObject();

            gen.writeObjectField("devices", config.getDevices());

            gen.writeEndObject();
        }
    }
}
