/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.model.log;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import se.esss.ics.iocfactory.configuration.IOCEnvironment;
import se.esss.ics.iocfactory.configuration.IOCFactSetup;

/**
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
@JsonPropertyOrder({"epicsBaseDirectory", "sandboxBaseDirectory", "iocDeviceTypes", "ccdbDependencyPropery",
    "ccdbOsProperty", "ccdbSnippetProperty", "ccdbMacroPrefixProperty", "environments"})
public class SettingsSavedV1 implements Serializable {
    private final String epicsBaseDirectory;
    private final String sandboxBaseDirectory;
    private final String epicsE3BaseDirectory;
    private final String iocDeviceTypes;
    private final String ccdbDependencyPropery;
    private final String ccdbOsProperty;
    private final String ccdbSnippetProperty;
    private final String ccdbMacroPrefixProperty;

    private final List<SettingsSavedEnvV1> environments;

    public SettingsSavedV1(IOCFactSetup setup, List<IOCEnvironment> envs) {
        this.epicsBaseDirectory = setup.getEpicsBaseDirectory();
        this.sandboxBaseDirectory = setup.getSandboxBaseDirectory();
        this.epicsE3BaseDirectory = setup.getEpicsE3BaseDirectory();
        this.iocDeviceTypes = setup.getIocDeviceTypes();
        this.ccdbDependencyPropery = setup.getCcdbDependencyPropery();
        this.ccdbOsProperty = setup.getCcdbOsProperty();
        this.ccdbSnippetProperty = setup.getCcdbSnippetProperty();
        this.environments = envs.stream().map(env -> new SettingsSavedEnvV1(env)).collect(Collectors.toList());
        this.ccdbMacroPrefixProperty = setup.getCcdbMacroPrefixProperty();
    }

    public String getEpicsBaseDirectory() { return epicsBaseDirectory; }
    public String getSandboxBaseDirectory() { return sandboxBaseDirectory; }
    public String getEpicsE3BaseDirectory() { return epicsE3BaseDirectory; }
    public String getIocDeviceTypes() { return iocDeviceTypes; }
    public String getCcdbDependencyPropery() { return ccdbDependencyPropery; }
    public String getCcdbOsProperty() { return ccdbOsProperty; }
    public String getCcdbSnippetProperty() { return ccdbSnippetProperty; }
    public List<SettingsSavedEnvV1> getEnvironments() { return environments; }
    public String getCcdbMacroPrefixProperty() { return ccdbMacroPrefixProperty; }
}
