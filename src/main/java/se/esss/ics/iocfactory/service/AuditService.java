/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import com.google.common.base.Objects;

import se.esss.ics.iocfactory.configuration.IOCEnvironment;
import se.esss.ics.iocfactory.model.AuditEntry;
import se.esss.ics.iocfactory.model.Configuration;
import se.esss.ics.iocfactory.model.ConsistencyMsgLevel;
import se.esss.ics.iocfactory.model.ConsistencyStatusValue;
import se.esss.ics.iocfactory.model.GenerateEntry;
import se.esss.ics.iocfactory.model.IOC;
import se.esss.ics.iocfactory.util.Util;

/**
 * Service providing functionality for auditing IOCs (comparing the deployed/generated configuration) to the saved
 * configuration
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
@Stateless
public class AuditService {
    private static final Logger LOG = Logger.getLogger(AuditService.class.getName());

    @Inject
    private transient IOCConfigsCache iocCache;
    @Inject
    private transient CachedSetupService setupService;

    // ToDo: Using EJB annotation due bug in Wildfly 8.1
    @EJB
    private transient ConfigurationService configService;
    @Inject
    private transient GenerateService generateService;

    @Inject
    private transient GenerateEntryService generateEntryService;

    @PersistenceContext
    private transient EntityManager em;

    /**
     * Retrieves a list of audit entries which is a cartesian product between IOCs and environments
     *
     * Obviously not all IOCs will have complete audit entries.
     *
     * @return list of audit entries
     * @throws ConfigClientException
     */
    public List<AuditEntry> getAuditEntries() throws ConfigClientException {
        // Collect the entries in a map accessable by IOC
        final Map<String, GenerateEntry> genEntries =
                generateEntryService.getLastGenerateEntriesWithConfig().stream()
                        .collect(Collectors.toMap(ge -> generateIocEnvKey(ge.getIocName(), ge.getEnvironment()),
                                Function.identity()));

        final List<IOC> iocs = iocCache.getIOCs();
        iocs.sort((l, r) -> l.getName().compareTo(r.getName()));

        final List<IOCEnvironment> envs = setupService.loadEnvs();
        envs.sort((l, r) -> l.getName().compareTo(r.getName()));

        final List<AuditEntry> result = new ArrayList<>();

        iocs.forEach(ioc -> {
            envs.forEach(env -> {
                final GenerateEntry generateEntry = genEntries.get(generateIocEnvKey(ioc.getName(), env.getName()));
                if (generateEntry != null) {
                    final AuditEntry auditEntry = new AuditEntry(ioc, env.getName(), generateEntry);
                    checkDiffers(auditEntry);
                    result.add(auditEntry);
                }
            });
        });

        return result;
    }

    public Pair<String, String> getEnvAndStFromDisk(AuditEntry auditEntry) {
        final File path = new File(auditEntry.getGenerateEntry().getTargetPath());
        final File env = new File(path, GenerateService.SHELL_OUTPUT_FILENAME);
        final File st = new File(path, GenerateService.OUTPUT_FILENAME);
        try {
            return new ImmutablePair(FileUtils.readFileToString(env, StandardCharsets.US_ASCII),
                    FileUtils.readFileToString(st, StandardCharsets.US_ASCII));
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, "Error while reading the env.sh file on disk", ex);
            return new ImmutablePair("", "");
        }
    }

    public Pair<String, String> getEnvAndStFromConf(AuditEntry auditEntry)
            throws ConfigClientException, GenerateService.GenerationException {
        final Configuration config = configService.loadConfiguration(auditEntry.getGenerateEntry().getConfigId());
        return generateService.previewConfiguration(config, false);
    }

    private void setDiskModificationTime(AuditEntry auditEntry) {
        final File path = new File(auditEntry.getGenerateEntry().getTargetPath());
        final File env = new File(path, GenerateService.SHELL_OUTPUT_FILENAME);
        final File st = new File(path, GenerateService.OUTPUT_FILENAME);

        final Optional<Long> lastModified = Stream.of(path, env, st).filter(File::exists).map(File::lastModified)
                .max(Long::compare);
        auditEntry.setDiskModificationTime(lastModified.isPresent() ? new Date(lastModified.get()) : null);
    }

    public void checkDiffers(AuditEntry auditEntry) {
        final GenerateEntry ge = auditEntry.getGenerateEntry();
        final Configuration config = em.find(Configuration.class, ge.getConfigId());
        if (config == null) {
            throw new IllegalArgumentException("Should not happen. Audit entry configuration not found in Db.");
        }
        if (generateEntryService.isGenerateEntryPresentInFilesystem(ge)) {
            setDiskModificationTime(auditEntry);
            final File path = new File(ge.getTargetPath());
            boolean different = !Objects.equal(config.getOutputHash(), calculatePathHash(path, auditEntry));
            auditEntry.setConfigDifferent(different);
            if (different) {
                auditEntry.getConsistencyStatus()
                        .add(new ConsistencyStatusValue(ConsistencyMsgLevel.WARN,
                                String.format(
                                        "Configuration in the directory differs from the configuration revision %d",
                                        ge.getConfigRevision())));
            }
        } else {
            auditEntry.setConfigDifferent(true);
            auditEntry.getConsistencyStatus()
                    .add(new ConsistencyStatusValue(ConsistencyMsgLevel.WARN,
                            "IOC deployment directory missing for environment: " + ge.getEnvironment()));
        }
    }

    private static String calculatePathHash(File path, AuditEntry auditEntry) {
        final MessageDigest digest = Util.getSHA1MessageDigest();

        try {
            File[] directoryChildren = path.listFiles();
            // We don't need comparator here because File is a comparable class, which by default sorts pathnames
            // lexicographically.
            Arrays.sort(directoryChildren);
            for (File child : directoryChildren) {
                digestFile(digest, child, auditEntry);
            }
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, null, ex);
            return "";
        }

        final String digestString = Hex.encodeHexString(digest.digest());
        LOG.info("File system hash: " + digestString);
        return digestString;
    }

    private static void digestFile(final MessageDigest digest, final File path, final AuditEntry auditEntry)
            throws IOException {

        if (!path.exists() || !path.isFile()) {
            auditEntry.getConsistencyStatus()
                    .add(new ConsistencyStatusValue(ConsistencyMsgLevel.ERROR, String.format("The %s file is missing",
                            path.getName())));
            return;
        }

        if (!path.canRead()) {
            auditEntry.getConsistencyStatus()
                    .add(new ConsistencyStatusValue(ConsistencyMsgLevel.ERROR,
                            String.format("The %s file is unreadable by IOC Factory", path.getName())));
            return;
        }

        try (final FileInputStream fis = new FileInputStream(path)) {
            DigestUtils.updateDigest(digest, fis);
        } catch (IOException ex) {
            auditEntry.getConsistencyStatus()
                    .add(new ConsistencyStatusValue(ConsistencyMsgLevel.ERROR,
                            String.format("Error while reading the %s file", path.getName())));
            LOG.log(Level.SEVERE, String.format("Error while digesting the file %s", path.getAbsolutePath()), ex);
            throw ex;
        }
    }

    private static String generateIocEnvKey(String iocName, String envName) {
        return String.format("%s###%s", iocName, envName);
    }
}
