/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Sets;
import java.io.File;
import java.util.*;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.Pair;

import se.esss.ics.iocfactory.model.ConsistencyMsgLevel;
import se.esss.ics.iocfactory.model.Module;
import se.esss.ics.iocfactory.model.ModuleDependency;
import se.esss.ics.iocfactory.model.ParameterPlaceholder;
import se.esss.ics.iocfactory.model.Repository;
import se.esss.ics.iocfactory.model.RepositoryMessage;
import se.esss.ics.iocfactory.model.Snippet;
import se.esss.ics.iocfactory.model.VersionConverters;
import se.esss.ics.iocfactory.util.Util;
import se.esss.ics.macrolib.MacroStorage;

/**
 * Base class for scanning a directory representing an EPICS module base for modules, OS versions and EPICS versions
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 *
 */
public abstract class BaseRepositoryScanner {
    private static final Logger LOG = Logger.getLogger(BaseRepositoryScanner.class.getCanonicalName());

    protected static final String EPICS_DIR_PREFIX = "base-";
    private static final String DEP_FILE_EXTENSION = ".dep";
    private static final String LIB_DIRECTORY = "lib";

    /** a set of names that can occur in base/lib directory that are not in-fact OS version */
    private static final Set<String> BASE_LIB_OS_BLACK_LIST = Sets.newHashSet(
                "perl",
                "pkgconfig"
            );

    private File repoDir;

    protected static class FileVersion {
        private final File verDir;
        private final String dirName;

        public FileVersion(File verDir, String dirName) {
            this.verDir = verDir;
            this.dirName = dirName;
        }

        public File getVerDir() {
            return verDir;
        }

        public String getDirName() {
            return dirName;
        }
    }

    protected void setRepositoryBaseDir(File baseDir) {
        this.repoDir = baseDir;
    }

    protected void prepareRepository(final Repository repo) {
        repo.clear();
        repo.setLastScanTime(new Date());
    }

    protected File createRepoDirChild(String childDirName) {
        Preconditions.checkNotNull(repoDir);
        return new File(repoDir, childDirName);
    }

    protected File getRepoDir() {
        return repoDir;
    }

    protected List<File> extractEpicsBaseDirs(Repository repo, File basesDir) {
        if (!isValidDir(repo, basesDir, "EPICS Bases dir does not exist")) {
            return Collections.emptyList();
        }

        return Arrays.stream(Objects.requireNonNull(basesDir.listFiles())).
                filter(File::isDirectory).
                filter(f -> f.getName().startsWith(EPICS_DIR_PREFIX)).collect(Collectors.toList());
    }

    protected List<FileVersion> collectVersionStrings(Repository repo, File modulesDir) {
        List<FileVersion> result = new ArrayList<>();
        if (isValidDir(repo, modulesDir, String.format(
                "Module repository directory '%s' is invalid (does not exist). Will have empty repo.",
                modulesDir.getAbsolutePath()))) {
            Arrays.stream(modulesDir.listFiles()).
                    filter(File::isDirectory).
                    forEach(dir
                            -> Arrays.stream(dir.listFiles()).
                            filter(File::isDirectory).
                            forEach(verDir -> {
                                result.add(new FileVersion(verDir, dir.getName()));
                            }));
        }

        return result;
    }

    protected List<Module> processLibDir(Repository repo, String moduleName, String moduleVersion, File snippetDir,
                                         Pattern snippetPattern, File epicsDir, String epicsVersion) {
        List<Module> modules = new ArrayList<>();
        final File libDir = new File(epicsDir, LIB_DIRECTORY);
        if (!Util.testIfReadableDir(libDir)) {
            logWarning(repo, "Directory not valid or does not exist", libDir);
        } else {
            // Go trough each OS directory
            Arrays.stream(libDir.listFiles()).
                    filter(File::isDirectory).forEach(osDir -> {
                final File depFile = new File(osDir, moduleName + DEP_FILE_EXTENSION);
                final List<ModuleDependency> dependencies =
                        ModuleDependencyLoader.loadModuleDependencies(repo, depFile);
                List<Snippet> snippets = loadSnippets(repo, snippetDir, snippetPattern);
                final Module module = new Module(moduleName, moduleVersion,
                        VersionConverters.getEpicsConverter().fromString(epicsVersion),
                        osDir.getName(), dependencies, snippets);
                modules.add(module);
            });
        }
        return modules;
    }

    protected boolean isRepoDirValidDir(Repository repo) {
        Preconditions.checkNotNull(repoDir);
        return isValidDir(repo, repoDir, "EPICS repository directory is invalid (does not exist). "
                + "Will have empty repo.");
    }

    protected boolean isValidDir(Repository repo, File dir, String message) {
        boolean rv = dir!=null && dir.exists() && dir.isDirectory();
        if (!rv) {
            logError(repo, message, dir);
        }
        return rv;
    }

    protected String getEpicsVersionFromDir(File f) {
        return f.getName().replace(EPICS_DIR_PREFIX, "");
    }

    protected void scanOSVersions(Repository repo) {
        Preconditions.checkNotNull(repoDir);
        Arrays.stream(repoDir.listFiles()).
            filter(f -> f.isDirectory() && f.getName().startsWith(EPICS_DIR_PREFIX)).
            map(f -> new File(f,"lib")).
            filter(Util::testIfReadableDir).
            forEach(libDir ->
                Arrays.stream(libDir.listFiles()).
                    filter(osDir -> osDir.isDirectory() && !BASE_LIB_OS_BLACK_LIST.contains(osDir.getName())).
                    forEach(osDir ->
                       repo.addOsVersion(osDir.getName())
                    )
            );
    }

    private List<Snippet> loadSnippets(Repository repo, File startupDir, Pattern snippetPattern) {
        if (!Util.testIfReadableDir(startupDir)) {
            return ImmutableList.of();
        }

        final File[] fileList = startupDir.listFiles();
        if (fileList == null) {
            return ImmutableList.of();
        }

        final ArrayList<Snippet> snippets = new ArrayList<>();
        boolean anythingFound = false;

        for (File sf : fileList) {
            final Matcher matcher = snippetPattern.matcher(sf.getName());
            if (matcher.matches() && matcher.groupCount()==1) {
                anythingFound = true;

                final Pair<MacroStorage, List<ParameterPlaceholder>> lm =
                        (new MacroLoader(repo, sf)).loadMacros();
                snippets.add( new Snippet(matcher.group(1), sf, lm.getLeft(), lm.getRight()));
            }
        }

        if (!anythingFound) {
            logWarning(repo, String.format("No snippets found matching the pattern %s in the directory, "
                    + "is the pattern valid?", snippetPattern.toString()), startupDir);
        }

        return snippets;
    }

    private void logError(Repository repo, String message, File dir) {
        LOG.severe(message);
        repo.getMessages().add(new RepositoryMessage(ConsistencyMsgLevel.ERROR, message, dir));
        repo.setError(true);
    }

    private void logWarning(Repository repo, String message, File dir) {
        LOG.fine(message);
        repo.getMessages().add(new RepositoryMessage(ConsistencyMsgLevel.WARN, message, dir));
    }
}
