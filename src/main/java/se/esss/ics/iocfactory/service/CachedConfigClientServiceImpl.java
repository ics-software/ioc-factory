/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.enterprise.inject.Alternative;
import javax.inject.Inject;
import javax.faces.view.ViewScoped;
import se.esss.ics.iocfactory.model.IOC;
import se.esss.ics.iocfactory.model.IOCDevice;
import se.esss.ics.iocfactory.service.qualifiers.DirectConfigClient;

/**
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
@ViewScoped
@Alternative
public class CachedConfigClientServiceImpl implements ConfigClientService, Serializable {
    @Inject @DirectConfigClient private transient ConfigClientService impl;

    private List<IOC> cachedIocs = null;
    private Map<String, List<IOCDevice>> cachedDevices = new HashMap<>();

    @Override
    public List<IOC> getAllIOCs() throws ConfigClientException {
        if (cachedIocs == null) {
            cachedIocs = impl.getAllIOCs();
        }
        return cachedIocs;
    }

    @Override
    public List<IOCDevice> getDevicesForIOCByName(String iocName) throws ConfigClientException {
        final List<IOCDevice> cached = cachedDevices.get(iocName);
        if (cached == null) {
            final List<IOCDevice> retrieved = impl.getDevicesForIOCByName(iocName);
            cachedDevices.put(iocName, retrieved);
            return retrieved;
        } else {
            return cached;
        }
    }

    @Override
    public List<IOCDevice> getDevicesForIOCByNameId(String iocNameId) throws ConfigClientException {
        final List<IOCDevice> cached = cachedDevices.get(iocNameId);
        if (cached == null) {
            final List<IOCDevice> retrieved = impl.getDevicesForIOCByNameId(iocNameId);
            cachedDevices.put(iocNameId, retrieved);
            return retrieved;
        } else {
            return cached;
        }
    }
}
