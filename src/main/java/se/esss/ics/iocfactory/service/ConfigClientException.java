/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service;

/**
 * Exception class used to report CCDB REST client errors
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
public class ConfigClientException extends Exception {
    /** The default user friendly error message for the exception */
    public static final String ERR_MSG = "Error while connecting to the Controls Configuration Database. "
            + "Please contact support.";

    /** Initializes an exception object with the default error message, see {@link ConfigClientException#ERR_MSG}*/
    public ConfigClientException() {
        super(ERR_MSG);
    }

    /**
     * Initializes exception object with a message
     * 
     * @param msg 
     */
    public ConfigClientException(String msg) {
        super(ERR_MSG);
    }
    /** Initializes an exception object wrapping the causing {@link Throwable} and the default error message
     * @param reason the cause exception
     */
    public ConfigClientException(Throwable reason) {
        super(ERR_MSG, reason);
    }
}
