/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service;

import java.util.List;
import se.esss.ics.iocfactory.model.IOC;
import se.esss.ics.iocfactory.model.IOCDevice;

/**
 * An abstract service that provides information for IOCs, devices and concrete modules they use
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 *
 */
public interface ConfigClientService {
    /**
     * Method to obtain all known IOC instances in the system.
     *
     * @return list of IOCs in the CCDB
     * @throws ConfigClientException if error occurs during API call
     */
    public List<IOC> getAllIOCs() throws ConfigClientException;


    /**
     * Retrieves all devices in order controlled by the IOC, based on the IOC name.
     *
     * Note it includes all devices directly CONTROLED BY and indirectly CONTROLED BY this IOC
     *
     * The list should contain the devices as gather by DEPTH FIRST slot visiting from the IOC slot.
     *
     * @param iocName the name of the IOC for which to get devices
     * @return the list of IOCDevice objects
     * @throws ConfigClientException if error occurs during the API call
     */
    public List<IOCDevice> getDevicesForIOCByName(String iocName) throws ConfigClientException;

    /**
     * Retrieves all devices in order controlled by the IOC, based on the IOC name.
     *
     * Note it includes all devices directly CONTROLED BY and indirectly CONTROLED BY this IOC
     *
     * The list should contain the devices as gather by DEPTH FIRST slot visiting from the IOC slot.
     * 
     * @param iocNameId the name UUID of the IOC for which to get devices
     * @return the list of IOCDevice objects
     * @throws ConfigClientException if error occurs during the API call
     */
    public List<IOCDevice> getDevicesForIOCByNameId(String iocNameId) throws ConfigClientException;
}
