/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.openepics.discs.client.CCDBClient;
import org.openepics.discs.client.impl.ResponseException;
import org.openepics.discs.conf.jaxb.SlotReferenceXml;
import org.openepics.discs.conf.jaxb.SlotXml;
import org.openepics.discs.conf.jaxb.PropertyValueXml;
import org.openepics.discs.conf.jaxrs.client.DeviceTypeClient;
import org.openepics.discs.conf.jaxrs.client.InstallationSlotClient;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import se.esss.ics.iocfactory.configuration.IOCFactSetup;
import se.esss.ics.iocfactory.model.IOC;
import se.esss.ics.iocfactory.model.IOCConsistencyStatus;
import se.esss.ics.iocfactory.model.IOCDevice;
import se.esss.ics.iocfactory.service.qualifiers.DirectConfigClient;
import se.esss.ics.iocfactory.util.Util;

/**
 * A dummy implementation of a {@link ConfigClientService}
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 *
 */
@Stateless
@DirectConfigClient
public class ConfigClientServiceImpl implements ConfigClientService {

    private static final Logger LOG = Logger.getLogger(ConfigClientServiceImpl.class.getName());

    public static final String CCDB_URL_PROPERTY = "iocfactory.ccdb.rest.url";
    public static final String CCDB_URL_DEFAULT = "http://localhost:8080/ccdb/";

    public static final String SNIPPET_VALUE_EXTENSION = ".cmd";

    private static final String STRINGS_LIST = "Strings List";

    @Inject
    private transient CachedSetupService setupService;

    private static final CSVFormat STRING_LIST_FORMAT = CSVFormat.DEFAULT.withSkipHeaderRecord(true).withDelimiter(',')
            .withIgnoreSurroundingSpaces().withQuote('"');

    private InstallationSlotClient slotClient;
    private DeviceTypeClient devTypeClient;

    /**
     * Initializes the client from System properties
     */
    @PostConstruct
    public void init() {
        final Properties clientProps = new Properties();

        final String sysPropUrl = System.getProperty(CCDB_URL_PROPERTY, CCDB_URL_DEFAULT);

        clientProps.put(CCDBClient.PROPERTY_NAME_BASE_URL, Util.ensureTrailingSlash(sysPropUrl) + "rest");

        // Dummy values as they are required
        clientProps.put(CCDBClient.PROPERTY_NAME_USERNAME, "user");
        clientProps.put(CCDBClient.PROPERTY_NAME_PASSWORD, "pass");

        slotClient = CCDBClient.createInstallationSlotClient(clientProps);
        devTypeClient = CCDBClient.createDeviceTypeClient(clientProps);
    }

    /**
     * {@inheritDoc}
     *
     * @return list of all iocs
     * @throws ConfigClientException
     */
    @Override
    public List<IOC> getAllIOCs() throws ConfigClientException {
        final Pattern iocDevNameRegEx = Pattern.compile(setupService.loadSetup().getIocDeviceTypes());
        final IOCFactSetup loadedSetup = setupService.loadSetup();

        try {
            return devTypeClient.getAllDeviceTypes().stream().map(devType -> devType.getName())
                    .filter(devType -> iocDevNameRegEx.matcher(devType).matches())
                    .flatMap(devTypeName -> slotClient.getInstallationSlots(devTypeName).stream())
                    .map(slot -> slotToIOC(slot, loadedSetup.getCcdbOsProperty(),
                            loadedSetup.getCcdbHostnameProperty()))
                    .collect(Collectors.toList());
        } catch (ResponseException re) {
            LOG.log(Level.SEVERE, re.getMessage(), re);
            throw new ConfigClientException(re);
        }
    }

    /**
     * {@inheritDoc}
     *
     * @param iocName
     * @return list of devices for given ioc
     * @throws ConfigClientException
     */
    @Override
    public List<IOCDevice> getDevicesForIOCByName(String iocName) throws ConfigClientException {
        try {
            final SlotXml iocSlot = slotClient.getInstallationSlotByName(iocName);
            final List<SlotXml> deviceSlotsForIoc =
                    slotClient.getControlsChildrenByName(iocName, true, Collections.emptyList());
            return toIocDevices(iocSlot, deviceSlotsForIoc);
        } catch (ResponseException re) {
            LOG.log(Level.SEVERE, re.getMessage(), re);
            throw new ConfigClientException(re);
        }
    }

    private List<IOCDevice> toIocDevices(SlotXml iocSlot, List<SlotXml> deviceSlotsForIoc) {
        final Map<String, SlotXml> slotCache = Stream
                .concat(Stream.of(iocSlot),deviceSlotsForIoc.stream())
                .collect(Collectors.toMap(SlotXml::getName, Function.identity()));

        // To prevent repetition of the flattened stream
        final Set<String> visited = new HashSet<>();

        // The controlled devices stream
        final Stream<SlotXml> devicesSlotStream = flattenControlsSlots(iocSlot, slotCache).filter(slot -> {
            if (visited.contains(slot.getName())) {
                return false;
            } else {
                visited.add(slot.getName());
                return true;
            }
        });

        final IOCFactSetup setup = setupService.loadSetup();
        final String modulePropName = setup.getCcdbDependencyPropery();
        final String snippetPropName = setup.getCcdbSnippetProperty();
        final String macroValuePrefix = setup.getCcdbMacroPrefixProperty();

        final Set<String> propNames = ImmutableSet.of(modulePropName, snippetPropName);

        // device entry for the IOC itself + controlled devices
        // returns only devices which contain at least one of the specified property
        return Stream.concat(Stream.of(iocSlot), devicesSlotStream)
                .filter(slot -> hasSlotAnyProperty(slot, propNames))
                .map(slot -> slotToDevice(slot, modulePropName, snippetPropName, macroValuePrefix))
                .collect(Collectors.toList());
    }

    /**
     * {@inheritDoc}
     *
     * @param iocNameId
     * @return list of devices for given ioc
     * @throws ConfigClientException
     */
    @Override
    public List<IOCDevice> getDevicesForIOCByNameId(String iocNameId) throws ConfigClientException {
        try {
            final SlotXml iocSlot = slotClient.getInstallationSlotByNameId(iocNameId);
            final List<SlotXml> deviceSlotsForIoc =
                    slotClient.getControlsChildrenByNameId(iocNameId, true, Collections.emptyList());
            return toIocDevices(iocSlot, deviceSlotsForIoc);
        } catch (ResponseException re) {
            LOG.log(Level.SEVERE, re.getMessage(), re);
            throw new ConfigClientException(re);
        }
    }

    /**
     * @return <code>true</code> if the slot's properties contain at least one of the properties in propNames.
     */
    private static boolean hasSlotAnyProperty(SlotXml slot, Set<String> propNames) {
        if (slot == null || slot.getProperties() == null) {
            return false;
        }

        long matched = slot.getProperties().stream().filter(prop -> STRINGS_LIST.equals(prop.getDataType()))
                .filter(prop -> propNames.contains(prop.getName())).count();

        return matched > 0;
    }

    private static SlotXml slotForName(final String slotName, final Map<String, SlotXml> slotCache) {
        return slotCache.get(slotName);
    }

    private static Stream<SlotXml> flattenControlsSlots(final SlotXml slot,
            final Map<String, SlotXml> slotCache) {
        // A visited set to prevent infinite loops
        final Set<String> visited = new HashSet<>();
        return flattenControlsSlotsInternal(slot, visited, slotCache);
    }

    private static Stream<SlotXml> flattenControlsSlotsInternal(SlotXml slot,
            final Set<String> visited, final Map<String, SlotXml> slotCache) {
        if (visited.contains(slot.getName())) {
            return Stream.empty();
        } else {
            visited.add(slot.getName());

            return slot.getControls() != null ? Stream.concat(
                    slot.getControls().stream().map(slotName -> slotForName(slotName.getName(), slotCache))
                            .filter(islot -> islot != null),
                    slot.getControls().stream().map(slotName -> slotForName(slotName.getName(), slotCache))
                            .filter(islot -> islot != null)
                            .flatMap(s -> flattenControlsSlotsInternal(s, visited, slotCache)))
                    : Stream.empty();
        }
    }

    private IOC slotToIOC(SlotXml slot, String osPropName, String hostnamePropName) {
        if (slot.getParents() != null && slot.getParents().size() == 1) {
            final SlotReferenceXml parentSlotReference = slot.getParents().get(0);
            final SlotXml parentSlot = parentSlotReference.getNameId() != null ?
                    slotClient.getInstallationSlotByNameId(parentSlotReference.getNameId()) :
                    slotClient.getInstallationSlotByName(parentSlotReference.getName());

            final String explicitHostname = getHostnamePropertyValue(parentSlot, hostnamePropName);

            return new IOC(StringUtils.trim(slot.getName()), StringUtils.trim(slot.getNameId()),
                    StringUtils.trim(slot.getDeviceType()),
                    getPropertyValue(parentSlot, osPropName).orElse(null), StringUtils.isEmpty(explicitHostname)
                            ? null : explicitHostname,
                    StringUtils.trim(slot.getDescription()));
        } else {
            final IOC result = new IOC(StringUtils.trim(slot.getName()), StringUtils.trim(slot.getNameId()),
                    StringUtils.trim(slot.getDeviceType()), null,
                    null, StringUtils.trim(slot.getDescription()));
            result.getConsistencyStatus().add(IOCConsistencyStatus.INVALID_CCDB_STRUCTURE);
            return result;
        }
    }

    private static IOCDevice slotToDevice(SlotXml slot, String modulePropName, String snippetPropName,
                                          final String macroValuePrefix) {
        final int numControlledBy = (slot.getControlledBy() != null) ? slot.getControlledBy().size() : 0;

        return new IOCDevice(StringUtils.trim(slot.getNameId()),
                StringUtils.trim(slot.getName()),
                slot.getDeviceType() != null ? StringUtils.trim(slot.getDeviceType()) : "",
                getStringListPropertyValue(slot, modulePropName), getSnippedPropertiesValue(slot, snippetPropName),
                getPredefinedMacroValues(slot, macroValuePrefix),
                StringUtils.trim(slot.getDescription()),
                numControlledBy);
    }

    private static Optional<String> getPropertyValue(SlotXml slot, String propName) {
        if (slot == null || slot.getProperties() == null) {
            return Optional.empty();
        }
        return slot.getProperties().stream().filter(prop -> StringUtils.equals(prop.getName(), propName)).findFirst()
                .map(PropertyValueXml::getValue);
    }

    private static String getStringListPropertyValue(SlotXml slot, String propName) {
        if (slot == null || slot.getProperties() == null) {
            return "";
        }

        Optional<String> propValue = slot.getProperties().stream()
                .filter(prop -> STRINGS_LIST.equals(prop.getDataType()))
                .filter(prop -> StringUtils.equals(prop.getName(), propName) && isNonEmptyList(prop.getValue()))
                .findFirst().map(PropertyValueXml::getValue);
        if (propValue.isPresent()) {
            return StringUtils.trim(getFirstCsvValue(propValue.get()));
        } else {
            return "";
        }
    }

    private static List<String> getSnippedPropertiesValue(SlotXml slot, String propName) {
        if (slot == null || slot.getProperties() == null) {
            return Collections.emptyList();
        }

        Optional<String> propValue = slot.getProperties().stream()
                .filter(prop -> STRINGS_LIST.equals(prop.getDataType()))
                .filter(prop -> StringUtils.equals(prop.getName(), propName) && isNonEmptyList(prop.getValue()))
                .findFirst().map(PropertyValueXml::getValue);
        if (propValue.isPresent()) {
            return getCsvValueEntries(propValue.get(), SNIPPET_VALUE_EXTENSION);
        } else {
            return Collections.emptyList();
        }
    }
    
    private static Map<String, String> getPredefinedMacroValues(final SlotXml slot, final String macroValuePrefix) {
        if (slot == null || slot.getProperties() == null || StringUtils.isEmpty(macroValuePrefix)) {
            return Collections.emptyMap();
        }

        return slot.getProperties().stream().filter(prop -> prop.getName().startsWith(macroValuePrefix)
                && StringUtils.isNotEmpty(prop.getValue()) && !StringUtils.equalsIgnoreCase(prop.getValue(), "null")).
                collect(Collectors.toMap(prop -> prop.getName().replace(macroValuePrefix, ""),
                        PropertyValueXml::getValue));
    }

    private static String getHostnamePropertyValue(SlotXml slot, String propName) {
        if (slot == null || slot.getProperties() == null) {
            return "";
        }

        Optional<String> propValue = slot.getProperties().stream().filter(prop -> "CSEntryHost".equals(prop.getDataType()))
                .filter(prop -> StringUtils.equals(prop.getName(), propName) && !"null".equals(prop.getValue()))
                .findFirst().map(PropertyValueXml::getValue);
        return propValue.map(StringUtils::trim).orElse("");
    }

    private static String getFirstCsvValue(String value) {
        final CSVRecord record = getFirstCsvRecord(value);
        return record != null && record.size() > 0 ? StringUtils.trim(record.get(0)) : "";
    }

    private static List<String> getCsvValueEntries(String value, String endingToTrim) {
        final CSVRecord record = getFirstCsvRecord(value);
        if (record != null) {
            final ImmutableList.Builder<String> listBldr = ImmutableList.builder();
            for (final String entry : record) {
                if (!StringUtils.isEmpty(entry)) {
                    listBldr.add(removeEnding(StringUtils.trim(entry), endingToTrim));
                }
            }
            return listBldr.build();
        } else {
            return Collections.emptyList();
        }
    }

    private static String removeEnding(String value, String ending) {
        return value.endsWith(ending) ? value.substring(0, value.length() - ending.length()) : value;
    }

    private static boolean isNonEmptyList(String value) {
        return value != null && !value.isEmpty() && !"null".equals(value) && value.startsWith("[")
                && value.endsWith("]");
    }

    private static CSVRecord getFirstCsvRecord(String value) {
        if (isNonEmptyList(value)) {
            final String withoutSquareBrakets = value.substring(1, value.length() - 1);
            final List<CSVRecord> records;
            try {
                records = CSVParser.parse(withoutSquareBrakets, STRING_LIST_FORMAT).getRecords();
                if (records == null || records.isEmpty()) {
                    return null;
                }
                return records.get(0);
            } catch (IOException ex) {
                LOG.log(Level.WARNING, ex.getMessage(), ex);
                return null;
            }
        } else {
            return null;
        }
    }
}
