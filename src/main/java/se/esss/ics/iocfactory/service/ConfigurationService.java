/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.mutable.MutableObject;

import se.esss.ics.iocfactory.model.Configuration;
import se.esss.ics.iocfactory.model.ConfigurationConsistencyStatus;
import se.esss.ics.iocfactory.model.ConsistencyMsgLevel;
import se.esss.ics.iocfactory.model.ConsistencyStatusSet;
import se.esss.ics.iocfactory.model.ConsistencyStatusValue;
import se.esss.ics.iocfactory.model.DeviceConfiguration;
import se.esss.ics.iocfactory.model.DeviceConsistencyStatus;
import se.esss.ics.iocfactory.model.E3Module;
import se.esss.ics.iocfactory.model.E3Repository;
import se.esss.ics.iocfactory.model.Module;
import se.esss.ics.iocfactory.model.IOC;
import se.esss.ics.iocfactory.model.IOCDevice;
import se.esss.ics.iocfactory.model.Parameter;
import se.esss.ics.iocfactory.model.ParameterConsistencyStatus;
import se.esss.ics.iocfactory.model.ParameterPlaceholder;
import se.esss.ics.iocfactory.model.ParameterType;
import se.esss.ics.iocfactory.model.Repository;
import se.esss.ics.iocfactory.model.Snippet;
import se.esss.ics.iocfactory.service.rbac.IOCFactoryRBACDefinitions;
import se.esss.ics.iocfactory.util.IOCFactoryRuntimeException;
import se.esss.ics.iocfactory.util.ModuleVersionsComparator;
import se.esss.ics.iocfactory.util.UiUtility;
import se.esss.ics.iocfactory.util.Util;

/**
 * A service that performs operations on IOC configurations - {@link Configuration}
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
@Stateless
@PermitAll
public class ConfigurationService {

    private static final String ENV_MODULE_NAME = "environment";

    @PersistenceContext
    private EntityManager em;

    @Inject
    private ConfigClientService configClientService;
    @Inject
    private IOCService iocService;
    @Inject
    private IOCConfigsCache iocCache;

    // ToDo: Using EJB annotation due bug in Wildfly 8.1
    @EJB
    private GenerateService generateService;
    @EJB
    private LogService logService;

    private static final YamlHelper<Configuration> YAML_HELPER = new YamlHelper(true);

    /**
     * Retrieves a list of persisted configurations, correctly sorted by IOC, revision (last rev to first rev) for the
     * given {@link List} of IOCs (@{link IOC})
     *
     * The provided configuration objects only contain the persisted data (they are not fully assembled as done by
     * {@link ConfigurationService#loadConfiguration(java.lang.Long)}
     *
     * Note: Not yet used as of 2015-12-08
     *
     * @param iocs
     *            a list of IOCs for which to retrieve the configurations
     * @return a list of matching configurations
     */
    public List<Configuration> getConfigurationsForIOCs(List<String> iocs) {
        final List<Configuration> configs = (List<Configuration>) em
                .createQuery("select c from Configuration c " + "where c.ioc.name in :iocs order by c.revision DESC")
                .setParameter("ioc_name", iocs).getResultList();
        // Validate configuration
        checkConsistency(configs);

        return configs;
    }

    /**
     * Retrieves a list of persisted configurations in the correctly sorted order by revision (last rev to first rev)
     * for a given IOC name
     *
     * The provided configuration objects only contain the persisted data (they are not fully assembled as done by
     * {@link ConfigurationService#loadConfiguration(java.lang.Long)}.
     *
     * @param ioc
     *            the name of the IOC for which the configurations are to be retrieved
     * @return a list of matching configurations
     */
    @SuppressWarnings("unchecked")
    public List<Configuration> getConfigurationsForIOC(String ioc) {
        final List<Configuration> configs = (List<Configuration>) em
                .createQuery("select c from Configuration c " + "where c.ioc.name = :ioc_name order by c.revision DESC")
                .setParameter("ioc_name", ioc).getResultList();
        // Validate configuration
        checkConsistency(configs);

        return configs;
    }

    /**
     * Retrieves a persisted configuration by a given configurationraiton id.
     *
     * The provided configuration object only contains the persisted data (it is not fully assembled as done by
     * {@link ConfigurationService#loadConfiguration(java.lang.Long)}.
     *
     * @param id
     * @return configuration with the given id
     */
    public Configuration getConfigurationById(Long id) {
        return (Configuration) Util.getSingleResult(
                em.createQuery("select c from Configuration c where c.id = :id").setParameter("id", id));
    }

    /**
     * Persists a newly created configuration metadata. Used when new configuration is created.
     *
     * This method does not take into account the run-time constructed configuration topology (devices, macros, globals)
     * using hte CCDB & the repository structure. It should be used only for saving the metadata for a new
     * configuration.
     *
     * For full configuration save, please use
     * {@link ConfigurationService#updateConfiguration(se.esss.ics.iocfactory.model.Configuration)}
     *
     * @param config
     */
    @RolesAllowed({ IOCFactoryRBACDefinitions.CONFIGURE_IOC_PERMISSION })
    public void saveNewConfiguration(Configuration config) {
        // Persist the IOC if it is not persisted in the database
        config.setIoc(iocService.saveIOC(config.getIoc()));

        // Make sure to update last revision on the IOC
        updateIOCLastRevision(config);

        config.setLastEdit(new Date());

        em.persist(config);

        // Audit log entry
        logService.logConfigAdded(config);
    }

    /**
     * EJB Roles secured version of
     * {@link ConfigurationService#updateConfiguration(se.esss.ics.iocfactory.model.Configuration)}
     *
     * @param config
     *            the configuration to be updated
     */
    @RolesAllowed({ IOCFactoryRBACDefinitions.CONFIGURE_IOC_PERMISSION })
    public void updateConfigurationSecured(Configuration config) {
        try {
            updateConfiguration(config);

            // Audit log entry
            logService.logConfigSaved(config);
        } catch (OptimisticLockException ole) {
            UiUtility.showErrorMessage("Configuration update failed", "Configuration (" + config.getIoc().getName() + "Revision: " +
                            config.getRevision() + ") was concurrently modified by another transaction, please try again!", ole);
            throw ole;
        }
    }

    /**
     * Updates a configuration in the database in case if needed, honoring the dependent devices, device
     * macros
     *
     * @param config original IOC configuration
     * @param devices updated device configuration list (may contain device configurations with consistency error)
     */
    public void updateConfigurationDevicesIfNeeded(final Configuration config, final List<DeviceConfiguration> devices) {
        List<DeviceConfiguration> devicesWithoutError = getDevicesWithoutError(devices);
        final boolean hasNewDevice = devicesWithoutError.stream().
                anyMatch(deviceConfiguration -> !config.getDevices().contains(deviceConfiguration));
        // Device configuration to save exists
        if (hasNewDevice) {
            config.getDevices().clear();
            config.getDevices().addAll(devices);
            updateConfiguration(config);
        }
    }

    /**
     * Updates a configuration in the database, honoring all the dependent entities (global macros, devices, device
     * macros) - System non secured updates, for secured (EJB Roles respected) please use
     * {@link ConfigurationService#updateConfigurationSecured(se.esss.ics.iocfactory.model.Configuration)}
     *
     * @param config
     */
    public void updateConfiguration(Configuration config) {
        config.setLastEdit(new Date());
        config.setHash(YAML_HELPER.getEntityDigestQuiet(config));
        config.setOutputHash(generateService.calculateConfigurationOutputHash(config, false));
        internalMergeConfiguration(config);
    }

    /**
     * Validates a configuration
     *
     * @param config
     */
    public List<ConsistencyStatusValue> validateConfiguration(Configuration config) {
        return validateModuleVersions(config).collect(Collectors.toList());
    }

    /**
     * Fully loads a configuration from the database, including the dependent entities (global macros, devices, device
     * macros). Performs merge operation with current CCDB and repository data. Marks inconsistencies in the entity
     * topology.
     *
     * @param configId
     * @return configuration with the given id
     * @throws ConfigClientException
     */
    public Configuration loadConfiguration(Long configId) throws ConfigClientException {
        final Configuration config = eagerLoadAndDetach(configId);

        // Check if we have to rename devices and apply name IDs
        setMissingDeviceNameIds(config);

        processIOCandDevices(config);

        return config;
    }

    public Configuration eagerLoadAndDetach(Long configId) {
        return eagerLoadAndDetach(em.find(Configuration.class, configId));
    }

    /**
     * Sets name ids for all the devices that don't have the name id set yet.
     *
     * @param config the configuration for which we wish to set device name ids.
     * @throws ConfigClientException if the retrieval of the devices for the configuration failed.
     */
    public void setMissingDeviceNameIds(Configuration config) throws ConfigClientException {
        final List<DeviceConfiguration> persistedDevices = config.getDevices();
        final List<IOCDevice> ccdbDevices = getDevicesForIoc(config.getIoc());

        for (DeviceConfiguration persistedDevice : persistedDevices) {
            Optional<IOCDevice> ccdbDevice = ccdbDevices.stream()
                    .filter(device -> isDeviceMatch(persistedDevice, device))
                    .findFirst();

            if (ccdbDevice.isPresent()) {
                final IOCDevice iocDevice = ccdbDevice.get();

                boolean changedPersistedDevice = false;

                if (StringUtils.isEmpty(persistedDevice.getNameId())) {
                    // Set the name id if it wasn't assigned to it in the past.
                    persistedDevice.setNameId(iocDevice.getNameId());
                    changedPersistedDevice = true;
                }

                if (!config.isCommited() && !StringUtils.equals(persistedDevice.getName(), iocDevice.getName())) {
                    // Use device name from CCDB. Doing this facilitates renaming of devices.
                    persistedDevice.setName(iocDevice.getName());
                    changedPersistedDevice = true;
                }

                if (changedPersistedDevice) {
                    em.merge(persistedDevice);
                }
            }
        }
    }

    /**
     * A utility method that retrieves a revision id for a new configuration for the given IOC name.
     *
     * It works by consulting the {@link IOC#getLastConfigRevision()} field in the IOC database table
     *
     * @param iocName
     *            the name of the IOC
     * @return a new revision number for a configuration for the given IOC
     */
    public int getNextConfigurationForIOC(String iocName) {
        // If IOC was persisted in DB, it contains last revision number used for configurations,
        // if not in db we'll assign 0 as such IOC was not yet persisted in the db
        return getNextConfigurationForIOC(em.find(IOC.class, iocName));
    }

    /**
     * Deletes a configuration. Only non committed configurations can be deleted.
     *
     * @param selectedConfigs
     *            the List of configs to delete
     *
     * @return true if configuration is deleted, false if it is committed
     */
    @RolesAllowed({ IOCFactoryRBACDefinitions.CONFIGURE_IOC_PERMISSION })
    public boolean deleteConfigurations(List<Configuration> selectedConfigs) {
        if (selectedConfigs.stream().anyMatch(c -> c.isCommited())) {
            return false;
        }

        selectedConfigs.forEach(config -> {
            final Configuration mergedConfig = em.merge(config);
            logService.logConfigDeleted(mergedConfig);
            em.remove(mergedConfig);
        });
        return true;
    }

    /**
     * Changes the module of a device configuration and reloads parameters to a new version
     *
     * @param device
     *            the device configuration
     * @param newVersion
     *            the new version
     * @throws ConfigClientException
     * @return true if the module was found and changed, false if the module version is incompatible with the dev.
     *         module
     */
    @RolesAllowed({ IOCFactoryRBACDefinitions.CONFIGURE_IOC_PERMISSION })
    public boolean changeDeviceModuleVersion(DeviceConfiguration device, String newVersion)
            throws ConfigClientException {
        final Module supportModule = device.getSupportModule();
        if (supportModule == null) {
            throw new NullPointerException("changeDeviceModuleVersion: Device doesn't have support module defined.");
        }

        final Optional<Module> newModule = Util.getRepository().getModuleForSpec(supportModule.getName(),
                supportModule.getOs(), supportModule.getEpicsVersion(), newVersion);

        if (newModule.isPresent()) {
            device.setSupportModule(newModule.get());

            validateDeviceModule(device, Util.getRepository());

            loadDeviceParameters(device);
            return true;
        } else {
            return false;
        }
    }

    @RolesAllowed({ IOCFactoryRBACDefinitions.CONFIGURE_IOC_PERMISSION })
    public boolean deleteInvalidDevices(List<DeviceConfiguration> devices) throws ConfigClientException {
        MutableObject<Configuration> lastRemovedConfigRef = new MutableObject<>(null);
        devices.stream().filter(dev -> dev.getConsistencyStatus().isError()).forEach(dev -> {
            lastRemovedConfigRef.setValue(dev.getConfiguration());
            lastRemovedConfigRef.getValue().getDevices().remove(dev);
        });

        if (lastRemovedConfigRef.getValue() != null) {
            processIOCandDevices(lastRemovedConfigRef.getValue());
            return true;
        } else {
            return false;
        }
    }

    @RolesAllowed({ IOCFactoryRBACDefinitions.CONFIGURE_IOC_PERMISSION })
    public boolean renameInvalidDevice(DeviceConfiguration from, DeviceConfiguration to) throws ConfigClientException {
        if (from == null || to == null || !Objects.equals(from.getConfiguration(), to.getConfiguration())) {
            return false;
        }
        if (!from.getConsistencyStatus().isError()) {
            return false;
        }

        final Configuration config = from.getConfiguration();

        // merge config params
        mergeParameters(from, to);

        // remove from device
        config.getDevices().remove(from);

        // reload the to device
        validateDeviceModule(to, Util.getRepository());
        loadDeviceParameters(to);

        return true;
    }

    @RolesAllowed({ IOCFactoryRBACDefinitions.CONFIGURE_IOC_PERMISSION })
    public boolean copyConfiguration(Long srcId, String destIocName, String user) throws ConfigClientException {
        final IOC destIOC = iocCache.getIOCByName(destIocName, true);
        if (destIOC == null) {
            return false;
        }

        final Configuration srcLoaded = loadConfiguration(srcId);

        final Configuration dest = new Configuration();

        copyConfigMetadata(srcLoaded, dest, destIOC, user);

        saveNewConfiguration(dest);

        copyConfigGlobals(srcLoaded, dest);
        copyDevices(srcLoaded, dest);

        ensureHierarchy(dest);

        dest.setOutputHash(generateService.calculateConfigurationOutputHash(dest, false));

        return true;
    }

    private void copyConfigMetadata(Configuration src, Configuration dest, IOC targetIOC, String user) {
        dest.setComment(src.getComment());
        dest.setCommited(false);
        dest.setEpicsVersion(src.getEpicsVersion());
        dest.setIoc(targetIOC);
        dest.setLastEdit(new Date());
        dest.setOs(targetIOC.getOs());
        dest.setRevision(getNextConfigurationForIOC(targetIOC));
        dest.setUser(user);
        dest.setEnvVersion(src.getEnvVersion());
        dest.setRequireVersion(src.getRequireVersion());
        dest.setProcServPort(src.getProcServPort());
    }

    private void copyConfigGlobals(Configuration src, Configuration dest) {
        src.getGlobals().forEach(srcGlobal -> {
            final Parameter destGlobal = new Parameter(srcGlobal.getName(), null, srcGlobal.getType(),
                    srcGlobal.getTraits());
            destGlobal.setConfiguration(dest);
            destGlobal.setValue(srcGlobal.getValue());

            dest.getGlobals().add(destGlobal);
        });
    }

    private static Stream<DeviceConfiguration> streamPersistedDevices(Configuration src) {
        return src.getDevices().stream().filter(srcDev -> srcDev.getId() != null);
    }

    private void copyDevices(Configuration src, Configuration dest) throws ConfigClientException {

        final List<IOCDevice> ccdbDevices = getDevicesForIoc(src.getIoc());
        final Map<String, IOCDevice> nameIdToDeviceMappings
                = ccdbDevices.stream().collect(Collectors.toMap(IOCDevice::getNameId, Function.identity()));

        streamPersistedDevices(src).forEach(srcDev -> {
            final DeviceConfiguration destDev = new DeviceConfiguration();

            // Get device in CCDB.
            //final IOCDevice iocDevice = nameIdToDeviceMappings.get(srcDev.getNameId());

            destDev.setConfiguration(dest);
            destDev.setNameId(srcDev.getNameId());
            destDev.setName(srcDev.getName());
            destDev.setSupportModule(srcDev.getSupportModule());
            destDev.setSnippets(new ArrayList<>(srcDev.getSnippets()));
            destDev.setEnableInclude(srcDev.getEnableInclude());
            destDev.setNumControlledBy(srcDev.getNumControlledBy());

            dest.getDevices().add(destDev);

            copyDeviceParams(srcDev, destDev);
        });

    }

    private void copyDeviceParams(DeviceConfiguration srcDev, DeviceConfiguration destDev) {
        srcDev.getParameters().stream().filter(srcParam -> srcParam.getId() != null).forEach(srcParam -> {
            final Parameter destParam = new Parameter(srcParam.getName(), srcParam.getSnippetName(), srcParam.getType(),
                    srcParam.getTraits());
            destParam.setDeviceConfiguration(destDev);
            destParam.setValue(srcParam.getValue());
            destParam.setDesc(srcParam.getDesc());

            destDev.getParameters().add(destParam);
        });
    }

    private void mergeParameters(DeviceConfiguration from, DeviceConfiguration to) {
        final Iterator<Parameter> iter = from.getParameters().iterator();

        while (iter.hasNext()) {
            final Parameter fromParam = iter.next();

            // Remove dest param if found with the same space
            final Optional<Parameter> toParam = filterParamsWithSpec(to.getParameters(), fromParam.getName(),
                    fromParam.getSnippetName(), fromParam.getType()).findAny();
            if (toParam.isPresent()) {
                toParam.get().setDeviceConfiguration(null);
                to.getParameters().remove(toParam.get());
            }

            // Move from param to the other device
            fromParam.setDeviceConfiguration(to);
            to.getParameters().add(fromParam);
            iter.remove();
        }
    }

    private void processIOCandDevices(Configuration config) throws ConfigClientException {
        updateConfigIOC(config);
        final Repository repo = Util.getRepository();

        final List<DeviceConfiguration> persistedDevices = new ArrayList<>(config.getDevices());
        final List<IOCDevice> ccdbDevices = getDevicesForIoc(config.getIoc());

        boolean anyPersistedDeviceChanged = false;

        for (DeviceConfiguration persistedDevice : persistedDevices) {
            IOCDevice ccdbDevice = ccdbDevices.stream()
                                              .filter(device -> isDeviceMatch(persistedDevice, device))
                                              .findFirst()
                                              .orElse(null);

            if (ccdbDevice != null) {
                // Update number of slots controlling this device.
                persistedDevice.setNumControlledBy(ccdbDevice.getNumControlledBy());

                if (!config.isCommited()) {
                    // If a module is not set, the variables contain an empty string.
                    // This makes handling of nulls easier.
                    final String persistedModuleName = getNameFromModule(persistedDevice.getSupportModule());
                    final String ccdbDeviceModuleName =
                            ccdbDevice.getModuleName() != null ? ccdbDevice.getModuleName() : "";

                    if (!StringUtils.equals(persistedModuleName, ccdbDeviceModuleName)) {
                        // Updated configuration with data from CCDB device
                        retrieveAndSetModuleOnDeviceConfig(config, persistedDevice, ccdbDeviceModuleName);
                        anyPersistedDeviceChanged = true;
                    }
                }
            }

            // compare (and properly mark) persisted devices with data retrieved from CCDB
            validateDeviceModule(persistedDevice, ccdbDevice, repo);
        }

        // Add devices that are not persisted at the end:
        final int numDevicesBeforeAdding = persistedDevices.size();
        ccdbDevices.stream()
        // All devices that are not in the persisted devices
                .filter(ccdbDevice -> persistedDevices.stream()
                        .noneMatch(persistedDevice -> isDeviceMatch(persistedDevice, ccdbDevice)))
                .forEach(ccdbDevice -> persistedDevices.add(createBlankDeviceConfigFromCCDBDevice(config, ccdbDevice)));

        // If devices were added that also counts as a change.
        anyPersistedDeviceChanged |= (numDevicesBeforeAdding != persistedDevices.size());

        if (!config.isCommited() && anyPersistedDeviceChanged) {
            updateConfigurationDevicesIfNeeded(config, persistedDevices);
        }

        // Process parameters
        persistedDevices.forEach(this::loadDeviceParameters);
        persistedDevices.forEach(this::markDevicesWithEmptySnippets);
    }

    private boolean isDeviceMatch(DeviceConfiguration dbDevice, IOCDevice ccdbDevice) {
        return dbDevice.getNameId() != null && ccdbDevice.getNameId() != null ?
                Objects.equals(dbDevice.getNameId(), ccdbDevice.getNameId()) :
                Objects.equals(dbDevice.getName(), ccdbDevice.getName());
    }

    private List<IOCDevice> getDevicesForIoc(IOC ioc) throws ConfigClientException {
        return ioc.getNameId() != null ?
                configClientService.getDevicesForIOCByNameId(ioc.getNameId()) :
                configClientService.getDevicesForIOCByName(ioc.getName());
    }

    private String getNameFromModule(Module module) {
        if (module != null && module.isDefined()) {
            return module.getName();
        } else {
            return "";
        }
    }

    private DeviceConfiguration createBlankDeviceConfigFromCCDBDevice(final Configuration config,
            final IOCDevice ccdbDevice) {
        final DeviceConfiguration newDeviceConfig = new DeviceConfiguration();
        newDeviceConfig.setConfiguration(config);
        newDeviceConfig.setNameId(ccdbDevice.getNameId());
        newDeviceConfig.setName(ccdbDevice.getName());
        newDeviceConfig.setCcdbDevice(ccdbDevice);
        newDeviceConfig.setEnableInclude(true);
        newDeviceConfig.setNumControlledBy(ccdbDevice.getNumControlledBy());

        // a new (un-generated) device was added in CCDB to a generated config
        if (config.isCommited()) {
            newDeviceConfig.getConsistencyStatus().add(DeviceConsistencyStatus.DEVICE_DOES_NOT_BELONG);
        }

        retrieveAndSetModuleOnDeviceConfig(config, newDeviceConfig, ccdbDevice.getModuleName());

        return newDeviceConfig;
    }

    private void retrieveAndSetModuleOnDeviceConfig(Configuration config, DeviceConfiguration deviceConfig,
                                                    String moduleName) {
        final Repository repo = Util.getRepository();
        // Will prioritize to have numbered modules first
        final Optional<Module> repoModule = repo.getLatestModuleForSpec(moduleName,
                config.getOs(), config.getEpicsVersion(), ModuleVersionsComparator::comparePrioritizeNumbered);

        if (repoModule.isPresent()) {
            deviceConfig.setSupportModule(repoModule.get());
        } else {
            // Set for display purposes
            if (moduleName.isEmpty()) {
                deviceConfig.setSupportModule(new Module());
                deviceConfig.getConsistencyStatus().add(DeviceConsistencyStatus.MODULE_NOT_DEFINED);
            } else {
                deviceConfig.setSupportModule(new Module(moduleName));
                deviceConfig.getConsistencyStatus().add(repo instanceof E3Repository ?
                        DeviceConsistencyStatus.MODULE_DOES_NOT_EXIST_E3 :
                        DeviceConsistencyStatus.MODULE_DOES_NOT_EXIST_EEE);
            }
        }
    }

    private void validateDeviceModule(DeviceConfiguration persistedDevice, IOCDevice ccdbDevice, Repository repo) {
        persistedDevice.getConsistencyStatus().clear();
        persistedDevice.setCcdbDevice(ccdbDevice);

        if (ccdbDevice == null) {
            persistedDevice.getConsistencyStatus().add(DeviceConsistencyStatus.DEVICE_IS_ORPHANED);

        } else if (!persistedDevice.getName().equals(ccdbDevice.getName())) {
            persistedDevice.getConsistencyStatus().add(DeviceConsistencyStatus.DEVICE_NAME_CHANGED);
        } else if (persistedDevice.getSupportModule() == null
                || persistedDevice.getSupportModule().getName() == null
                || persistedDevice.getSupportModule().getName().isEmpty()) {

            persistedDevice.getConsistencyStatus().add(DeviceConsistencyStatus.MODULE_NOT_DEFINED);

        } else if (!Objects.equals(ccdbDevice.getModuleName(), persistedDevice.getSupportModule().getName())) {
            persistedDevice.getConsistencyStatus().add(DeviceConsistencyStatus.DIFFERENT_MODULE);

        } else {
            Optional<Module> repoModule = Optional.empty();
            Optional<E3Module> e3RepoModule = Optional.empty();
            if (isE3Config(persistedDevice.getConfiguration())) {
                if (repo instanceof E3Repository) {
                    E3Repository e3Repository = (E3Repository) repo;
                    e3RepoModule = e3Repository.getModuleFromRepo(
                            new E3Module(persistedDevice.getSupportModule(),
                                    persistedDevice.getConfiguration().getRequireVersion()));
                } else {
                    throw new IOCFactoryRuntimeException("Validating E3 IOC configuration using non E3 repository");
                }

            } else {
                repoModule = repo.getModuleFromRepo(persistedDevice.getSupportModule());
            }

            if (repoModule.isPresent()) {
                persistedDevice.setSupportModule(repoModule.get());
            } else if (e3RepoModule.isPresent()) {
                persistedDevice.setSupportModule(e3RepoModule.get());
            } else {
                if (persistedDevice.getSupportModule().getName().isEmpty()) {
                    persistedDevice.getConsistencyStatus().add(DeviceConsistencyStatus.MODULE_NOT_DEFINED);
                } else {
                    persistedDevice.getConsistencyStatus().add(repo instanceof E3Repository ?
                            DeviceConsistencyStatus.MODULE_DOES_NOT_EXIST_E3 :
                            DeviceConsistencyStatus.MODULE_DOES_NOT_EXIST_EEE);
                }
            }
        }
    }

    private boolean isE3Config(final Configuration configuration) {
        return StringUtils.isNotEmpty(configuration.getRequireVersion()) && configuration.getEnvVersion() == null;
    }

    private void validateDeviceModule(DeviceConfiguration device, Repository repo) throws ConfigClientException {
        final IOCDevice ccdbDevice = getDevicesForIoc(device.getConfiguration().getIoc())
                .stream().filter(ccdbDev -> Objects.equals(ccdbDev.getName(), device.getName())).findAny().orElse(null);
        validateDeviceModule(device, ccdbDevice, repo);
    }

    private Configuration eagerLoadAndDetach(Configuration config) {
        if (config == null) {
            return null;
        }

        // Get global params
        config.getGlobals().forEach(Parameter::getName);

        // get all collections down the way, update OS & EPICS version from config for the modules
        config.getDevices().forEach(dc -> {
            dc.getSupportModule().getName();

            dc.getSupportModule().setOs(config.getOs());
            dc.getSupportModule().setEpicsVersion(config.getEpicsVersion());

            dc.getParameters().forEach(Parameter::getName);
        });

        // Detach is cascaded
        em.detach(config);
        return config;
    }

    private void loadDeviceParameters(DeviceConfiguration persistedDevice) {
        // Clear consistency status as this might be invoked on already loaded parameters
        persistedDevice.getParameters().forEach(param -> param.getConsistencyStatus().clear());

        if (!persistedDevice.getConsistencyStatus().isError()) {
            // disregard empty loaded params
            removeEmptyLoadedParameters(persistedDevice);

            // Get the list of loaded parameters, and put an empty list to build instead
            final List<Parameter> persistedParameters = persistedDevice.getParameters();
            final List<Parameter> newParametersList = new ArrayList<>();
            persistedDevice.setParameters(newParametersList);

            final Set<Parameter> processedPersisted = new HashSet<>();
            final List<Snippet> deviceSnippets = new ArrayList<>();
            persistedDevice.setSnippets(deviceSnippets);

            for (final String persistedSnippet : persistedDevice.getCcdbDevice().getSnippets()) {
                Optional<Snippet> deviceSnippet = persistedDevice.getSupportModule().getSnippets().stream()
                        .filter(snippet -> StringUtils.equalsIgnoreCase(persistedSnippet, snippet.getName())).findFirst();
                if (deviceSnippet.isPresent()) {
                    deviceSnippets.add(deviceSnippet.get());
                    loadDeviceParametersForValidSnippet(persistedDevice, persistedParameters, deviceSnippet.get(),
                            processedPersisted);
                }
            }

            // Handle persisted parameters that are not reported to be part of the config
            persistedParameters.stream().filter(persistedParameter -> !processedPersisted.contains(persistedParameter))
                    .forEach(persistedParameter -> {
                        persistedParameter.setDesc("");
                        persistedParameter.getConsistencyStatus().add(ParameterConsistencyStatus.PERSISTED_INVALID);
                        newParametersList.add(persistedParameter);
                    });

            // For all parameters update the device reference
            newParametersList.forEach(param -> param.setDeviceConfiguration(persistedDevice));
        } else {
            markInvalidParameters(persistedDevice);
        }

    }

    /**
     * Marks devices which have no snippets or have empty snippet values with the
     * {@link DeviceConsistencyStatus#SNIPPET_NOT_DEFINED} status.
     * Skips marking if the device already has a {@link ConsistencyMsgLevel#ERROR} status.
     */
    private void markDevicesWithEmptySnippets(DeviceConfiguration deviceConfiguration) {
        if (!deviceConfiguration.getConsistencyStatus().isError()
                && (deviceConfiguration.getSnippets().isEmpty()
                    || deviceConfiguration.getSnippets().stream().anyMatch(snippet -> snippet.getName().isEmpty()))) {
            deviceConfiguration.getConsistencyStatus().add(DeviceConsistencyStatus.SNIPPET_NOT_DEFINED);
        }
    }

    private void removeEmptyLoadedParameters(final DeviceConfiguration persistedDevice) {
        persistedDevice.getParameters().removeIf(param -> StringUtils.isEmpty(param.getValue()));
    }

    private Stream<Parameter> filterParamsWithSpec(List<Parameter> parameters, String name, String snippetName,
            ParameterType type) {
        return parameters.stream()
                .filter(persistedParameter -> type == persistedParameter.getType()
                        && Objects.equals(persistedParameter.getSnippetName(), snippetName)
                        && StringUtils.equalsIgnoreCase(name, persistedParameter.getName()));
    }

    private void loadDeviceParametersForValidSnippet(DeviceConfiguration persistedDevice,
            List<Parameter> persistedParameters, Snippet snippet, Set<Parameter> processedPersisted) {
        final List<ParameterPlaceholder> placeholders = snippet.getPlaceholders();
        final List<Parameter> deviceParameters = persistedDevice.getParameters();
        final Map<String, String> predefinedMacroValues = persistedDevice.getCcdbDevice().getPredefinedMacroValues();

        // Add all parameters relevant according to placeholders, do it in ConfClient Placeholder order to
        // keep ordering as in definitions
        placeholders.forEach(placeholder -> filterParamsWithSpec(persistedParameters, placeholder.getName(),
                snippet.getName(), placeholder.getType()).findAny().ifPresent(persistedParameter -> {
                    // update desc as it is not persisted
                    persistedParameter.setDesc(placeholder.getDesc());
                    persistedParameter.setMacroEntry(placeholder.getMacroEntry());
                    persistedParameter.setSnippet(snippet);
                    persistedParameter.setTraits(placeholder.getTraits());
                    deviceParameters.add(persistedParameter);

                    processedPersisted.add(persistedParameter);
                }));

        // Add parameters from CCDB
        placeholders.stream()
                .filter(placeholder -> filterParamsWithSpec(deviceParameters, placeholder.getName(), snippet.getName(),
                        placeholder.getType())
                        .noneMatch(parameter -> placeholder.getType() == parameter.getType()
                                && Objects.equals(placeholder.getName(), parameter.getName())))
                .filter(placeholder -> predefinedMacroValues.containsKey(placeholder.getName()))
                .forEach(placeholder -> deviceParameters.add(parameterFromPredefinedMacroValue(placeholder, snippet,
                predefinedMacroValues.get(placeholder.getName()))));

        // Add possible remaining non-matched placeholders
        placeholders.stream()
                .filter(placeholder -> filterParamsWithSpec(deviceParameters, placeholder.getName(), snippet.getName(),
                        placeholder.getType())
                                .noneMatch(parameter -> placeholder.getType() == parameter.getType()
                                        && Objects.equals(placeholder.getName(), parameter.getName())))
                .forEach(placeholder -> deviceParameters.add(emptyParameterFromPlaceholder(placeholder, snippet)));
    }

    private void markInvalidParameters(DeviceConfiguration persistedDevice) {
        persistedDevice.getParameters()
                .forEach(param -> param.getConsistencyStatus().add(ParameterConsistencyStatus.INVALID_DEVICE_CONFIG));

    }

    private static Parameter emptyParameterFromPlaceholder(ParameterPlaceholder placeholder, Snippet snippet) {
        final Parameter result = new Parameter(placeholder, snippet);
        result.getConsistencyStatus().add(ParameterConsistencyStatus.PARAM_IS_NEW);
        return result;
    }

    private static Parameter parameterFromPredefinedMacroValue(final ParameterPlaceholder placeholder,
                                                               final Snippet snippet, final String value) {
        final Parameter result = new Parameter(placeholder, snippet);
        result.setValue(value);
        result.getConsistencyStatus().add(ParameterConsistencyStatus.PREDEFINED_IN_CCDB);
        return result;
    }

    private static List<DeviceConfiguration> ensureHierarchy(Configuration config) {
        // Store devices with consistency error
        List<DeviceConfiguration> devicesWithError = new ArrayList<>();
        config.getGlobals().forEach(global -> {
            global.setConfiguration(config);
            global.setDeviceConfiguration(null);
        });

        // Don't save devices which have errors
        final Iterator<DeviceConfiguration> devIter = config.getDevices().iterator();
        while (devIter.hasNext()) {
            final DeviceConfiguration devConfig = devIter.next();

            if (devConfig.getConsistencyStatus().isError()) {
                // Collect devices with consistency error
                devicesWithError.add(devConfig);
                devIter.remove();
            }
        }

        config.getDevices().forEach(dev -> {
            dev.setConfiguration(config);
            dev.getParameters().stream().forEach(param -> {
                param.setConfiguration(null);
                param.setDeviceConfiguration(dev);
            });
        });

        return devicesWithError;
    }

    private static List<DeviceConfiguration> getDevicesWithoutError(final List<DeviceConfiguration> devices) {
        List<DeviceConfiguration> devicesWithError = new ArrayList<>();
        for (DeviceConfiguration devConfig : devices) {
            if (!devConfig.getConsistencyStatus().isError()) {
                devicesWithError.add(devConfig);
            }
        }

        return devicesWithError;
    }

    private void internalMergeConfiguration(Configuration config) {
        List<DeviceConfiguration> devicesWithError = ensureHierarchy(config);

        em.merge(config);
        // Add devices with consistency error again to display them on UI
        config.getDevices().addAll(devicesWithError);
    }

    private void updateIOCLastRevision(Configuration config) {
        int iocLastRevNo = config.getIoc().getLastConfigRevision();
        int configRevNo = config.getRevision();

        if (configRevNo > iocLastRevNo) {
            config.getIoc().setLastConfigRevision(configRevNo);
        }
    }

    private void checkConsistency(List<Configuration> configs) {
        final Repository repository = Util.getRepository();
        configs.forEach(config -> {
            final ConsistencyStatusSet status = config.getConsistencyStatus();

            status.clear();

            // Check if EPICS version is valid
            if (!repository.getEpicsVersions().contains(config.getEpicsVersion())) {
                status.add(ConfigurationConsistencyStatus.INVALID_EPICS_VERSION);
            }

            // Check if OS version is valid
            if (!repository.getSupportedOsVersions().contains(config.getOs())) {
                status.add(ConfigurationConsistencyStatus.INVALID_OS_VERSION);
            }

            if (StringUtils.isNotEmpty(config.getRequireVersion())) {
                // Check if Require version is valid
                if(!(repository instanceof E3Repository)
                        || !((E3Repository)repository).getBaseRequires(config.getEpicsVersion()).contains(config.getRequireVersion())) {
                    status.add(ConfigurationConsistencyStatus.INVALID_REQUIRE_VERSION);
                }
            } else {
                // Check if Env version is valid
                final Optional<Module> envModule = repository.getModuleForSpec(ENV_MODULE_NAME, config.getOs(),
                        config.getEpicsVersion(), config.getEnvVersion());
                if (!envModule.isPresent()) {
                    status.add(ConfigurationConsistencyStatus.INVALID_ENV_VERSION);

                    final List<Module> envs = repository.getModulesForSpec(ENV_MODULE_NAME, config.getOs(),
                            config.getEpicsVersion());
                    if (envs.isEmpty()) {
                        status.add(ConfigurationConsistencyStatus.NO_ENVIRONMENTS_FOR_OS_EPICS);
                    }
                }
            }
        });
    }

    /**
     * Only the IOC name is stored in persisted storage. This method populates the IOC reference transient fields in the
     * loaded configuration with data from the CCDB.
     *
     * @param config
     *            The configuration for which to update the IOC object transient fields
     * @throws ConfigClientException
     */
    private void updateConfigIOC(Configuration config) throws ConfigClientException {
        final IOC ccdbIoc = iocCache.getIOCByName(config.getIoc().getName(), true);

        if (ccdbIoc != null) {
            IOCService.updateTransientIOCProperties(ccdbIoc, config.getIoc());
        } else {
            config.getConsistencyStatus().add(ConfigurationConsistencyStatus.INVALID_IOC);
        }
    }

    private int getNextConfigurationForIOC(IOC ioc) {
        return ioc != null ? ioc.getLastConfigRevision() + 1 : 0;
    }

    private Stream<ConsistencyStatusValue> validateModuleVersions(Configuration config) {
        // Map from Module Name to Map of Module Version and list of configurations for that version
        // Type aliases would have been nice in Java :(
        final Map<String, Map<String, List<DeviceConfiguration>>> modules = new TreeMap<>();

        // Fill the map
        config.getDevices().forEach(device -> {
            final Module devModule = device.getSupportModule();
            if (devModule != null && devModule.isDefined()) {
                final String moduleName = devModule.getName();
                final String moduleVersion = devModule.getModuleVersion();

                Map<String, List<DeviceConfiguration>> versionDevMap = modules.computeIfAbsent(moduleName, k -> new HashMap<>());

                List<DeviceConfiguration> devList = versionDevMap.computeIfAbsent(moduleVersion, k -> new ArrayList<>());

                devList.add(device);
            }
        });

        // Check for duplicates and add formatted messages
        return modules.entrySet().stream().filter(entry -> entry.getValue().size() > 1).map(entry -> {
            return new ConsistencyStatusValue(ConsistencyMsgLevel.ERROR, String.format(
                    "Different versions of the module %s used:\n%s", entry.getKey(), formatVersions(entry.getValue())));
        });
    }

    private String formatVersions(final Map<String, List<DeviceConfiguration>> entry) {
        return entry.entrySet().stream().map(inner -> {
            return String.format("%s version with device(s) %s", inner.getKey(),
                    inner.getValue().stream().map(dev -> "\t" + dev.getName()).collect(Collectors.joining(", ")));
        }).collect(Collectors.joining("\n"));
    }
}
