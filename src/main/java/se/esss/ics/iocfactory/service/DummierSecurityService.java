/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service;

import java.io.Serializable;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Alternative;

/**
 * Dummy implementation of {@link SecurityService} with one hard coded user supported
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 *
 */
@Stateful
@SessionScoped
@Alternative
@SuppressWarnings({
    // Methods should not be empty
    "squid:S1186"
})
public class DummierSecurityService implements SecurityService, Serializable {
    private static final String USERNAME = "miroslavpavleski";

    @Override
    public boolean doLogin(String user, String password) { return true; }

    @Override
    public String getUsername() { return USERNAME; }

    @Override
    public void doLogout() { }

    @Override
    public boolean isLoggedIn() { return true; }

    @Override
    public boolean canAdminister() { return false; }

    @Override
    public boolean canConfigureIOCs() { return true; }

    @Override
    public boolean canDeployNonProduction() { return true; }

    @Override
    public boolean canDeployProduction() { return true; }
}
