package se.esss.ics.iocfactory.service;

import org.apache.commons.lang3.ArrayUtils;
import se.esss.ics.iocfactory.model.E3Module;
import se.esss.ics.iocfactory.model.E3Repository;
import se.esss.ics.iocfactory.model.EEERepository;
import se.esss.ics.iocfactory.model.Repository;
import se.esss.ics.iocfactory.model.VersionConverters;

import javax.enterprise.inject.Instance;
import javax.enterprise.inject.spi.CDI;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.regex.Pattern;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
@Named("e3RepositoryScanner")
public class E3RepositoryScanner extends BaseRepositoryScanner implements RepositoryScanner<E3Repository> {

    private static final String EPICS_E3_REQUIRE_DIR = "require";
    private static final String EPICS_E3_MODULES_DIR = "siteMods";
    private static final Pattern EPICS_E3_SNIPPET_PATTERN = Pattern.compile("(.*)\\.iocsh");

    @Override
    public void setRepositoryBaseDir(File baseDir) {
        super.setRepositoryBaseDir(baseDir);
    }

    /**
     * Scans the repository
     *
     * We use references and clearing of a repo because of CDI keeping the same reference, it is difficult to replace
     * it, once a Repository object is created by a producer method in the appropriate CDI scope
     *
     * @return A {@link Repository} model object, if repo param was non-null it will hold the same reference
     */
    public E3Repository scanRepository() {
        Instance<E3Repository> instance = CDI.current().select(E3Repository.class);
        E3Repository repository = instance.get();
        CDI.current().destroy(instance);

        prepareRepository(repository);

        if (isRepoDirValidDir(repository)) {
            scanEpicsBaseVersions(repository);
            scanOSVersions(repository);
        }
        return repository;
    }

    private void scanEpicsBaseVersions(E3Repository repo) {
        extractEpicsBaseDirs(repo, getRepoDir()).
                forEach( f -> {
                    repo.addEPICSVersion(VersionConverters.getEpicsConverter().fromString(
                            f.getName().substring(EPICS_DIR_PREFIX.length())));
                        final File requireDir = new File(f, EPICS_E3_REQUIRE_DIR);
                        scanE3Modules(repo, requireDir, getEpicsVersionFromDir(f));
                });
    }

    private void scanE3Modules(E3Repository repo, File requireDir, String epicsVersion) {
        final Pattern requireVerPattern = VersionConverters.REQUIRE_VERSION_PATTERN;
        File[] files = requireDir.listFiles();
        if(ArrayUtils.isNotEmpty(files)) {
            Arrays.stream(files).filter(
                    file -> file.isDirectory()
                            && requireVerPattern.matcher(file.getName()).matches()).forEach(
                    requireVersionDir -> {
                        final File modulesDir = new File(requireVersionDir, EPICS_E3_MODULES_DIR);
                        collectVersionStrings(repo, modulesDir)
                                .forEach(v -> scanE3Module(repo, v.getDirName(), v.getVerDir().getName(),
                                        v.getVerDir(), epicsVersion, requireVersionDir.getName()));
                    });
        }
    }

    private void scanE3Module(E3Repository repo, String moduleName, String moduleVersion, File moduleDir,
                              String epicsVersion, String requireVersion) {

        Collections.singletonList(moduleDir)
                .forEach(epicsDir -> processLibDir(repo, moduleName, moduleVersion, moduleDir, EPICS_E3_SNIPPET_PATTERN,
                        epicsDir, epicsVersion).
                        forEach(module -> repo.addModule(new E3Module(module, requireVersion))));
    }
}
