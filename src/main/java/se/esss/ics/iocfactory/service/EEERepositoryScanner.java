package se.esss.ics.iocfactory.service;

import se.esss.ics.iocfactory.model.EEERepository;
import se.esss.ics.iocfactory.model.Repository;
import se.esss.ics.iocfactory.model.VersionConverters;

import javax.enterprise.inject.Instance;
import javax.enterprise.inject.spi.CDI;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.File;
import java.util.Arrays;
import java.util.Objects;
import java.util.regex.Pattern;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
@Named("eeeRepositoryScanner")
public class EEERepositoryScanner extends BaseRepositoryScanner implements RepositoryScanner<EEERepository> {

    private static final String EPICS_BASES_DIR = "bases";
    private static final String EPICS_MODULES_DIR = "modules";
    private static final String EPICS_STARTUP_DIR = "startup";
    private static final Pattern EPICS_EEE_SNIPPET_PATTERN = Pattern.compile("(.*)\\.cmd");

    @Override
    public void setRepositoryBaseDir(File baseDir) {
        super.setRepositoryBaseDir(baseDir);
    }

    /**
     * Scans the repository
     * <p>
     * We use references and clearing of a repo because of CDI keeping the same reference, it is difficult to replace
     * it, once a Repository object is created by a producer method in the appropriate CDI scope
     *
     * @return A {@link Repository} model object, if repo param was non-null it will hold the same reference
     */
    public EEERepository scanRepository() {
        Instance<EEERepository> instance = CDI.current().select(EEERepository.class);
        EEERepository repository = instance.get();
        CDI.current().destroy(instance);

        prepareRepository(repository);

        if (isRepoDirValidDir(repository)) {
            scanEpicsBaseVersions(repository);
            scanOSVersions(repository);
            scanEEEModules(repository, createRepoDirChild(EPICS_MODULES_DIR));
        }
        return repository;
    }

    private void scanEpicsBaseVersions(EEERepository repo) {
        final File basesDir = createRepoDirChild(EPICS_BASES_DIR);
        extractEpicsBaseDirs(repo, basesDir).forEach(f ->
                repo.addEPICSVersion(VersionConverters.getEpicsConverter().fromString(
                f.getName().substring(EPICS_DIR_PREFIX.length()))));
    }

    private void scanEEEModules(EEERepository repo, File modulesDir) {
        collectVersionStrings(repo, modulesDir)
                .forEach(v -> scanEEEModule(repo, v.getDirName(), v.getVerDir().getName(), v.getVerDir()));

    }

    private void scanEEEModule(EEERepository repo, String moduleName, String moduleVersion, File moduleDir) {
        final Pattern epicsVerPattern = VersionConverters.EPICS_PATTERN;

        // Go trough each EPICS version directory. ToDo Check if EPICS version exists in scanned versions
        Arrays.stream(Objects.requireNonNull(moduleDir.listFiles())).
                filter(epicsDir -> epicsDir.isDirectory() && epicsVerPattern.matcher(epicsDir.getName()).matches()).
                forEach(epicsDir -> processLibDir(repo, moduleName, moduleVersion,
                        new File(moduleDir, EPICS_STARTUP_DIR), EPICS_EEE_SNIPPET_PATTERN, epicsDir,
                        epicsDir.getName()).forEach(repo::addModule));
    }
}
