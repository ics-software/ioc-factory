/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service;

import java.util.Date;

/***
 *
 * Generate entry filter fields wrapper -- field values can be null.
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 *
 */
public class GenerateEntryFilterValues {

    private String iocName;
    private String os;
    private String hostname;
    private String targetPath;
    private String desc;
    private String user;
    private String environment;
    private Integer configRevision;
    private Date generatedDate;
    private Boolean isProduction;
    private Boolean isLatestRevision;
    private GenerateEntryConsistencyStatus consistencyStatus;

    /**
     * @param iocName iocName column filter value (nullable)
     * @param os os column filter value (nullable)
     * @param hostname hostname column filter value (nullable)
     * @param configRevision configRevision column filter value (nullable)
     * @param user user column filter value (nullable)
     * @param generatedDate generatedDate column filter value (nullable)
     * @param environment environment column filter value (nullable)
     * @param isProduction isProduction column filter value (nullable)
     * @param isLatestRevision isLatestRevision column filter value (nullable)
     * @param targetPath targetPath column filter value (nullable)
     * @param desc desc column filter value (nullable)
     * @param consistencyStatus consistencyStatus column filter value (nullable)
     */
    public GenerateEntryFilterValues(String iocName, String os, String hostname, String targetPath,
            String desc, String user, String environment, Integer configRevision,
            Date generatedDate, Boolean isProduction, Boolean isLatestRevision,
            GenerateEntryConsistencyStatus consistencyStatus) {
        this.iocName = iocName;
        this.os = os;
        this.hostname = hostname;
        this.targetPath = targetPath;
        this.desc = desc;
        this.user = user;
        this.environment = environment;
        this.configRevision = configRevision;
        this.generatedDate = generatedDate != null ? new Date(generatedDate.getTime()) : null;
        this.isProduction = isProduction;
        this.isLatestRevision = isLatestRevision;
        this.consistencyStatus = consistencyStatus;
    }

    // getters
    public String getIocName() {
        return iocName;
    }

    public String getOs() {
        return os;
    }

    public String getHostname() {
        return hostname;
    }

    public Integer getConfigRevision() {
        return configRevision;
    }

    public String getUser() {
        return user;
    }

    public Date getGeneratedDate() {
        return generatedDate != null ? new Date(generatedDate.getTime()) : null;
    }

    public String getEnvironment() {
        return environment;
    }

    public Boolean getIsProduction() {
        return isProduction;
    }

    public Boolean getIsLatestRevision() {
        return isLatestRevision;
    }

    public String getTargetPath() {
        return targetPath;
    }

    public String getDesc() {
        return desc;
    }

    public GenerateEntryConsistencyStatus getConsistencyStatus() {
        return consistencyStatus;
    }

}
