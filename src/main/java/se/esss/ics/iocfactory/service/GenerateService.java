/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service;

import com.google.common.base.Preconditions;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.output.NullOutputStream;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import se.esss.ics.iocfactory.configuration.IOCEnvironment;
import se.esss.ics.iocfactory.configuration.IOCFactSetup;
import se.esss.ics.iocfactory.model.Configuration;
import se.esss.ics.iocfactory.model.DeviceConfiguration;
import se.esss.ics.iocfactory.model.GenerateEntry;
import se.esss.ics.iocfactory.model.IOC;
import se.esss.ics.iocfactory.model.Module;
import se.esss.ics.iocfactory.model.Parameter;
import se.esss.ics.iocfactory.model.Snippet;
import se.esss.ics.iocfactory.model.VersionConverters;
import se.esss.ics.iocfactory.util.ModuleVersionsComparator;
import se.esss.ics.iocfactory.util.Util;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.DigestOutputStream;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
@Stateless
public class GenerateService {

    private static final String NAME_PLACEHOLDER = "{iocname}";
    private static final String HOSTNAME_PLACEHOLDER = "{hostname}";
    private static final String REVISION_PLACEHOLDER = "{revisionnumber}";

    public static final String OUTPUT_FILENAME = "st.cmd";
    public static final String SHELL_OUTPUT_FILENAME = "env.sh";

    private static final String GIT_USERNAME_PROPERTY_NAME = "iocfactory.technicalNetwork.gitUsername";
    public static final String GIT_HOST_PROPERTY_NAME = "iocfactory.technicalNetwork.gitHost";

    private static final String GIT_CLONE_COMMAND = "git clone git@%s:%s/%s.git %s";

    /**
     * This command pushes to git only when there are changes.
     */
    private static final String GIT_PUSH_COMMAND =
            "cd %s"
            + " && git add --all"
            + " && { git diff-index --quiet HEAD -- || git commit -am \"%s\"; }"
            + " && git push origin master";
    public static final String GIT_RESET_COMMAND = "cd %s && git reset --hard origin/master";

    // ToDo: Using EJB annotation due bug in Wildfly 8.1
    @EJB private ConfigurationService configService;

    @Inject private CachedSetupService setupService;
    @Inject private SecurityService securityService;

    @EJB private LogService logService;

    @PersistenceContext private EntityManager em;

    private IOCFactSetup loadedSetup = new IOCFactSetup();

    private static final YamlHelper<Configuration> YAML_HELPER = new YamlHelper(true);

    /**
     * Generates IOC shell script for the given configuration and environment
     *
     * @param config the configuration object
     * @param env the environment object
     * @return true if the configuration object was frozen (if the env was production)
     *
     * @throws se.esss.ics.iocfactory.service.GenerateService.GenerationException
     */
    public boolean generateConfiguration(final Configuration config, final IOCEnvironment env, final String desc)
            throws GenerationException {

        checkPreconditions(config, env);

        loadedSetup = setupService.loadSetup();

        final File dir = new File(getDestFolder(config, env.getDirectory()));

        if (env.isGitEnvironment()) {
            // Environment does deploy to Git.
            final File repoPath = new File(getGitRepositoryFolder(config, env.getDirectory()));

            if (!repoPath.getParentFile().exists()) {
                // Create path up to the repository directory.
                if (!repoPath.getParentFile().mkdirs()) {
                    throw new GenerationException("Failed to make parent directories for the IOC deployment");
                }
            }

            if (!repoPath.exists()) {
                // Checkout GIT repository
                checkoutGitRepository(env, config.getIoc().getHostname(), repoPath.getAbsolutePath());
            }
        }

        // Create directories all the way, if they don't exist yet.
        if (!dir.exists()) {
            checkForBrokenSymlink(getDestHostnameFolder(config, env.getDirectory()));
            if (!dir.mkdirs()) {
                throw new GenerationException("Failed to make the directory for the IOC");
            }
        } else if (!dir.isDirectory()) {
            throw new GenerationException("A file named " + dir.getAbsolutePath() + " already exists.");
        }

        try {

            final File targetEnvShFile = new File(dir, SHELL_OUTPUT_FILENAME);
            writeEnvironment(config, targetEnvShFile, env.getEeeType().isE3());

            final File targetIocShFile = new File(dir, OUTPUT_FILENAME);
            writeConfiguration(config, targetIocShFile, env.getEeeType().isE3());

            writeDbEntry(config, dir, env.getName(), env.isProduction(), desc);

            if (env.isGitEnvironment()) {
                final File repoPath = new File(getGitRepositoryFolder(config, env.getDirectory()));
                pushChangesToGitRepository(repoPath, config);
            }

            if (env.isProduction()) {
                config.setCommited(true);
                configService.updateConfiguration(config);
                return true;
            } else {
                if (!config.isCommited()) {
                    // For uncommitted configurations, update the output hash also for configurations that have been
                    // deployed to non-production environments.
                    configService.updateConfiguration(config);
                }
                return false;
            }
        } catch (GenerationException e) {
            if (env.isGitEnvironment()) {
                resetGitDirectory(dir);
            }

            throw e;
        }
    }

    private void checkForBrokenSymlink(final String destHostnameFolder) throws GenerationException {
        final File symlink = new File(destHostnameFolder);
        final Path symlinkPath = symlink.toPath();
        if (Files.isSymbolicLink(symlinkPath)) {
            try {
                Path symlinkTarget = Files.readSymbolicLink(symlinkPath);
                if (Files.notExists(symlinkTarget)) {
                    Files.delete(symlinkPath);
                }
            } catch (IOException e) {
                throw new GenerationException("Failed to delete symbolic link: " + symlink.getPath());
            }
        }
    }

    private void resetGitDirectory(File dir) throws GenerationException {
        final String command = String.format(GIT_RESET_COMMAND, dir.getAbsolutePath());
        runShellCommand(command);
    }

    private void pushChangesToGitRepository(File repoPath, Configuration config) throws GenerationException {

        final int revision = config.getRevision();
        final Date lastEdit = config.getLastEdit();

        final String commitMessage =
                String.format("Configuration %d, last edited on %s", revision, lastEdit.toString());
        final String command = String.format(GIT_PUSH_COMMAND, repoPath.getAbsolutePath(), commitMessage);

        runShellCommand(command);
    }

    /**
     * Get the path to the directory that will contain the Git repository directories.
     *
     * @param directory
     * @return
     */
    private String getGitRepositoryFolder(Configuration config, String directory) throws GenerationException {
        final String[] splitPath = directory.split(Pattern.quote(HOSTNAME_PLACEHOLDER));

        if (splitPath.length == 2) {
            return splitPath[0] + config.getIoc().getHostname();
        } else {
            throw new GenerationException(
                    String.format("Could not determine filesystem Git repository path. Path is: %s", directory));
        }

    }

    private void checkoutGitRepository(IOCEnvironment env, String hostname, String filesystemLocaton)
            throws GenerationException {
        Preconditions.checkState(env.isGitEnvironment());

        final String gitHost = System.getProperty(GIT_HOST_PROPERTY_NAME);
        Preconditions.checkNotNull(gitHost);

        final String command = String.format(GIT_CLONE_COMMAND,
                                             gitHost,
                                             env.getGitRepositoryPath(),
                                             hostname,
                                             filesystemLocaton);

        // if repository unavailable or otherwise checkout not ok, encourage user to ensure repository available
        try {
            runShellCommand(command);
        } catch (GenerationException e) {
            throw new GenerationException(
                    "Failed to clone remote repository. Please ensure that repository " + hostname
                        + " is available at server " + gitHost + " in folder " + env.getGitRepositoryPath() + ".",
                    e);
        }
    }

    private void runShellCommand(String command) throws GenerationException {
        final ProcessBuilder processBuilder = new ProcessBuilder();

        processBuilder.command("bash", "-c", command);

        try {
            final Process process = processBuilder.start();

            final BufferedReader errorStream = new BufferedReader(new InputStreamReader(
                    process.getErrorStream()));

            final boolean processFinished = process.waitFor(30, TimeUnit.SECONDS);

            if (!processFinished) {
                throw new GenerationException("Timed out waiting to execute command: " + command);
            }

            if (process.exitValue() != 0) {
                throw new GenerationException(String.format("Shell command finished with status %d error: %s",
                                                            process.exitValue(),
                                                            errorStream.lines().collect(Collectors.joining())));
            }
        } catch (IOException | InterruptedException e) {
            throw new GenerationException(e);
        }
    }

    public Pair<String, String> previewConfiguration(Configuration config, boolean isE3) throws GenerationException {
        loadedSetup = setupService.loadSetup();
        final String envConfig;
        final String stConfig;

        try (final StringWriter writer = new StringWriter()) {
            writeEnvironment(config, writer, isE3);
            envConfig = writer.toString();
        } catch (IOException e) {
            throw new GenerationException("IO error while generating preview for the " + SHELL_OUTPUT_FILENAME, e);
        }

        try (final StringWriter writer = new StringWriter()) {
            writeConfiguration(config, writer, isE3);
            stConfig = writer.toString();
        } catch (IOException e) {
            throw new GenerationException("IO error while generating preview for the " + OUTPUT_FILENAME, e);
        }

        return new ImmutablePair<>(envConfig, stConfig);
    }

    /**
     * Undeploys the generated configuration: deletes all the generated IOC shell scripts. Also deletes parent
     * directories if they get empty.
     *
     * @param generateEntry generated configuration to undeploy.
     * @param isGitEnvironment true if the environment pushes to a Git repository, false if not
     * @throws GenerationException
     */
    public void undeployConfiguration(final GenerateEntry generateEntry, boolean isGitEnvironment)
            throws GenerationException {
        try {
            File directoryToDelete = new File(generateEntry.getTargetPath());

            do {
                FileUtils.deleteDirectory(directoryToDelete);
                // If the parent directory becomes empty after the deletion, delete that as well.
                directoryToDelete = directoryToDelete.getParentFile();

            } while (directoryToDelete != null && directoryToDelete.list().length == 0);

            if (isGitEnvironment) {
                try {
                    final String command = String.format(GIT_PUSH_COMMAND,
                                                         directoryToDelete.getAbsolutePath(),
                                                         "Undeployed configuration");
                    runShellCommand(command);
                } catch (GenerationException e) {
                    resetGitDirectory(directoryToDelete);

                    throw e;
                }
            }
        } catch (IOException e) {
            throw new GenerationException("Failed to delete the directory containing the generated configuration.", e);
        }
    }

    /** Calculates a configuration output hash (from generated env.sh and st.cmd) contents
     *
     * @param config the fully loaded configuration for which to calculate
     * @param isE3 true to calculate hash for E3 or false for EEE
     * @return the hex encoded SHA-1 hash of the output or {@code null} if an exception was thrown
     */
    public String calculateConfigurationOutputHash(Configuration config, boolean isE3) {
        loadedSetup = setupService.loadSetup();
        final String envConfig;
        final String stConfig;

        final MessageDigest digest = Util.getSHA1MessageDigest();

        final DigestOutputStream dos = new DigestOutputStream(new NullOutputStream(), digest);
        try (PrintWriter writer = new PrintWriter(new OutputStreamWriter(dos, StandardCharsets.US_ASCII), false)) {
            writeEnvironment(config, writer, isE3);
            writeConfiguration(config, writer, isE3);
        } catch(GenerationException | IOException e) {
            return "";
        }

        return Hex.encodeHexString(digest.digest());
    }

    public static String getDestFolder(final Configuration config, final String dirPattern) {
        final String hostname = config.getIoc().getHostname();
        final String iocName = config.getIoc().getName();

        String result = dirPattern;
        if (!StringUtils.isEmpty(hostname)) {
            result = result.replace(HOSTNAME_PLACEHOLDER, hostname);
        }

        if (!StringUtils.isEmpty(iocName)) {
            result = result.replace(NAME_PLACEHOLDER, iocName.replace(":", "_"));
        }

        result = result.replace(REVISION_PLACEHOLDER, Integer.toString(config.getRevision()));

        return result;
    }

    private static String getDestHostnameFolder(final Configuration config, final String dirPattern) {
        final String hostname = config.getIoc().getHostname();
        String result = dirPattern.substring(0, dirPattern.lastIndexOf(HOSTNAME_PLACEHOLDER)) + HOSTNAME_PLACEHOLDER;
        if (!StringUtils.isEmpty(hostname)) {
            result = result.replace(HOSTNAME_PLACEHOLDER, hostname);
        }

        return result;
    }

    private static String escapePlaceholder(String placeholder) {
        return placeholder.replace("{", "").replace("}", "");
    }

    private void checkPreconditions(Configuration config, IOCEnvironment env) throws GenerationException {
        final String dirString = env.getDirectory();
        if (StringUtils.isEmpty(dirString)) {
            throw new GenerationException("IOC Environment " + escapePlaceholder(env.getName()) +
                    " has empty directory specificaton");
        } else if (!dirString.contains(NAME_PLACEHOLDER)) {
            throw new GenerationException("The IOC environment directory specification"
                + " does not contain the " + escapePlaceholder(NAME_PLACEHOLDER) + " placeholder.");
        } else if (config.getIoc()==null || StringUtils.isEmpty(config.getIoc().getName())) {
            throw new GenerationException("IOC is not specified");
        } else if (StringUtils.isEmpty(config.getIoc().getHostname())) {
            throw new GenerationException("Hostname for the IOC is not set or it has been changed and IOC still has "
                              + "deployed configurations on old hostname. Set the hostname or "
                              + "undeploy old configurations in the Generate IOC tab.");
        }
    }

    private void writeEnvironment(Configuration config, File targetEnvShFile, boolean isE3) throws GenerationException {
        try (OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(targetEnvShFile),
                StandardCharsets.US_ASCII)) {
            writeEnvironment(config, writer, isE3);
        } catch (IOException e) {
            throw new GenerationException("IO error while writing " + SHELL_OUTPUT_FILENAME, e);
        }
    }

    private void writeEnvironment(Configuration config, Writer writer, boolean isE3) throws IOException {
        writer.append(String.format("PROCSERV_PORT=%d\n", config.getProcServPort()));
        writer.append(String.format("EPICS_BASE=%s\n", constructEpicsBasePath(config, isE3) ));
        writer.append(String.format("EPICS_HOST_ARCH=%s\n",config.getOs()));
        if (!isE3) {
            writer.append(String.format("EPICS_MODULES_PATH=%s\n", constructModulesBasePath(config)));
            writer.append(
                    String.format(
                            "ENVIRONMENT_VERSION=%s\n",
                            config.getEnvVersion() == null ? getEnvironmentModuleLatestVersion()
                                    : config.getEnvVersion()));
        }
        if (isE3) {
            writer.append(String.format("REQUIRE_VERSION=%s\n", config.getRequireVersion()));
        }
        writer.append(
                String.format(
                        "BASE=%s\n",
                        VersionConverters.getEpicsConverter().toString(config.getEpicsVersion())));
        if (isE3) {
            writer.append(String.format("export IOCNAME=%s\n", config.getIoc().getName()));
            writer.append(String.format("export IOCDIR=%s\n", getSlugVariableForIOCName(config.getIoc())));
        }
    }

    private String getSlugVariableForIOCName(IOC ioc) {
        return ioc != null && ioc.getName() != null ? ioc.getName().replace(":", "_") : "";
    }

    private String constructEpicsBasePath(Configuration config, boolean isE3) {
        // different base path for Epics E3 and EEE
        return (
                new File(
                        isE3
                            ? new File(loadedSetup.getEpicsE3BaseDirectory())
                            : new File(loadedSetup.getEpicsBaseDirectory(), "bases"),
                        "base-" + VersionConverters.getEpicsConverter().toString(config.getEpicsVersion())))
                .getAbsolutePath();
    }

    private String getEnvironmentModuleLatestVersion() {
        final Optional<String> lastVersion = Util.getRepository().getModulesForName("environment").stream().
                map(Module::getModuleVersion).max((a, b) -> ModuleVersionsComparator.compare(a,b));

        return lastVersion.isPresent() ? lastVersion.get() : "";
    }

    private String constructModulesBasePath(Configuration config) {
        return (new File(loadedSetup.getEpicsBaseDirectory(), "modules")).getAbsolutePath();
    }

    private void writeConfiguration(Configuration config, File targetIocShFile, boolean isE3)
            throws GenerationException {
        try (OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(targetIocShFile),
                StandardCharsets.US_ASCII)) {
            writeConfiguration(config, writer, isE3);
        } catch (IOException e) {
            throw new GenerationException("IO error while writing IOCSH script", e);
        }
    }

    private void writeConfiguration(Configuration config, Writer writer, boolean isE3)
            throws IOException, GenerationException {
        writeHeaderComment(config, writer);

        writeGlobals(config, writer);

        if (isE3) {
            // module "common" renamed to "essioc"
            //     in epics version 7.0.5 (7000005000), require version 3.4.0 (3004000000), onwards
            Long epicsVersion = config.getEpicsVersion();
            String requireVersion = config.getRequireVersion();
            Long requireVersionLong = requireVersion != null
                    ? VersionConverters.getEpicsConverter().fromString(requireVersion)
                    : null;
            boolean e3EssIoc = epicsVersion != null && epicsVersion >= 7000005000l
                    && requireVersionLong != null && requireVersionLong >= 3004000000l;

            if (e3EssIoc) {
                writeRequireEssIoc(writer);
            } else {
                writeRequireCommon(writer);
            }
        }

        for (DeviceConfiguration dev : config.getDevices()) {
            // Write the device config only if including this device is enabled.
            if (dev.getEnableInclude()) {
                writeDeviceConfig(dev, writer, isE3);
            }
        }
    }

    private void writeHeaderComment(Configuration config, Writer writer) throws GenerationException {
        try {
            writer.write("#\n");
            writer.write(String.format("# IOC: %s\n", config.getIoc()));
            writer.write(String.format("# Revision: %d\n", config.getRevision()));
            writer.write(String.format("# OS: %s, EPICS: %s\n#\n", config.getOs(),
                    VersionConverters.getEpicsConverter().toString(config.getEpicsVersion())));
            writer.write("#\n");
            writer.write(String.format("# Description: %s\n", config.getComment()));
            writer.write("#\n#\n\n");
        } catch (IOException ex) {
            throw new GenerationException(String.format("IO Error while writing configuration header for config id %d, "
                    + "IOC %s, revision %d", config.getId(), config.getIoc().getName(), config.getRevision()), ex);
        }
    }

    private void writeGlobals(Configuration config, Writer writer) throws IOException {
        writer.write(String.format("#\n# GLOBAL ENVIRONMENT\n#\n"));
        writeParameters(config.getGlobals(), writer, null);
        writer.write("\n\n");
    }

    private void writeRequireCommon(Writer writer) throws IOException {
        writer.write(String.format("#\n# COMMON\n#\n\n"));
        writer.write("require common\n");
        writer.write("iocshLoad(\"$(common_DIR)e3-common.iocsh\")");
        writer.write("\n\n");
    }

    private void writeRequireEssIoc(Writer writer) throws IOException {
        writer.write(String.format("#\n# ESSIOC\n#\n\n"));
        writer.write("require essioc\n");
        writer.write("iocshLoad(\"$(essioc_DIR)common_config.iocsh\")");
        writer.write("\n\n");
    }

    private void writeDeviceConfig(DeviceConfiguration dev, Writer writer, boolean isE3) throws IOException {
        // Do not output devices with errors
        if (dev.getConsistencyStatus().isError()) {
            return;
        }

        final boolean outputModule = dev.getSupportModule().isDefined();
        final boolean outputSnippets = !dev.getSnippets().isEmpty();

        if (outputModule || outputSnippets) {
            writer.write(String.format("#\n# Device: %s\n", dev.getName()));
        }

        if (outputModule) {
            final String modName  = dev.getSupportModule().getName();

            final String modVersion = dev.getSupportModule().getModuleVersion();
            writer.write(
                    String.format(
                            "# Module: %s %s\n#   Deps:   %s\n#\n",
                            modName,
                            modVersion,
                            Util.formatModuleDependencies(dev.getSupportModule())));
            if (Module.LATEST_MODULE_VERSION.equals(modVersion)) {
                writer.write(String.format("\n\nrequire %s\n\n", modName));
            } else {
                writer.write(String.format("\n\nrequire %s, %s\n\n", modName, modVersion));
            }
        }

        if (outputSnippets) {
            for (final Snippet snippet : dev.getSnippets()) {
                if (isE3) {
                    writeSnippetE3(dev, writer, snippet);
                } else {
                    writeSnippetEEE(dev, writer, snippet);
                }
            }
        }
    }

    private void writeSnippetEEE(DeviceConfiguration dev, final Writer writer, final Snippet snippet)
            throws IOException {
        writeParameters(dev.getParameters(), writer, snippet);
        final String snippetPath = snippet.getFile().getAbsolutePath();
        if (!StringUtils.isEmpty(snippetPath)) {
            writer.write(String.format("< %s\n\n", snippetPath));
        }
    }

    private void writeSnippetE3(DeviceConfiguration dev, final Writer writer, final Snippet snippet)
            throws IOException {
        final String snippetPath = snippet.getFile().getAbsolutePath();
        if (StringUtils.isNotEmpty(snippetPath)) {
            writer.write(String.format("iocshLoad(\"%s\", ", snippetPath));
            if(dev.getParameters().size() > 0) {
                writer.write("\"");
            }
            writeE3Parameters(dev.getParameters(), writer, snippet);
            if(dev.getParameters().size() > 0) {
                writer.write("\"");
            }
            writer.write(")");
            writer.write("\n\n");
        }
    }

    /**
     * Writes parameters for a given snippet or the global parameters if the snippet is null
     *
     * @param parameters
     * @param writer
     * @param snippet
     * @throws IOException
     */
    private void writeParameters(
            final List<Parameter> parameters, final Writer writer, final Snippet snippet)
            throws IOException {
        for (Parameter param : parameters) {
            if (snippet == null || Objects.equals(param.getSnippet(), snippet)) {
                writeParameterEEE(param, writer);
            }
        }
    }

    private void writeE3Parameters(
            final List<Parameter> parameters, final Writer writer, final Snippet snippet)
            throws IOException {
        List<String> params = new ArrayList<>();
        for (Parameter param : parameters) {
            if (snippet == null || Objects.equals(param.getSnippet(), snippet)) {
                if (StringUtils.isNotEmpty(param.getValue())) {
                    params.add(String.format("%s=%s", param.getName(), param.getValue()));
                }
            }
        }
        writer.write(StringUtils.join(params, ", "));
    }

    private void writeParameterEEE(final Parameter param, final Writer writer) throws IOException {
        final String paramValue = param.getValue();
        writer.write(String.format("epicsEnvSet(%s, \"%s\")\n", param.getName(),
                paramValue != null ? paramValue : ""));
    }

    private void writeDbEntry(Configuration config, File dir, String envName, boolean inProd, String desc)
            throws GenerationException {
        final Pair<String, String> configDumpAndDigest;
        try {
            configDumpAndDigest = YAML_HELPER.getEntityDumpAndDigest(config);
        } catch (IOException ex) {
            throw new GenerationException("Failed while exporting the configuration to JSON and computing the SHA1 "
                    + "hash", ex);
        }
        final GenerateEntry entry = new GenerateEntry(
                config.getIoc().getName(),
                config.getIoc().getOs(),
                config.getIoc().getHostname(),
                config.getId(),
                config.getRevision(),
                securityService.getUsername(),
                new Date(),
                envName,
                inProd,
                dir.getAbsolutePath(),
                configDumpAndDigest.getRight(),
                configDumpAndDigest.getLeft(),
                desc
        );
        em.persist(entry);

        // Audit log entry
        logService.logConfigGenerated(config, entry, desc);
    }

    /**
     * Utility class for handling unsuccessful deploy configuration.
     */
    public static class GenerationException extends Exception {
        private GenerationException ge;

        public GenerationException(String message) {
            super(message);
        }
        public GenerationException(String message, Throwable cause) {
            super(message, cause);
        }
        public GenerationException(String message, GenerationException ge) {
            super(message);
            this.ge = ge;
        }
        public GenerationException(Exception e) {
            super(e);
        }

        public GenerationException getGenerationException() {
            return this.ge;
        }
    }

}
