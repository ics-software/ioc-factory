/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service;

import static se.esss.ics.iocfactory.model.IOCConsistencyStatus.HOSTNAME_CHANGED;
import static se.esss.ics.iocfactory.model.IOCConsistencyStatus.INVALID_OS;
import static se.esss.ics.iocfactory.model.IOCConsistencyStatus.IOC_NAME_CHANGED;
import static se.esss.ics.iocfactory.model.IOCConsistencyStatus.IOC_NOT_IN_CCDB;
import static se.esss.ics.iocfactory.model.IOCConsistencyStatus.NEW_IOC;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import se.esss.ics.iocfactory.model.Configuration;
import se.esss.ics.iocfactory.model.ConsistencyStatus;
import se.esss.ics.iocfactory.model.GenerateEntry;
import se.esss.ics.iocfactory.model.IOC;
import se.esss.ics.iocfactory.model.IOCConsistencyStatus;
import se.esss.ics.iocfactory.model.Repository;
import se.esss.ics.iocfactory.service.cdievents.IOCRemoved;
import se.esss.ics.iocfactory.util.Util;

/**
 * A service that provides access to the IOC configurations stored in the DB
 *
 * @author <a href=mailto:miroslav.pavleski@cosylab.com>Miroslav Pavleski</a>
 *
 */
@Stateless
public class IOCService {

    @PersistenceContext
    private EntityManager em;

    @Inject
    private transient ConfigClientService configClient;

    @Inject
    private transient GenerateEntryService generateEntryService;

    @Inject
    private transient ConfigurationService configService;

    @Inject
    @IOCRemoved
    private transient Event<IOC> iocRemovedEvent;

    /**
     * Gets JPA managed version of an IOC
     *
     * The method checks in DB if there is already an IOC like that (name & os).
     *
     * @param ioc
     *            the IOC object
     * @return managed JPA ioc object
     */
    public IOC saveIOC(IOC ioc) {
        IOC result;
        if (ioc.getNameId() == null) {
            result = (IOC) Util.getSingleResult(
                    em.createQuery("select ioc from IOC ioc where " + "ioc.name = :ioc_name")
                            .setParameter("ioc_name", ioc.getName()));
        } else {
            result = (IOC) Util.getSingleResult(
                    em.createQuery("select ioc from IOC ioc where " + "ioc.nameId = :ioc_name_id")
                            .setParameter("ioc_name_id", ioc.getNameId()));
        }
        if (result == null) {
            // New IOC is being added to the database.
            result = em.merge(ioc);
        }

        if (result.getNameId() == null) {
            // If the IOC from database doesn't have the nameId assigned, assign it.
            result.setNameId(ioc.getNameId());
        } else if (!result.getName().equals(ioc.getName())) {
            // If the IOC name changed
            result = renameIOC(result, ioc.getName());
        }
        // Update transient stuff
        updateTransientIOCProperties(ioc, result);
        return result;
    }

    private IOC renameIOC(IOC ioc, String newIocName) {
        // Since "name" is also IOC's primary key and it is impossible to update the primary key in JPA
        // (see https://stackoverflow.com/questions/21018728/
        // jpa-hibernate-changing-the-primary-key-of-an-persisted-object),
        // we have to create new IOC and delete the old one.
        IOC iocRenamed = new IOC(newIocName);
        iocRenamed.setNameId(ioc.getNameId());
        iocRenamed.setLastConfigRevision(ioc.getLastConfigRevision());
        em.merge(iocRenamed);
        // Force the IOC to be created in the database because it needs to be present before the re-wiring of its
        // configurations.
        em.flush();
        em.createQuery("UPDATE Configuration c SET c.ioc.name = :newIocName WHERE c.ioc = :ioc")
                .setParameter("ioc", ioc)
                .setParameter("newIocName", newIocName)
                .executeUpdate();
        // Remove old IOC from the database. The code is somewhat more complex due to the reason described below:
        // https://stackoverflow.com/questions/17027398/
        // java-lang-illegalargumentexception-removing-a-detached-instance-com-test-user5
        em.remove(ioc);
        return iocRenamed;
    }

    public static void updateTransientIOCProperties(IOC src, IOC dest) {
        dest.setDesc(src.getDesc());
        dest.setOs(src.getOs());
        dest.setHostname(src.getHostname());
        dest.setType(src.getType());

        dest.getConsistencyStatus().clear();
        src.getConsistencyStatus().forEach(cs -> dest.getConsistencyStatus().add(cs));
    }

    /**
     * Loads and processes all IOCs from database, merging information with the CCDB
     *
     * @return a pair containing both list and map (by ioc name) representation of the IOCS
     *
     * @throws ConfigClientException
     */
    public List<IOC> getIOCs() throws ConfigClientException {
        // Get IOCs from DB & detach entities
        final List<IOC> dbIOCs = (List<IOC>) em.createQuery("from IOC i", IOC.class).getResultList();


        dbIOCs.stream().forEach(ioc -> em.detach(ioc));

        // Get IOCs from CCDB
        final List<IOC> ccdbIOCs = configClient.getAllIOCs();

        // Update DB IOCs transient properties from the CCDB ones
        ccdbIOCs.stream().forEach(ccdbIOC -> {
            int indexDbIOC = dbIOCs.indexOf(ccdbIOC);
            if (indexDbIOC != -1) {
                updateTransientIOCProperties(ccdbIOC, dbIOCs.get(indexDbIOC));
            }
        });

        // Make sure to add IOCs in CCDB not in DB to the DB map
        ccdbIOCs.stream()
                .filter(ccdbIOC -> !dbIOCs.contains(ccdbIOC))
                .forEach(ccdbIOC -> {
                    ccdbIOC.getConsistencyStatus().add(NEW_IOC);
                    dbIOCs.add(ccdbIOC);
                });

        // update in CCDB for all IOCs in the map
        dbIOCs.forEach(ioc -> {
            final Optional<IOC> ccdbIOCResult = ccdbIOCs.stream().filter(ccdbIoc -> ccdbIoc.equals(ioc)).findFirst();
            if (ccdbIOCResult.isPresent()) {
                if (StringUtils.isEmpty(ioc.getNameId())) {
                    // if the IOC from database doesn't have the nameId assigned assign it the one from the CCDB.
                    ioc.setNameId(ccdbIOCResult.get().getNameId());
                    saveIOC(ioc);
                }

                // if names are not equal, try to rename it.
                if (!ccdbIOCResult.get().getName().equals(ioc.getName())) {
                    List<GenerateEntry> generateEntriesForIoc =
                            generateEntryService.getGenerateEntriesPresentInFileSystem(ioc.getName());
                    if (generateEntriesForIoc.isEmpty()) {
                        // If the IOC doesn't have any deployed configurations, change its name automatically.
                        ioc.setName(ccdbIOCResult.get().getName());
                        saveIOC(ioc);
                    } else {
                        // If the IOC has deployed configurations, warn the user about the name change.
                        ioc.getConsistencyStatus().add(IOC_NAME_CHANGED);
                    }
                }

                ioc.setOs(ccdbIOCResult.get().getOs());
            } else {
                ioc.getConsistencyStatus().add(IOC_NOT_IN_CCDB);
            }
        });

        // Check if the hostname changed.
        dbIOCs.forEach(ioc -> {
            List<GenerateEntry> generateEntriesForIoc = generateEntryService.getGenerateEntriesPresentInFileSystem(
                    ioc.getName());
            Set<String> listOldHostnames = generateEntriesForIoc.stream()
                    .filter(generateEntry -> !generateEntry.getHostname().equals(ioc.getHostname()))
                    .map(generateEntry -> generateEntry.getHostname())
                    .collect(Collectors.toSet());
            if (!listOldHostnames.isEmpty()) {
                String warningDesc = String.format("%s. Old hostnames: %s", HOSTNAME_CHANGED.getDesc(),
                        String.join(", ", listOldHostnames));
                ioc.getConsistencyStatus().add(new ConsistencyStatus(HOSTNAME_CHANGED, warningDesc));
            }
        });

        // Check OS
        final Repository repo = Util.getRepository();
        dbIOCs.stream().forEach((IOC ioc) -> {
            if (!repo.getSupportedOsVersions().contains(ioc.getOs())) {
                ioc.getConsistencyStatus().add(INVALID_OS);
            }
        });

        return dbIOCs;
    }

//    private void updateNameId(IOC src, IOC dest) {
//        dest.setNameId(src.getNameId());
//        em.merge(dest);
//    }

    /**
     * Counts the number of configurations in the DB for a given IOC name
     *
     * @param iocName
     * @return number of configurations in the db with name iocname
     */
    public Long countIOCDbConfigs(String iocName) {
        return (Long) em.createQuery("SELECT COUNT(c) FROM Configuration c WHERE c.ioc.name = :iocName")
                .setParameter("iocName", iocName).getSingleResult();
    }

    /**
     * Deletes an IOC.
     *
     * Only possible for IOCs that have been fully loaded by this service and has
     * {@link IOCConsistencyStatus#IOC_NOT_IN_CCDB} status
     *
     * @param ioc
     * @return true if the delete operation succeeds
     */
    public boolean deleteIOC(IOC ioc) {
        if (ioc == null || !ioc.getConsistencyStatus().contains(IOC_NOT_IN_CCDB)) {
            return false;
        }

        // Get all configurations for the IOC and delete them (Cascade on JPA level will clear all dep. entities)
        final List<Configuration> configs = (List<Configuration>) em
                .createQuery("SELECT c FROM Configuration c WHERE " + "c.ioc.name = :iocName")
                .setParameter("iocName", ioc.getName()).getResultList();
        if (configs != null) {
            configs.forEach(config -> em.remove(config));
        }

        final IOC merged = em.merge(ioc);
        em.remove(merged);

        iocRemovedEvent.fire(ioc);

        return true;
    }

}
