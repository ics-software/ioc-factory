/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service;

import java.util.Date;

import se.esss.ics.iocfactory.model.log.LogEntryType;

/***
 *
 * Log table filter fields wrapper -- field values can be null.
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 *
 */
public class LogFilterValues {

    private String details;
    private String user;
    private Date logTimestamp;
    private LogEntryType type;


    /**
     * @param details details column filter value (nullable)
     * @param user user column filter value (nullable)
     * @param logTimestamp timestamp column filter value (nullable)
     * @param type type column filter value (nullable)
     */
    public LogFilterValues(String details, String user, Date logTimestamp, LogEntryType type) {
        this.details = details;
        this.user = user;
        this.logTimestamp = logTimestamp != null ? new Date(logTimestamp.getTime()) : null;
        this.type = type;
    }


    //getter methods
    public String getDetails() {
        return details;
    }
    public String getUser() {
        return user;
    }
    public Date getLogTimestamp() {
        return logTimestamp != null ? new Date(logTimestamp.getTime()) : null;
    }
    public LogEntryType getType() {
        return type;
    }

}
