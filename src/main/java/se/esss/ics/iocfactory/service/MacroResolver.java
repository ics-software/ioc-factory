/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Multimap;
import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;
import org.apache.commons.lang3.StringUtils;
import se.esss.ics.iocfactory.model.Configuration;
import se.esss.ics.iocfactory.model.MacroExpansion;
import se.esss.ics.iocfactory.model.Parameter;
import se.esss.ics.macrolib.MacroException;
import se.esss.ics.macrolib.MacroLib;
import se.esss.ics.macrolib.MacroOccurrence;

/**
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
public class MacroResolver implements Serializable {
    private static final Logger LOG = Logger.getLogger(MacroResolver.class.getName());

    private Multimap<String, Parameter> paramsByName = ArrayListMultimap.create();

    private final transient MacroLib macroLib = new MacroLib();
    private Configuration config;

    public void setConfiguration(Configuration config) {
        this.config = config;

        paramsByName = ArrayListMultimap.create();

        getJoinedParamsStream().forEach(param -> paramsByName.put(param.getName().toUpperCase(), param) );
    }

    public void resolveMacros() {
        resolveMacrosAfter(null, p -> {});
    }

    public void resolveMacrosAfter(Parameter startingParam) {
        resolveMacrosAfter(startingParam, p -> {});
    }

    public void resolveMacrosAfter(Parameter startingParam, Consumer<Parameter> processor) {
        getJoinedParamsStream().filter(paramsFilter(startingParam)).
                forEach(param -> {
            param.setExpandedValue(Collections.emptyList());

            if (!StringUtils.isEmpty(param.getValue())) {
                param.setExpandedValue( ImmutableList.of(safeGetParamExpandedValue(param, param.getValue(),
                        macroResolver(param),null)) );
            } else if (param.getMacroEntry() != null) {
                final ImmutableList.Builder<MacroExpansion> defValueListBuilder = ImmutableList.builder();
                param.getMacroEntry().getOccurrences().stream().
                        filter(o -> o.getDefValue() != null).forEach(occurrence ->
                            defValueListBuilder.add(safeGetParamExpandedValue(param,
                                    occurrence.getDefValue().getRawValue(),
                                    macroResolver(param), occurrence)) );
                param.setExpandedValue(defValueListBuilder.build());
            }
            processor.accept(param);
        });
    }

    private MacroExpansion safeGetParamExpandedValue(Parameter param, String line, Function<String, String> resolver,
            MacroOccurrence occurrence) {
        try {
            final String expandededValue = macroLib.expandMacros(line, resolver);

            final MacroExpansion errExpansion = MacroValueChecker.checkParameterValue(param, expandededValue,
                    occurrence);
            if (errExpansion != null) {
                return errExpansion;
            }

            return new MacroExpansion(expandededValue, occurrence);
        } catch(MacroException ex) {
            LOG.log(Level.FINER, ex.getMessage(), ex);
            return new MacroExpansion(null, occurrence, ex.getMessage());
        }
    }

    private Stream<Parameter> getJoinedParamsStream() {
        return  Stream.concat(
                config.getGlobals().stream(),
                config.getDevices().stream().flatMap(dev -> dev.getParameters().stream())
        );
    }

    /** Returns a function that will resolve a given macro before a given param
     *
     * @param param the param
     * @return
     */
    private Function<String, String> macroResolver(final Parameter param) {
        return name -> {
            Collection<Parameter> paramsList = paramsByName.get(name);
            Optional<String> val = paramsList.stream().
                    filter(p -> {
                        boolean isGlobal = param.getDeviceConfiguration() == null || p.getDeviceConfiguration() == null;
                        if (isGlobal) {
                            return p.getParamPosition() < param.getParamPosition();
                        } else {
                            // If both the parameter for which we resolve and the parameter found are in devices
                            // count the resolved parameter only if it is defined in the current or the previous devices
                            return p.getDeviceConfiguration().getDevicePosition() <=
                                    param.getDeviceConfiguration().getDevicePosition();
                        }
                    }).
                    filter(p -> hasNonDefaultExpandedValue(p)).
                    max((l, r) -> Integer.compare(l.getParamPosition(), r.getParamPosition())).
                    map(p -> getNonDefaultExpanedValue(p));
            return val.orElse(null);
        };
    }

    private static boolean hasNonDefaultExpandedValue(Parameter p) {
        return !StringUtils.isEmpty(getNonDefaultExpanedValue(p));
    }

    private static String getNonDefaultExpanedValue(Parameter p) {
        if (p.getExpandedValues().size() == 1) {
            final MacroExpansion firstVal = p.getExpandedValues().get(0);
            if (!firstVal.isDefalutValue() && !firstVal.isError()) {
                return firstVal.getExpandedValue();
            }
        }
        return null;
    }

    private Predicate<? super Parameter> paramsFilter(Parameter startingParam) {
        return param ->  startingParam == null ? true : param.getParamPosition() >= startingParam.getParamPosition();
    }
}


