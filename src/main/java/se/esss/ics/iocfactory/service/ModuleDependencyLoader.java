/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service;

import com.google.common.base.Throwables;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import se.esss.ics.iocfactory.model.ModuleDependency;
import se.esss.ics.iocfactory.model.Repository;

/**
 * This class loads dependencies for a given module form the repository
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 *
 */
public class ModuleDependencyLoader {
    private static final Pattern DEPENDENCY_PATTERN = Pattern.compile("(.*?)(?:[\\,|\" \"](.*?)(\\+)?)?");
    private static final String ERROR_MESSAGE = "Invlaid line in file, line %d";
    private static final Logger LOG = Logger.getLogger(ModuleDependencyLoader.class.getCanonicalName());

    /** Private constructor to prevent class instantiation */
    private ModuleDependencyLoader() {}

    /**
     * Given a {@link File} in dep format, loads a list of {@link ModuleDependency} from it
     *
     * @param depFile the dep file
     * @return <code>null</code> if the file does not exist, or a list of {@link ModuleDependency}
     */
    public static List<ModuleDependency> loadModuleDependencies(final Repository repo, final File depFile) {
        final Builder<ModuleDependency> listBuilder = ImmutableList.builder();

        if (!depFile.exists()) {
            return listBuilder.build();
        }

        try (final BufferedReader reader = new BufferedReader(new InputStreamReader(
                new FileInputStream(depFile), StandardCharsets.US_ASCII))) {
            String line;
            int lineNum = 0;
            while ((line = reader.readLine()) != null) {
                lineNum++;


                final ModuleDependency dependency = parseDependency(repo, depFile, line, lineNum);
                // null return means it was a comment line
                if (dependency != null) {
                    listBuilder.add(dependency);
                }
            }
        } catch (IOException e) {
            LOG.severe(Throwables.getStackTraceAsString(e));
        }

        return listBuilder.build();
    }

    /**
     * Parses a dep file line
     *
     * @param line the {@link String} contents of the line
     * @param lineNum line number used for diagnostic purposes
     * @return
     */
     static ModuleDependency parseDependency(final Repository repo, final File depFile,
             final String line, int lineNum) {
        // Check if comment line
        final String trimmedLine = line.trim();
        if (trimmedLine.length()==0 || trimmedLine.startsWith("#")) {
            return null;
        }

        final Matcher matcher = DEPENDENCY_PATTERN.matcher(trimmedLine);
        if (!matcher.matches()) {
            throw new RuntimeException(String.format(ERROR_MESSAGE, lineNum));
        }

         final String moduleName = matcher.group(1);
         String moduleVersion = matcher.group(2);
         String moduleGreaterSign = matcher.group(3);

        if (moduleName==null) {
            throw new RuntimeException(String.format(ERROR_MESSAGE, lineNum));
        }

        // If moduleVersion is not defined then it is implicit equal than zero
        if (moduleVersion==null) {
            moduleGreaterSign = "";
        }

        return new ModuleDependency(moduleName,
                moduleVersion!=null ? moduleVersion : "",
                moduleGreaterSign != null && "+".equals(moduleGreaterSign));
    }
}
