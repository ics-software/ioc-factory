/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package se.esss.ics.iocfactory.service;


import se.esss.ics.iocfactory.model.dto.OSAliasDto;

import javax.management.openmbean.KeyAlreadyExistsException;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * @author Imre Toth <imre.toth@ess.eu>
 **/
public interface OSAliasService {

    List<String> searchForEEEOSName(String e3Name);
    List<String> searchForE3OSName(String eeeName);
    List<OSAliasDto> getAllOSAlias();

    void deleteOSAlias(Long id, String userName) throws IllegalArgumentException, NoSuchElementException;
    void createAlias(String e3OSAlias, String eeeOSAlias, String userName) throws KeyAlreadyExistsException, IllegalArgumentException ;
    void updateAlias(OSAliasDto aliasDto, String userName) throws KeyAlreadyExistsException, IllegalArgumentException, NoSuchElementException;
}
