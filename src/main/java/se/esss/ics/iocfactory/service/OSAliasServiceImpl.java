/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package se.esss.ics.iocfactory.service;

import org.apache.commons.lang3.StringUtils;
import se.esss.ics.iocfactory.dao.OSAliasDao;
import se.esss.ics.iocfactory.model.OSAlias;
import se.esss.ics.iocfactory.model.dto.OSAliasDto;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.management.openmbean.KeyAlreadyExistsException;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.lang.String.format;

/**
 * @author Imre Toth <imre.toth@ess.eu>
 **/
@Stateless
public class OSAliasServiceImpl implements OSAliasService {

    private static final Logger LOG = Logger.getLogger(OSAliasServiceImpl.class.getName());

    @Inject
    private OSAliasDao osAliasDao;

    /**
     * Lists aliases from DB with a specific e3 OS name
     *
     * @param e3Name specific e3 name that has to be queried in DB
     * @return List of eee OS name, or empty list
     */
    @Override
    public List<String> searchForEEEOSName(String e3Name) {
        List<String> result = new ArrayList<>();

        List<OSAlias> osAliases = osAliasDao.searchForEEEOSName(e3Name);

        if ((osAliases != null) && (!osAliases.isEmpty())) {
            osAliases.forEach(
                    o -> result.add(o.getEEEOSName())
            );
        }

        return result;
    }

    /**
     * Lists aliases from DB with a specific eee OS name
     *
     * @param eeeName specific eee name that has to be queried in DB
     * @return List of e3 OS name, or empty list
     */
    @Override
    public List<String> searchForE3OSName(String eeeName) {
        List<String> result = new ArrayList<>();

        List<OSAlias> osAliases = osAliasDao.searchForE3OSName(eeeName);

        if ((osAliases != null) && (!osAliases.isEmpty())) {
            osAliases.forEach(
                    o -> result.add(o.getE3OSName())
            );
        }

        return result;
    }

    /**
     * Lists all OS aliases from DB to show it on UI
     *
     * @return List of OS Aliases, or empty list, if the DB table is empty
     */
    @Override
    public List<OSAliasDto> getAllOSAlias() {
        List<OSAliasDto> result = new ArrayList<>();
        List<OSAlias> allOSAlias = osAliasDao.getAllOSAlias();

        if ((allOSAlias != null) && (!allOSAlias.isEmpty())) {
            allOSAlias.forEach(
                    a -> result.add(new OSAliasDto(a))
            );
        }

        return result;
    }

    /**
     * Deletes an alias by ID from the DB
     *
     * @param id the ID of the entity of OS alias that has to be deleted
     * @throws IllegalArgumentException thrown when ID is NULL value
     * @throws NoSuchElementException   thrown when the OS alias can not be found in DB with the given ID
     */
    @Override
    public void deleteOSAlias(Long id, String userName) throws IllegalArgumentException, NoSuchElementException {
        //check constraints
        if (id == null) {
            LOG.log(Level.WARNING, "OS alias ID was NULL, can not be deleted!");

            throw new IllegalArgumentException("ID can not be NULL when trying to delete OS alias");
        }

        LOG.log(Level.FINE, format("Trying to delete OS alias by ID [{%d}] by %s", id, userName));
        //search in DB with given ID
        OSAlias osAlias = osAliasDao.searchById(id);

        if (osAlias == null) {
            LOG.log(Level.WARNING, format("OS Alias ID [%d] not found in DB to delete by user: %s", id, userName));

            throw new NoSuchElementException("ID can not be found in DB when trying to delete OS Alias");
        }

        //delete OS alias
        osAliasDao.deleteAlias(osAlias);
        LOG.log(Level.FINE, format("Deleted OS alias by ID [{%d}] by %s", id, userName));
    }

    /**
     * Creates an E3-EEE OS alias pair in DB for the given user
     *
     * @param e3OSAlias  the E3 OS alias name
     * @param eeeOSAlias the EEE OS alias name
     * @param userName   the user that created would create the OS alias pair
     * @throws IllegalArgumentException  throws when eee OS alias, or e3 OS alias is empty
     * @throws KeyAlreadyExistsException throws when the OS aliases already exist in DB so it violates constraints
     */
    @Override
    public void createAlias(String e3OSAlias, String eeeOSAlias, String userName) throws IllegalArgumentException, KeyAlreadyExistsException {
        //check constraints
        if ((StringUtils.isEmpty(eeeOSAlias)) || (StringUtils.isEmpty(e3OSAlias))) {

            LOG.log(Level.WARNING, format("Can not create OS alias when EEE name [%s], or E3 name [%s] is empty, user: %s",
                    eeeOSAlias, e3OSAlias, userName));

            throw new IllegalArgumentException("E3 OS alias or EEE OS alias was empty when trying to create OS alias");
        }

        LOG.log(Level.FINE, "Trying to create OS alias by user: ".concat(userName));

        OSAlias alias;
        //create OS alias
        try {
            alias = osAliasDao.createAlias(e3OSAlias, eeeOSAlias);
        } catch (ConstraintViolationException e) {

            LOG.log(Level.WARNING, "OS alias can't be created because it already exits");
            throw new KeyAlreadyExistsException("OS alias can't be created because it already exits with the given parameters");
        }

        LOG.log(Level.FINE, format("OS alias created with ID [%d],  E3 alias [%s], EEE alias [%s] by user: %s",
                alias.getId(), e3OSAlias, eeeOSAlias, userName));
    }

    /**
     * Updates an OS alias pair in DB by a given user
     *
     * @param aliasDto the alias DTO that contains the old ID, and new values
     * @param userName the user that want to update the OS alias pair
     * @throws IllegalArgumentException  throws when the ID, or new alias (String) values are empty
     * @throws NoSuchElementException    throws when the ID given in DTO can not be found in DB
     * @throws KeyAlreadyExistsException throws when the OS aliases already exist in DB so it violates constraints
     */
    @Override
    public void updateAlias(OSAliasDto aliasDto, String userName) throws IllegalArgumentException, NoSuchElementException, KeyAlreadyExistsException {
        //check constraints
        if (aliasDto == null) {
            LOG.log(Level.WARNING, format("OS alias updating is not possible when DTO is NULL value, user: %s", userName));

            throw new IllegalArgumentException("OS alias DTO was NULL valued when trying to update OS alias");
        }

        if ((StringUtils.isEmpty(aliasDto.getE3OsName())) || (StringUtils.isEmpty(aliasDto.getEeeOsName()))) {
            LOG.log(Level.WARNING, format("Can not update OS alias when EEE name [%s], or E3 name [%s] is empty, user: %s",
                    aliasDto.getEeeOsName(), aliasDto.getE3OsName(), userName));

            throw new IllegalArgumentException("E3 OS alias or EEE OS alias was empty when trying to update OS alias");
        }

        LOG.log(Level.FINE, format("Trying to update OS alias in DB with ID [%d] by user: ",
                aliasDto.getId(), userName));

        //search for OS alias in DB
        OSAlias osAlias = osAliasDao.searchById(aliasDto.getId());

        if (osAlias == null) {
            LOG.log(Level.WARNING, format("OS alias not found in DB to update its values, ID [%d], user: %s",
                    aliasDto.getId(), userName));

            throw new NoSuchElementException("ID can not be found in DB when trying to update OS Alias");
        }

        //update OS alias
        try {
            osAliasDao.updateAlias(aliasDto);
        } catch (ConstraintViolationException e) {

            LOG.log(Level.WARNING, "OS alias can't be updated to new value because it already exits in DB");
            throw new KeyAlreadyExistsException("OS alias can't be updated to new values because it already exits with the given parameters");
        }

        LOG.log(Level.FINE, format("OS alias update finished in DB with ID [%d] by user: ",
                aliasDto.getId(), userName));
    }
}
