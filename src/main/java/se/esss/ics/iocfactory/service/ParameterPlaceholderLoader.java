/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;
import se.esss.ics.iocfactory.model.ConsistencyMsgLevel;
import se.esss.ics.iocfactory.model.ParameterPlaceholder;
import se.esss.ics.iocfactory.model.ParameterType;
import se.esss.ics.iocfactory.model.PlaceholderTrait;
import static se.esss.ics.iocfactory.model.PlaceholderTrait.*;
import se.esss.ics.iocfactory.model.Repository;
import se.esss.ics.iocfactory.model.RepositoryMessage;

/**
 * This class loads st.cmd template for a given module from the repository
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 *
 */
public class ParameterPlaceholderLoader {
    private static final Pattern ST_CMD_LINE_PATTERN = Pattern.compile("#\\-?\\s*@(\\S+)(.*)");

    private static final Logger LOGGER = Logger.getLogger(ParameterPlaceholderLoader.class.getCanonicalName());

    private final Repository repo;
    private final File scannedFile;

    public ParameterPlaceholderLoader(Repository repo, File scannedFile) {
        this.repo = repo;
        this.scannedFile = scannedFile;
    }

    private enum Annotation { NONE, GLOBAL, FIELD, TYPE, RESTRICTIONS, RUNTIME, UNIQUENESS }

    private static class StCmdFileLine {
        private final Annotation annotation;
        private final String text;
        private final int lineNum;

        private StCmdFileLine(Annotation annotation, String text, int lineNum) {
            super();
            this.annotation = annotation;
            this.text = text;
            this.lineNum = lineNum;
        }
    }

    /**
     * Given a {@link File} in st.cmd.template format, loads a list of {@link ParameterPlaceholder} from it
     *
     * @return pair of empty lists if the file does not exist, or a pair of placeholder list  and globals list of
     * {@link ParameterPlaceholder}
     */
    public List<ParameterPlaceholder> loadParameterPlaceholders() {
        if (!scannedFile.exists()) {
            return Collections.emptyList();
        }

        try (final LineNumberReader reader = new LineNumberReader(new InputStreamReader(
                new FileInputStream(scannedFile), StandardCharsets.US_ASCII))) {
            String line;
            final ParserStateMachine stateMachine = new ParserStateMachine(reader);

            while ((line = reader.readLine()) != null) {
                // null means a non-comment line or an error
                final StCmdFileLine stCmdFileLine = parseLine(line, reader.getLineNumber());

                stateMachine.handleLine(stCmdFileLine);
            }

            stateMachine.handleEpilog();

            return stateMachine.getPlaceholderList();
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);

            repo.getMessages().add(new RepositoryMessage(ConsistencyMsgLevel.ERROR,
                    "Fatal: IO Error while reading the file.", scannedFile));

            return Collections.emptyList();
        }
    }

    private static String buildAndTrimLastNewline(StringBuilder comment) {
        if (comment.length()>=1) {
            comment.delete(comment.length()-1, comment.length());
        }
        return comment.toString();
    }

    private void processParsedData(Builder<ParameterPlaceholder> listBuilder, String parameterName,
            String parameterType, String comment, int lineNum, Set<PlaceholderTrait> traits) {

        final ParameterPlaceholder parameterPlaceholder =
                createParameterPlaceholder(parameterName, parameterType, comment, lineNum, traits);

        if (parameterPlaceholder != null) {
            listBuilder.add(parameterPlaceholder);
        }
    }

     private StCmdFileLine parseLine(final String line, int lineNum) {
        // Check if comment line
        final String trimmedLine = line.trim();
        // skip empty lines and non-comments
        if (trimmedLine.length()==0 || !trimmedLine.startsWith("#")) {
            return null;
        }


        final Matcher matcher = ST_CMD_LINE_PATTERN.matcher(trimmedLine);
        if (!matcher.matches()) {
            // it is a comment without annotation
            final String trimmedComment =  trimmedLine.substring(trimmedLine.startsWith("#-") ? 2 : 1).trim();
            return new StCmdFileLine(Annotation.NONE, trimmedComment, lineNum);
        }

        final String annotationString = matcher.group(1);
        final String text = matcher.group(2);

        if (text == null || text.trim().isEmpty()) {
            logWarnMessage("Annotation without value", lineNum);
            return null;
        }

        try {
            Annotation annotation = Annotation.valueOf(annotationString.toUpperCase());
            return new StCmdFileLine(annotation, text.trim(), lineNum);
        } catch (IllegalArgumentException e) {
            LOGGER.log(Level.WARNING, e.getMessage());
            logWarnMessage(String.format("Unsupported annotation '%s'", annotationString), lineNum);
            return null;
        }
    }

    private ParameterPlaceholder createParameterPlaceholder(
            String name, String typeString, String comment, int lineNum, Set<PlaceholderTrait> traits) {
        if (name == null)
        {
            return null;
        } else {
            if (typeString == null) {
                logWarnMessage(String.format("Parameter type not specified for parameter '%s', defaulting to STRING",
                        name), lineNum);
                typeString = ParameterType.STRING.toString();
            }
            try {
                final ParameterType type = ParameterType.valueOf(typeString.toUpperCase());
                return new ParameterPlaceholder(name, type, comment, traits);
            } catch (IllegalArgumentException e) {
                LOGGER.log(Level.WARNING, e.getMessage());
                logWarnMessage(String.format("Unsupported parameter type '%s'", typeString), lineNum);
                return null;
            }
        }
    }

    private void logWarnMessage(String message, int lineNum) {
        repo.getMessages().add(new RepositoryMessage(ConsistencyMsgLevel.WARN,
            String.format("%s, line %d", message, lineNum), scannedFile));
    }

    /**
     * Implements a line-dependent state machine for parsing the placeholder metadata
     */
    private class ParserStateMachine {
        private final Builder<ParameterPlaceholder> placeholderList = ImmutableList.builder();

        private boolean insideParameterDefinition = false;

        private String parameterName = null;
        private String parameterType = null;

        private Set<PlaceholderTrait> traits =
                EnumSet.noneOf(PlaceholderTrait.class);

        private StringBuilder comment = new StringBuilder();
        private StringBuilder emptyComment = new StringBuilder();

        private final LineNumberReader reader;

        public ParserStateMachine(LineNumberReader reader) {
            this.reader = reader;
        }

        public List<ParameterPlaceholder> getPlaceholderList() { return placeholderList.build(); }

        public void handleLine(final StCmdFileLine stCmdFileLine) {
            if (stCmdFileLine == null) {
                handleNonCommentLine();
            } else {
                switch (stCmdFileLine.annotation) {
                    case NONE:
                        handleNone(stCmdFileLine);
                        break;
                    case GLOBAL:
                        handleField(stCmdFileLine, true);
                        break;
                    case FIELD:
                        handleField(stCmdFileLine, false);
                        break;
                    case TYPE:
                        handleType(stCmdFileLine);
                        break;
                    case RESTRICTIONS:
                        // placeholder to handle parameter restrictions in the future
                        break;
                    case RUNTIME:
                        handleRuntime(stCmdFileLine);
                        break;
                    case UNIQUENESS:
                        handleUniqueness(stCmdFileLine);
                        break;
                    default:
                        break;
                }
            }
        }

        public void handleEpilog() {
            if (insideParameterDefinition) {
                processParsedData(placeholderList, parameterName, parameterType,
                        buildAndTrimLastNewline(comment), reader.getLineNumber(), traits);
            }
        }

        private void handleNone(final StCmdFileLine stCmdFileLine) {
            if ("".equals(stCmdFileLine.text)) {
                emptyComment.append("\n");
            } else {
                comment.append(emptyComment);
                emptyComment = new StringBuilder();

                comment.append(stCmdFileLine.text);
                comment.append("\n");
            }
        }

        private void handleField(final StCmdFileLine stCmdFileLine, boolean scannedFieldGlobal) {
            if (insideParameterDefinition) {
                processParsedData(placeholderList, parameterName, parameterType,
                        buildAndTrimLastNewline(comment), reader.getLineNumber(), traits);
                resetState();
            }

            insideParameterDefinition = true;
            parameterName = stCmdFileLine.text;
            traits.add(scannedFieldGlobal ? GLOBAL_HINT : PLAIN);
        }

       private void handleNonCommentLine() {
            if (insideParameterDefinition) {
                processParsedData(placeholderList, parameterName, parameterType,
                        buildAndTrimLastNewline(comment), reader.getLineNumber(), traits);
                resetState();
                insideParameterDefinition = false;
            }
        }

         private void resetState() {
            parameterName = null;
            parameterType = null;
            comment = new StringBuilder();
            emptyComment = new StringBuilder();
            traits = EnumSet.noneOf(PlaceholderTrait.class);
        }


        private void handleType(final StCmdFileLine stCmdFileLine) {
            if (parameterType != null) {
                logWarnMessage("Duplicate parameter, ignoring subsequent definition",
                        stCmdFileLine.lineNum);
            } else {
                parameterType = stCmdFileLine.text;
            }
        }

        private void handleRuntime(final StCmdFileLine stCmdFileLine) {
            final String txt = StringUtils.lowerCase(StringUtils.trim(stCmdFileLine.text));
            if ("yes".equals(txt)) {
                traits.add(RUNTIME);
                traits.remove(NON_RUNTIME);
            } else if ("no".equals(txt)) {
                traits.remove(RUNTIME);
                traits.add(NON_RUNTIME);
            } else {
                logWarnMessage("Unknown runtime directive value " + StringUtils.trim(stCmdFileLine.text),
                        stCmdFileLine.lineNum);
            }
        }

        private void handleUniqueness(StCmdFileLine stCmdFileLine) {
            final String txt = StringUtils.lowerCase(StringUtils.trim(stCmdFileLine.text));
            if ("none".equals(txt)) {
                traits.remove(UNIQUE_IOC);
                traits.remove(UNIQUE_MACHINE);
            } else if ("ioc".equals(txt)) {
                traits.remove(UNIQUE_MACHINE);
                traits.add(UNIQUE_IOC);
            } else if ("machine".equals(txt)) {
                traits.remove(UNIQUE_IOC);
                traits.add(UNIQUE_MACHINE);
            } else {
                logWarnMessage("Unknown uniqueness directive value " + StringUtils.trim(stCmdFileLine.text),
                        stCmdFileLine.lineNum);
            }
        }
    }
}

