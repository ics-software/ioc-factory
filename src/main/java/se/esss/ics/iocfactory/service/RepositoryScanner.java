package se.esss.ics.iocfactory.service;

import se.esss.ics.iocfactory.model.Repository;

import java.io.File;

/**
 * Generic interface for repository scanner classes.
 *
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public interface RepositoryScanner <R extends Repository> {
    void setRepositoryBaseDir(File baseDir);

    R scanRepository();
}
