package se.esss.ics.iocfactory.service.mail;

import com.google.common.base.Joiner;
import se.esss.ics.iocfactory.service.rbac.UserDirectoryService;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This is a service to send mails through configuration specified in JBOSS.
 *
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
@Singleton
@Startup
public class MailService {
    private static final Logger LOGGER = Logger.getLogger(MailService.class.getName());

    @Inject
    private UserDirectoryService userDirectoryService;

    @Resource(name = "java:jboss/mail/Default")
    private Session mailSession;

    /**
     * Sends email with HTML content to users with given parameters.
     * The mail properties are stored in standalone.xml, and the proper JNDI name has to be set for mailSession
     * variable.
     *
     * @param emailAddresses list/set of recipient mail addresses
     * @param loggedInName the name of the user invoking the notification
     * @param subject: the subject of the mail address
     * @param content: the email content
     * @param attachments: list of email-attachments (streams)
     * @param filenames: attachment file-names
     * @param withAttachment: should the email contain the attached files, or skip them?
     */
    public void sendMail(Iterable<String> emailAddresses, String loggedInName,
                         String subject, String content,
                         List<InputStream> attachments, List<String> filenames, boolean withAttachment) {
        if (loggedInName != null) {
            String replyToEmail = userDirectoryService.getEmail(loggedInName);
            if (replyToEmail != null && replyToEmail.trim().length() > 0) {
                sendMail(emailAddresses, Collections.singletonList(replyToEmail), subject, content, attachments,
                        filenames, withAttachment);
            }
        }
    }

    /***
     * Sends email with HTML content to users with given parameters.
     * The mail properties are stored in standalone.xml, and the proper JNDI name has to be set for mailSession
     * variable.
     *
     * @param emailAddresses: list/set of recipient mail addresses
     * @param replyToEmail: email addresses where others can reply to (contains operator and the requester)
     * @param subject: the subject of the mail address
     * @param content: the email content
     * @param attachments: list of email-attachments (streams)
     * @param filenames: attachment file-names
     * @param withAttachment: should the email contain the attached files, or skip them?
     */
    public void sendMail(Iterable<String> emailAddresses, List<String> replyToEmail,
                         String subject, String content,
                         List<InputStream> attachments, List<String> filenames, boolean withAttachment) {
        try {
            final Message message = new MimeMessage(mailSession);

            final List<Address> toAddresses = new ArrayList<>();
            final List<Address> ccAddresses = new ArrayList<>();
            for (final String recipientAddress : emailAddresses) {

                if (recipientAddress == null || recipientAddress.trim().length() == 0) {
                    LOGGER.warning(
                            "Admin email address is empty, skipping adding it to recipient list: " + recipientAddress);
                    continue;
                }

                toAddresses.add(new InternetAddress(recipientAddress));
            }
            message.setRecipients(Message.RecipientType.TO, toAddresses.toArray(new Address[0]));

            if (toAddresses.isEmpty()) {
                LOGGER.log(Level.FINE, "Naming email TO list is empty, can not send email!");
                return;
            }

            message.setSubject("[IOCFactory] " + subject);
            message.setFrom(new InternetAddress(mailSession.getProperty("mail.from"), "IOCFactory"));

            if ((replyToEmail != null) && !(replyToEmail.isEmpty())) {
                for(String addr : replyToEmail) {
                    InternetAddress userAddress = new InternetAddress(addr);
                    ccAddresses.add(userAddress);
                    message.setReplyTo(new InternetAddress[]{userAddress});
                    message.setRecipients(Message.RecipientType.CC, ccAddresses.toArray(new Address[0]));
                }
            }

            message.setSentDate(new Date());

            if (withAttachment && attachments != null && !attachments.isEmpty() && filenames != null
                    && !filenames.isEmpty() && attachments.size() == filenames.size()) {
                MimeMultipart multipart = new MimeMultipart();
                MimeBodyPart messagePart = new MimeBodyPart();
                messagePart.setContent(content, "text/html; charset=utf-8");
                multipart.addBodyPart(messagePart);
                for (int i = 0; i < attachments.size(); i++) {
                    MimeBodyPart attachmentPart = new MimeBodyPart();
                    DataSource dataSource = new ByteArrayDataSource(attachments.get(i), "application/octet-stream");
                    attachmentPart.setDataHandler(new DataHandler(dataSource));
                    attachmentPart.setFileName(filenames.get(i));
                    multipart.addBodyPart(attachmentPart);
                }
                message.setContent(multipart);
            } else {
                message.setContent(content, "text/html; charset=utf-8");
            }

            LOGGER.fine("Assembled a message with subject '" + subject + "' to:" + Joiner.on(", ").join(toAddresses)
                    + " cc:" + Joiner.on(", ").join(ccAddresses));
            LOGGER.finest("Message text:\n" +content);


            LOGGER.log(Level.FINE, "Trying to send email to recipients");
            Transport.send(message);

        } catch (MessagingException | IOException e) {
            LOGGER.log(Level.WARNING, "Error while trying to sending email: " + e);
            throw new RuntimeException(e);
        }
    }
}
