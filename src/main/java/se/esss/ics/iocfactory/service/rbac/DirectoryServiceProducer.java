package se.esss.ics.iocfactory.service.rbac;

import se.esss.ics.rbac.dsaccess.DirectoryServiceAccess;
import se.esss.ics.rbac.dsaccess.DirectoryServiceAccessException;
import se.esss.ics.rbac.dsaccess.ldap.LDAPDirectoryServiceAccess;

import javax.enterprise.inject.Produces;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public class DirectoryServiceProducer {
    @Produces
    public DirectoryServiceAccess ldapDirectoryServiceProducer() throws DirectoryServiceAccessException {
        return new LDAPDirectoryServiceAccess();
    }
}
