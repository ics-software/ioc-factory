/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service.rbac;

/**
 * This contains RBAC resource and permission definitions.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public interface IOCFactoryRBACDefinitions {

    /** The RBAC resource corresponding to ioc factory. */
    String IOC_FACTORY_RESOURCE = "IOCFactory";

    /** The RBAC permission for administer the ioc factory. */
    String ADMINISTER_IOC_FACTORY_PERMISSION = "AdministerIOCFactory";

    /** The RBAC permission for configuring iocs. */
    String CONFIGURE_IOC_PERMISSION = "ConfigureIOCs";

    /** The RBAC permission for deploying non production IOCs */
    String DEPLOY_NON_PRODUCTION = "DeployNonProduction";

    /** The RBAC permission for deploying non production IOCs */
    String DEPLOY_PRODUCTION = "DeployProduction";
}
