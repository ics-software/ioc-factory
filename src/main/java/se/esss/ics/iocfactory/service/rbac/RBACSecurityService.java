/*
 * Copyright (c) 2015 European Spallation Source
 * Copyright (c) 2015 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service.rbac;

import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Alternative;

import se.esss.ics.iocfactory.service.SecurityService;
import se.esss.ics.rbac.loginmodules.service.Message;
import se.esss.ics.rbac.loginmodules.service.RBACSSOSessionService;

/**
 * An implementation of {@link SecurityService} using {@link RBACSSOSessionService}.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Alternative
@RequestScoped
public class RBACSecurityService extends RBACSSOSessionService implements SecurityService, Serializable {

    @Override
    public boolean doLogin(String username, String password) {
        final Message loginMessage = login(username, password);
        return loginMessage.isSuccessful();
    }

    @Override
    public String getUsername() {
        return getLoggedInName();
    }

    @Override
    public void doLogout() {
        logout();
    }

    @Override
    public boolean canAdminister() {
        return hasPermission(IOCFactoryRBACDefinitions.ADMINISTER_IOC_FACTORY_PERMISSION);
    }

    @Override
    public boolean canConfigureIOCs() {
        return hasPermission(IOCFactoryRBACDefinitions.CONFIGURE_IOC_PERMISSION);
    }

    @Override
    public boolean canDeployNonProduction() {
        return hasPermission(IOCFactoryRBACDefinitions.DEPLOY_NON_PRODUCTION);
    }

    @Override
    public boolean canDeployProduction() {
        return hasPermission(IOCFactoryRBACDefinitions.DEPLOY_PRODUCTION);
    }   
}
