package se.esss.ics.iocfactory.service.rbac;

import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import java.util.logging.Level;
import java.util.logging.Logger;
import se.esss.ics.rbac.dsaccess.DirectoryServiceAccess;
import se.esss.ics.rbac.dsaccess.DirectoryServiceAccessException;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
@Singleton
@Startup
public class RBACUserDirectoryService implements UserDirectoryService {
    private static final Logger LOGGER = Logger.getLogger(RBACUserDirectoryService.class.getName());

    private static final String TEST_USERNAME = "nonexisting_user";
    private static boolean validated;

    @Inject
    private DirectoryServiceAccess directoryServiceAccess;

    @Override
    public void validate() {
        if (validated) {
            return;
        }

        LOGGER.log(Level.INFO, "Checking for presence of user directory.");

        try {
            // test service by trying to retrieve some data
            directoryServiceAccess.getUserInfo(TEST_USERNAME.toCharArray());
        } catch (DirectoryServiceAccessException e) {
            throw new RuntimeException(e);
        }
        validated = true;
    }

    @Override
    public String getEmail(String username) {
        try {
            return directoryServiceAccess.getUserInfo(username.toCharArray()).getEMail();
        } catch (DirectoryServiceAccessException e) {
            throw new RuntimeException("There was an error retrieving a user email.", e);
        }
    }
}
