package se.esss.ics.iocfactory.service.rbac;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public interface UserDirectoryService {
    /** Tests that the directory service is reachable, and throws an exception if it isn't. */
    void validate();

    /**
     * Retrieves the email address for the given user.
     *
     * @param username
     *            the user name
     * @return the user email
     */
    String getEmail(String username);
}
