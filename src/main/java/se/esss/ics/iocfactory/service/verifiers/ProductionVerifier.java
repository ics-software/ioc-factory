/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service.verifiers;

import se.esss.ics.iocfactory.configuration.IOCEnvironment;
import se.esss.ics.iocfactory.model.Configuration;
import se.esss.ics.iocfactory.model.ConsistencyMsgLevel;
import se.esss.ics.iocfactory.model.Repository;
import se.esss.ics.iocfactory.model.VersionConverters;
import se.esss.ics.iocfactory.service.verifiers.dependency.ConfigDependencies;
import se.esss.ics.iocfactory.service.verifiers.dependency.DependencyUtils;

/**
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
public class ProductionVerifier extends ConfigVerifierBase implements ConfigVerifier {
    private static final String VERIFIER_CATEGORY = "Dependencies";

    @Override
    public int getPriority() { return 500; }
    
    @Override
    public String getCategory() { return VERIFIER_CATEGORY; }     

    @Override
    public void verifyImpl(final Configuration config, final Repository repo,  final IOCEnvironment env) {
        
        if (env.isProduction()) {
            final ConfigDependencies deps = DependencyUtils.flatConfigDependencies(config, repo);
            
            deps.getAllDeps().forEach((dep, moduleSatisfiedPair) -> {
                if (VersionConverters.isModuleVersionNamed(dep.getVersion())) {
                    addVerifyMessage(ConsistencyMsgLevel.ERROR, "Named dependency %s for module %s used in "
                            + " a production environment.", dep.getVersion(),
                            DependencyUtils.formatModuleString(moduleSatisfiedPair.getModule()));
                }
            });
        }
    }
}
