/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service.verifiers.dependency;

import java.util.SortedMap;
import java.util.TreeMap;
import se.esss.ics.iocfactory.model.Module;
import se.esss.ics.iocfactory.model.ModuleDependency;

/**
 *
 * @author mpavleski
 */
public class ConfigDependencies {

    /**
     * Flattened dependencies tree for a configuration
     *
     * Map contains dependency - and module declaring it pair. Using TreeMap to get sorted walk-trough
     */
    private final SortedMap<ModuleDependency, ModuleSatisfiedPair> allDeps =
            new TreeMap<>(DependencyUtils::compareDependency);

    public void addSatisfiedDep(final ModuleDependency dep, final Module declaringModule) {
        allDeps.put(dep, new ModuleSatisfiedPair(declaringModule, true));
    }

    public void addUnsatisfiedDep(final ModuleDependency dep, final Module declaringModule) {
        allDeps.put(dep, new ModuleSatisfiedPair(declaringModule, false));
    }

    public SortedMap<ModuleDependency, ModuleSatisfiedPair> getAllDeps() { return allDeps; }

}
