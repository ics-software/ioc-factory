/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 * 
 * This file is part of IOC Factory.
 * 
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service.verifiers.dependency;

import se.esss.ics.iocfactory.model.Module;

/**
 *
 * @author mpavleski
 */
public class ModuleSatisfiedPair {
    private final Module module;
    private final boolean satisfied;

    public ModuleSatisfiedPair(Module module, boolean satisfied) {
        this.module = module;
        this.satisfied = satisfied;
    }

    public Module getModule() { return module; }
    public boolean isSatisfied() { return satisfied; }
}
