/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.ui;

import java.io.Serializable;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.lang3.tuple.Pair;
import se.esss.ics.iocfactory.configuration.IOCEnvironment;
import se.esss.ics.iocfactory.model.AuditEntry;
import se.esss.ics.iocfactory.model.Configuration;
import se.esss.ics.iocfactory.service.*;
import se.esss.ics.iocfactory.util.UiUtility;

/**
 * UI bean for the Audit screen
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
@Named
@ViewScoped
public class AuditManager implements Serializable {
    private static final Logger LOG = Logger.getLogger(AuditManager.class.getName());

    @Inject private transient AuditService auditService;
    @Inject private transient GenerateService generateService;
    @Inject private transient ConfigurationService configService;
    @Inject private transient SetupService setupService;

    @Inject private transient DualCompareConfigManager compareMgr;

    private List<AuditEntry> auditEntries;
    private AuditEntry selectedEntry;

    public List<AuditEntry> getAuditEntries() {
        if (auditEntries == null) {
            try {
                auditEntries = auditService.getAuditEntries();
            } catch (ConfigClientException ex) {
                LOG.log(Level.SEVERE, "Failed accessing CCDB while reading the audit entries", ex);
                UiUtility.showErrorMessage("Failed accessing CCDB while reading the audit entries", ex);
                auditEntries = Collections.emptyList();
            }
        }

        return auditEntries;
    }

    public AuditEntry getSelectedEntry() { return selectedEntry; }
    public void setSelectedEntry(AuditEntry selectedEntry) { this.selectedEntry = selectedEntry; }

    public void prepareDualCompareDialog() {
        if (selectedEntry == null) {
            UiUtility.showWarningMessage("No configuration selected");
            FacesContext.getCurrentInstance().validationFailed();
            return;
        } else if (!selectedEntry.hasGenerateEntry()) {
            UiUtility.showWarningMessage("A configuration was not generated for the selected IOC.");
            FacesContext.getCurrentInstance().validationFailed();
            return;
        } else if (!selectedEntry.isConfigDifferent()) {
            UiUtility.showWarningMessage("The configurations do not differ");
            FacesContext.getCurrentInstance().validationFailed();
            return;
        }

        final Pair<String, String> disk = auditService.getEnvAndStFromDisk(selectedEntry);
        final Pair<String, String> conf;

        try {
            conf = auditService.getEnvAndStFromConf(selectedEntry);
        } catch (ConfigClientException ex) {
            LOG.log(Level.SEVERE, "doCompare failed to access CCDB service", ex);
            UiUtility.showErrorMessage("Failed to access CCDB service", ex);
            FacesContext.getCurrentInstance().validationFailed();
            return;
        } catch (GenerateService.GenerationException ex) {
            LOG.log(Level.SEVERE,
                    String.format("doCompare generation of preview failed for configuration id %d",
                            selectedEntry.getGenerateEntry()), ex);
            UiUtility.showErrorMessage("Internal error while generating preview from saved configuration", ex);
            FacesContext.getCurrentInstance().validationFailed();
            return;
        }

        final Date lastModified = selectedEntry.getDiskModificationTime();

        compareMgr.getTitles().set(0, String.format("%s, Revision %d", selectedEntry.getIocName(),
                selectedEntry.getGenerateEntry().getConfigRevision()));
        compareMgr.getTitles().set(1, String.format("%s on %s %s",
                selectedEntry.getIocName(), selectedEntry.getEnvironment(),
                lastModified != null ? "modified " + TimestampFormatter.format(lastModified) : ""));

        compareMgr.getEnvs().set(0, conf.getLeft());
        compareMgr.getEnvs().set(1, disk.getLeft());

        compareMgr.getSts().set(0, conf.getRight());
        compareMgr.getSts().set(1, disk.getRight());
    }

    public void restoreConfiguration() {
        if (selectedEntry == null) {
            return;
        }

        final String targetEnvName = selectedEntry.getEnvironment();
        final List<IOCEnvironment> loadedEnvs = setupService.loadEnvs();

        final Optional<IOCEnvironment> targetEnv = loadedEnvs.stream().
                filter(env -> Objects.equals(env.getName(), targetEnvName)).findFirst();

        final Configuration loadedConfig;
        try {
            loadedConfig = configService.loadConfiguration(selectedEntry.getGenerateEntry().getConfigId());
        } catch (ConfigClientException e) {
            LOG.log(Level.SEVERE, "Failed accessing CCDB while restoring config entry", e);
            UiUtility.showErrorMessage("Failed accessing CCDB while restoring config entry", e);
            return;
        }

        if (targetEnv.isPresent()) {
            try {
                generateService.generateConfiguration(loadedConfig, targetEnv.get(),
                        "SYSTEM: Restored in Audit view.");
            } catch (GenerateService.GenerationException e) {
                final String msg = String.format("Restoring the configuration failed: %s", e.getMessage());
                UiUtility.showErrorMessage(msg, e);
                LOG.log(Level.SEVERE, msg, e);
            }

            UiUtility.showInfoMessage("The configuration was successfully restored.");
            // Clear audit entries list, will cause to reload in the getter
            auditEntries = null;
            selectedEntry = null;
        } else {
            UiUtility.showWarningMessage(String.format("The %s does not exist in IOC factory any more", targetEnvName));
        }
    }
}
