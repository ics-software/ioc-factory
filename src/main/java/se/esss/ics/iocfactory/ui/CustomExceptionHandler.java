/*
 * Copyright (c) 2018 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package se.esss.ics.iocfactory.ui;

import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJBException;
import javax.el.ELException;
import javax.faces.FacesException;
import javax.faces.context.ExceptionHandler;
import javax.faces.event.ExceptionQueuedEvent;

import org.apache.commons.lang3.StringUtils;
import org.omnifaces.exceptionhandler.FullAjaxExceptionHandler;
import se.esss.ics.iocfactory.util.UiUtility;

/**
 * A global JSF exception handler that displays caught exceptions in the UI as
 * popup messages and logs them.
 */
public class CustomExceptionHandler extends FullAjaxExceptionHandler {

    private static final Logger LOGGER = Logger
            .getLogger(CustomExceptionHandler.class.getCanonicalName());
    private static final String UNEXPECTED_ERROR = "Unexpected error";
    private static final String SESSION_ERROR = "Session error";

    /**
     * A new JSF exception handler
     *
     * @param wrapped
     *            the original JSF exception handler
     */
    public CustomExceptionHandler(ExceptionHandler wrapped) {
        super(wrapped);
    }

    @Override
    public void handle() throws FacesException {
        final Iterator<ExceptionQueuedEvent> unhandledExceptionQueuedEvents = getUnhandledExceptionQueuedEvents()
                .iterator();

        if (!unhandledExceptionQueuedEvents.hasNext()) {
            return;
            // there are no unhandled exceptions
        }

        final Throwable unwrappedException = unhandledExceptionQueuedEvents
                .next().getContext().getException();

        final Throwable throwable = getExceptionNonFrameworkCause(
                unwrappedException);

        if (!(throwable instanceof javax.faces.application.ViewExpiredException)) {
            UiUtility.showUnexpectedErrorMessage(getDetails(throwable), throwable);
            LOGGER.log(Level.SEVERE, UNEXPECTED_ERROR, throwable);
        } else {
            super.handle();
        }
    }

    private String getDetails(final Throwable throwable) {
        return throwable.getClass().getSimpleName() +
                (StringUtils.isNotEmpty(throwable.getMessage()) ? ": " + throwable.getMessage() : "");
    }

    /*
     * Returns the nested exception cause that is not Faces or EJB exception, if
     * it exists.
     */
    private Throwable getExceptionNonFrameworkCause(Throwable exception) {
        return (exception instanceof FacesException
                || exception instanceof EJBException
                || exception instanceof ELException)
                && (exception.getCause() != null)
                        ? getExceptionNonFrameworkCause(exception.getCause())
                        : exception;
    }
}
