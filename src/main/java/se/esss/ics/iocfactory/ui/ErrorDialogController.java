package se.esss.ics.iocfactory.ui;

import com.google.common.collect.ImmutableMap;
import com.google.common.net.HttpHeaders;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import se.esss.ics.iocfactory.service.mail.MailService;
import se.esss.ics.iocfactory.service.rbac.RBACSecurityService;
import se.esss.ics.iocfactory.util.UiUtility;

import javax.faces.application.Application;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * Controller bean class for error dialogs.
 *
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
@Named
@ViewScoped
public class ErrorDialogController implements Serializable {

    private static final String SUBJECT = "Bug report";

    private List<String> details;
    private String stackTrace;
    private String comment;
    private boolean unexpectedError;
    @Inject
    private UIPropertiesManager properties;

    @Inject
    private MailService mailService;
    @Inject
    private RBACSecurityService sessionService;

    public List<String> getDetails() {
        return details;
    }

    public void setDetails(List<String> details) {
        this.details = details;
    }

    public void setStackTrace(String stackTrace) {
        this.stackTrace = stackTrace;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isUnexpectedError() {
        return unexpectedError;
    }

    public boolean showFeedbackUI() {
        return StringUtils.isNotEmpty(getSupportEmail()) && unexpectedError;
    }

    public void setUnexpectedError(boolean unexpectedError) {
        this.unexpectedError = unexpectedError;
    }

    public void reportBehavior() {
        String supportEmail = getSupportEmail();
        String attachment = stackTrace;
        Map<String, String> requestHeaders = getRequestHeaders();
        String header = "Application: " + getMessagesProperty("title") + "<br/>" +
                "Version: " + getMessagesProperty("titleVersion") + "<br/>" +
                "System: " + requestHeaders.get(HttpHeaders.HOST) + "<br/>" +
                "User agent: " + requestHeaders.get(HttpHeaders.USER_AGENT) + "<br/><br/>";
        String body = header + "User comment: " + (StringUtils.isNotEmpty(comment) ?
                comment.replaceAll("\\r\\n|\\r|\\n", "<br/>") : "-");
        InputStream attachmentStream = IOUtils.toInputStream(attachment);

        try {
            mailService.sendMail(Collections.singletonList(supportEmail), sessionService.getUsername(),
                    SUBJECT, body, Collections.singletonList(attachmentStream),
                    Collections.singletonList("stack_trace.txt"), true);
            UiUtility.showInfoMessage("Email sent to support team.");
            dismiss();
        } catch (Exception e) {
            UiUtility.showErrorFacesMessage("Failed to send email to support team! Please try again!");
        }
    }

    public String getSupportEmail() {
        return properties.getSupportEmail();
    }

    public void dismiss() {
        UiUtility.dismissErrorMessage();
        details = null;
        comment = null;
    }

    private String getMessagesProperty(String key) {
        FacesContext context = FacesContext.getCurrentInstance();
        Application app = context.getApplication();
        ResourceBundle messages = app.getResourceBundle(context, "msgs");
        return messages.getString(key);
    }

    private Map<String, String> getRequestHeaders() {
        HttpServletRequest request =
                (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        return ImmutableMap.of(HttpHeaders.USER_AGENT, request.getHeader(HttpHeaders.USER_AGENT),
                HttpHeaders.HOST, request.getHeader(HttpHeaders.HOST));
    }
}
