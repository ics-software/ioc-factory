/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.ui;

import se.esss.ics.iocfactory.model.IOC;
import se.esss.ics.iocfactory.service.GenerateService;
import se.esss.ics.iocfactory.service.SecurityService;
import se.esss.ics.iocfactory.service.cdievents.IOCUpdated;

import javax.enterprise.event.Event;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;

public class GenerateDialogBase implements Serializable {
    /**
     * A builder for a message that is displayed in a dialog window when the genrate/undeploy operation is finished.
     */
    protected static class CompleteMessageBuilder {
        private StringBuilder builder;
        
        public CompleteMessageBuilder(String operationText) {
            builder = new StringBuilder();
            builder.append(String.format("<p>Following configurations were %s:</p>", operationText));
            builder.append("<ul>");
        }
        
        public void end() {
            builder.append("</ul>");
        }
        
        public void add(String iocName, Integer revisionNumber, String directoryPath, boolean isProductionEnv, 
                boolean isFrozen) {
            builder.append("<li>");
            
            builder.append(isProductionEnv ? "Production configuration " : " Configuration ");
            builder.append(iocName);
            builder.append(", rev: " + revisionNumber);
            builder.append("<br/>Directory: " + directoryPath + "</b>");
            builder.append("</li>");
            if (isFrozen) {
                builder.append(" Configuration committed (frozen).");
            }
        }
        
        public String toString() {
            return builder.toString();
        }
    }
    
    protected transient GenerateManager generateManager;

    @Inject
    protected transient GenerateService generateService;
    @Inject
    private transient SecurityService securityService;
    @Inject
    @IOCUpdated
    protected transient Event<IOC> iocUpdated;
    
    protected List<String> generateMessageList = Collections.emptyList();
    private boolean canDeployNonProduction;
    private boolean canDeployProduction;
    
    protected GenerateDialogBase() {}
    
    protected GenerateDialogBase(final GenerateManager generateManager) {
        this.generateManager = generateManager;
    }
    
    protected void init() {
        canDeployNonProduction = securityService.canDeployNonProduction();
        canDeployProduction = securityService.canDeployProduction();
    }
    
    protected boolean canDeployToAny() {
        return canDeployNonProduction || canDeployProduction;
    }
    
    protected boolean canDeploy(boolean isProductionEnv) {
        return (!isProductionEnv && canDeployNonProduction) || (isProductionEnv && canDeployProduction);
    }
}
