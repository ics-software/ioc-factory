/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.ui;

import com.google.common.collect.Maps;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.PrimeFaces;
import se.esss.ics.iocfactory.configuration.IOCEnvironment;
import se.esss.ics.iocfactory.model.Configuration;
import se.esss.ics.iocfactory.service.ConfigClientException;
import se.esss.ics.iocfactory.service.ConfigurationService;
import se.esss.ics.iocfactory.service.GenerateService;
import se.esss.ics.iocfactory.util.UiUtility;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * A JSF backing bean for handling the Generate dialog
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 *
 */
@Named
@ViewScoped
public class GenerateDialogManager extends GenerateDialogBase implements Serializable {
    private static final Logger LOG = Logger.getLogger(GenerateDialogManager.class.getCanonicalName());

    public static final String TITLE = "Generate";
    public static final String ERROR_MESSAGE = "Generate failed";

    private static final String CONFIG_LIST_TITLE = "Following configurations will be generated:";

    @Inject
    private transient ConfigurationService configService;

    private String desc;
    private Map<String, Configuration> lastConfigs;

    /**
     * The non-private, no-arg constructor is needed for the implementation to create a proxy of the managed bean:
     * https://stackoverflow.com/questions/46543657/cdi-object-not-proxyable-with-injected-constructor
     */
    protected GenerateDialogManager() {}

    @Inject
    public GenerateDialogManager(GenerateManager generateManager) {
        this.generateManager = generateManager;
        lastConfigs = Maps.newHashMap();
    }

    /**
     * Initializes the Generate Dialog backing bean.
     */
    @PostConstruct
    public void init() {
        super.init();
    }

    /**
     * @return title of displayed configuration list.
     */
    public String getConfigListTitle() {
        return CONFIG_LIST_TITLE;
    }

    /**
     * @return list of messages to display in configuration list.
     */
    public List<String> getGenerateMessageList() {
        return generateMessageList;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    /**
     * Check configuration and prepare a list of configuration with highest revision number
     */
    public void prepareAndShow() {
        prepareLastConfigs();

        generateMessageList = lastConfigs.entrySet().stream()
                     .map(entry -> String.format("%s, rev: %d", entry.getKey(), entry.getValue().getRevision()))
                     .collect(Collectors.toList());

        verifyForGenerate();

        PrimeFaces.current().executeScript("PF('confirmGenerateDlg').show()");
    }

    private void verifyForGenerate() {
        try {
            generateManager.verifyForGenerate(lastConfigs.values());
        } catch (ConfigClientException ex) {
            LOG.log(Level.SEVERE, "Failed to access the CCDB in verifyForGenerate");
        }
    }

    private void prepareLastConfigs() {
        lastConfigs.clear();

        // Go through selected configurations list
        for (Configuration conf : generateManager.getSelectedConfigs()) {

            // Make a map. Key should be the IOC name and value should be configuration with biggest revision number
            if (lastConfigs.get(conf.getIoc().getName()) == null) {
                lastConfigs.put(conf.getIoc().getName(), conf);

            } else {
                Configuration lastConf = lastConfigs.get(conf.getIoc().getName());

                if (conf.getRevision() > lastConf.getRevision()) {
                    lastConfigs.put(conf.getIoc().getName(), conf);
                }
            }
        }
    }

    /**
     * A callback that is invoked when the user selects or de-selects the environment row.
     */
    public void onEnvRowSelectionChange() {
        verifyForGenerate();
    }

    /**
     * A callback that is invoked when the user confirms the generate action.
     */
    public void onConfirm() {
        generate();
        generateManager.clearSelectedEnvs();
    }

    /**
     * Generate selected configurations
     */
    public void generate() {
        if (!canDeployToAny()) {
            UiUtility.showErrorMessage("You don't have permissions to generate IOCs.");
            return;
        }

        try {
            final CompleteMessageBuilder generatedMessage = new CompleteMessageBuilder("generated");
            // Generate configurations
            for (final IOCEnvironment selectedEnv : generateManager.getSelectedEnvs()) {
                for (Map.Entry<String, Configuration> entry : lastConfigs.entrySet()) {
                    Configuration config = entry.getValue();
                    final boolean isProductionEnv = selectedEnv.isProduction();
                    if (canDeploy(isProductionEnv)) {
                        final Configuration loadedConfig = configService.loadConfiguration(config.getId());
                        generateService.generateConfiguration(loadedConfig, selectedEnv, desc);

                        generatedMessage.add(config.getIoc().getName(), config.getRevision(),
                                GenerateService.getDestFolder(loadedConfig, selectedEnv.getDirectory()),
                                isProductionEnv, isProductionEnv);

                        if (isProductionEnv) {
                            iocUpdated.fire(config.getIoc());
                        }
                    }
                }
            }
            generatedMessage.end();

            // Clear table selection
            generateManager.clearSelectedIocs();
            generateManager.clearSelectedEnvs();

            UiUtility.showInfoMessageDialog(TITLE, generatedMessage.toString());
        } catch (GenerateService.GenerationException ge) {
            if (StringUtils.isNotEmpty(ge.getMessage()) && ge.getGenerationException() != null) {
                LOG.log(Level.SEVERE, ge.getMessage(), ge.getGenerationException());
                UiUtility.showErrorMessage(ERROR_MESSAGE, ge.getMessage(), ge);
            } else {
                LOG.log(Level.SEVERE, ge.getMessage(), ge);
                UiUtility.showErrorMessage(ERROR_MESSAGE, StringUtils.isNotEmpty(ge.getMessage())
                        ? "Error while generating IOC configuration: " + ge.getMessage()
                        : "Error while generating IOC configuration", ge);
            }
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Unknown exception during configuration generation.", e);
            UiUtility.showErrorMessage(ERROR_MESSAGE, "Unexpected error happened during IOC script generation, "
                    + "please notify administrator.\nThe exception has been logged.", e);
        }
    }
}
