/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.ui;

import org.apache.commons.lang3.tuple.Pair;
import org.primefaces.PrimeFaces;
import se.esss.ics.iocfactory.configuration.IOCEnvironment;
import se.esss.ics.iocfactory.model.ConfigVerifyStatus;
import se.esss.ics.iocfactory.model.ConfigVerifyUIStatus;
import se.esss.ics.iocfactory.model.Configuration;
import se.esss.ics.iocfactory.model.E3Repository;
import se.esss.ics.iocfactory.model.GenerateEntry;
import se.esss.ics.iocfactory.model.IOC;
import se.esss.ics.iocfactory.model.IOCConsistencyStatus;
import se.esss.ics.iocfactory.service.CachedSetupService;
import se.esss.ics.iocfactory.service.ConfigClientException;
import se.esss.ics.iocfactory.service.ConfigurationService;
import se.esss.ics.iocfactory.service.GenerateEntryService;
import se.esss.ics.iocfactory.service.GenerateService;
import se.esss.ics.iocfactory.service.IOCConfigsCache;
import se.esss.ics.iocfactory.service.SecurityService;
import se.esss.ics.iocfactory.service.VerifyService;
import se.esss.ics.iocfactory.service.cdievents.IOCUpdated;
import se.esss.ics.iocfactory.util.UiUtility;
import se.esss.ics.iocfactory.util.Util;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * A JSF backing bean for handling the Generate screen
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 *
 */
@Named
@ViewScoped
public class GenerateManager implements Serializable {
    private static final Logger LOG = Logger.getLogger(GenerateManager.class.getCanonicalName());

    @Inject
    private transient ConfigurationService configService;
    @Inject
    private transient IOCConfigsCache configCache;
    @Inject
    private transient CachedSetupService setupService;
    @Inject
    private transient GenerateService generateService;
    @Inject
    private transient RepositorySelector repoSelector;
    @Inject
    private transient VerifyService verifyService;
    @Inject
    private transient GenerateEntryService generateEntryService;

    @Inject
    @IOCUpdated
    private transient Event<IOC> iocUpdated;

    @Inject
    private transient SecurityService securityService;

    @Inject
    private transient GenerateDialogManager generateDialogManager;
    @Inject
    private transient UndeployDialogManager undeployDialogManager;

    private List<IOC> iocs = Collections.emptyList();
    private List<Configuration> configs = null;
    private List<GenerateEntry> lastGenerateEntries;

    private List<IOC> selectedIocs = Collections.emptyList();

    private List<Configuration> selectedConfigs = Collections.emptyList();

    private List<IOCEnvironment> envs = Collections.emptyList();
    private List<IOCEnvironment> selectedEnvs;

    private String previewEnv;
    private String previewSt;

    private boolean canDeployNonProduction;
    private boolean canDeployProduction;

    private List<ConfigVerifyUIStatus> verifyMessages = Collections.emptyList();

    /**
     * Initializes the Generate IOC backing bean.
     */
    @PostConstruct
    public void init() {
        canDeployNonProduction = securityService.canDeployNonProduction();
        canDeployProduction = securityService.canDeployProduction();

        loadIocsAndEnvs();
    }

    public void reload() {
        final List<IOC> prevSelectedIocs = selectedIocs;
        final List<Configuration> prevSelectedConfigs = selectedConfigs;

        loadIocsAndEnvs();

        selectedIocs = iocs.stream().filter(ioc -> prevSelectedIocs.contains(ioc)).collect(Collectors.toList());
        getConfigurations();
        selectedConfigs = configs.stream().filter(conf -> prevSelectedConfigs.contains(conf))
                .collect(Collectors.toList());

        verifyMessages = Collections.emptyList();
    }

    public List<IOC> getIocs() {
        return iocs;
    }

    public void setIocs(List<IOC> iocs) {
        this.iocs = iocs;
    }

    public List<IOC> getSelectedIocs() {
        return selectedIocs;
    }

    public void setSelectedIocs(List<IOC> selectedIocs) {
        this.selectedIocs = selectedIocs;
        this.configs = null;
    }

    public void clearSelectedIocs() {
        selectedIocs.clear();
        this.configs = null;
    }

    public void clearConfigSelection() {
        this.configs = null;
    }

    public IOC getSelectedIoc() {
        return selectedIocs != null && selectedIocs.size() == 1 ? selectedIocs.get(0) : null;
    }

    public boolean canGenerate() {
        // check if all the selected IOC's exist in CCDB and their hostnames exist.
        boolean areAllSelectedIocsValid = selectedIocs.stream()
                .allMatch(selectedIoc ->
                        (!selectedIoc.getConsistencyStatus().contains(IOCConsistencyStatus.IOC_NOT_IN_CCDB)) &&
                        selectedIoc.getHostname() != null);
        return (canDeployProduction || canDeployNonProduction) && areAllSelectedIocsValid;
    }

    public boolean canUndeployConfiguration() {
        return canDeployProduction || canDeployNonProduction;
    }

    /**
     * Getter for the list of configurations Loads the configurations for the selected iocs via the
     * {@link IOCConfigsCache}
     *
     * @return list of configurations
     */
    public List<Configuration> getConfigurations() {
        if (configs == null) {
            configs = selectedIocs == null ? Collections.emptyList()
                    : selectedIocs.stream().flatMap(ioc -> configCache.getConfigurationsForIOC(ioc).stream())
                            .collect(Collectors.toList());
        }

        return configs;
    }

    public List<Configuration> getSelectedConfigs() {
        return selectedConfigs;
    }

    public void setSelectedConfigs(List<Configuration> selectedConfigs) {
        this.selectedConfigs = selectedConfigs;
    }

    public List<IOCEnvironment> getEnvs() {
        return envs;
    }

    public IOCEnvironment getSelectedEnv() {
        return !selectedEnvs.isEmpty() ? selectedEnvs.get(0) : null;
    }

    public void setSelectedEnv(IOCEnvironment selectedEnv) {
        selectedEnvs.clear();
        selectedEnvs.add(selectedEnv);
    }

    public void clearSelectedEnvs() {
        selectedEnvs.clear();
    }

    public List<IOCEnvironment> getSelectedEnvs() {
        return selectedEnvs;
    }

    public void setSelectedEnvs(List<IOCEnvironment> selectedEnvs) {
        this.selectedEnvs = selectedEnvs;
    }

    public String getPreviewEnv() {
        return previewEnv;
    }

    public String getPreviewSt() {
        return previewSt;
    }

    public List<ConfigVerifyUIStatus> getVerifyMessages() {
        return verifyMessages;
    }

    public void clearVerifyMessages() {
        verifyMessages = new ArrayList<>();
    }

    public List<GenerateEntry> getLastGenerateEntries() {
        return lastGenerateEntries;
    }

    public GenerateDialogManager getGenerateDialogManager() {
        return generateDialogManager;
    }

    public UndeployDialogManager getUndeployDialogManager() {
        return undeployDialogManager;
    }

    public void verify() {
        try {
            verifyForGenerate(selectedConfigs);
        } catch (ConfigClientException ex) {
            LOG.log(Level.SEVERE, "Failed to access the CCDB in verify.", ex);
            UiUtility.showErrorMessage("Failed to access CCDB", ex);
        }
    }

    public void verifyForGenerate(final Collection<Configuration> cfgs) throws ConfigClientException {
        clearVerifyMessages();
        if (selectedEnvs.isEmpty()) {
            return;
        }
        IOCEnvironment selectedEnv = selectedEnvs.get(0);
        if (cfgs != null && !cfgs.isEmpty()) {
            for (final Configuration config : cfgs) {
                final Configuration loadedConfig = configService.loadConfiguration(config.getId());

                for (final ConfigVerifyStatus msg : verifyService.verifyConfiguration(loadedConfig, selectedEnv)) {
                    verifyMessages.add(
                            new ConfigVerifyUIStatus(loadedConfig.getIoc().getName(), loadedConfig.getRevision(), msg));
                }
            }
        }
    }

    /**
     * Dry runs generation and show the results.
     */
    public void preview() {
        if (selectedConfigs == null || selectedConfigs.size() != 1) {
            UiUtility.showInfoMessageDialog("Preview", "Please select only a single configuration.");
            return;
        }

        final Configuration config = selectedConfigs.get(0);

        try {
            final Pair<String, String> previews = generateService
                    .previewConfiguration(configService.loadConfiguration(config.getId()),
                            Util.getRepository() instanceof E3Repository);
            previewEnv = previews.getLeft();
            previewSt = previews.getRight();

            PrimeFaces.current().executeScript("PF('generatePreviewDlg').show()");
        } catch (GenerateService.GenerationException ge) {
            LOG.log(Level.SEVERE, ge.getMessage(), ge);
            UiUtility.showErrorMessage(GenerateDialogManager.ERROR_MESSAGE,
                    "Error while previewing IOC configuration: " + ge.getMessage(), ge);
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Unknown exception during configuration generation.", e);
            UiUtility.showErrorMessage(GenerateDialogManager.ERROR_MESSAGE,
                    "Unexpected error happened during IOC script generation, "
                    + "please notify administrator.\nThe exception has been logged.", e);
        }
    }

    /**
     * Reloads the list of last generated configurations for all the IOC's.
     */
    public void loadLastGenerateEntries() {
        lastGenerateEntries = generateEntryService.getLastGenerateEntriesForIocs();
    }

    /**
     * Reloads the list of IOC's and the list of environments.
     */
    public void loadIocsAndEnvs() {
        try {
            loadLastGenerateEntries();
            iocs = configCache.getIOCs().stream()
                    .filter(ioc -> !configCache.getConfigurationsForIOC(ioc).isEmpty() ||
                            generateEntryService.isGenerateEntryPresentInFilesystem(ioc, lastGenerateEntries))
                    .collect(Collectors.toList());
            configs = null;
        } catch (ConfigClientException ce) {
            LOG.log(Level.SEVERE, "Failed accessing the CCDB in init()", ce);
            UiUtility.showErrorMessage("Failed to access CCDB", ce);
        }

        iocs.sort((l, r) -> l.getName().compareTo(r.getName()));

        List<IOCEnvironment> loadedEnvs = setupService.loadEnvs();
        if (loadedEnvs == null) {
            loadedEnvs = Collections.emptyList();
        }

        envs = loadedEnvs.stream().filter(env -> env.getEeeType() == repoSelector.getSelectedEee()).filter(
                env -> (env.isProduction() && canDeployProduction) || (!env.isProduction() && canDeployNonProduction))
                .collect(Collectors.toList());

        selectedEnvs = new ArrayList<>();
        if (!envs.isEmpty()) {
            selectedEnvs.add(envs.get(0));
        }
    }
}
