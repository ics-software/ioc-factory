/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.ui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.lang3.mutable.MutableBoolean;
import se.esss.ics.iocfactory.model.GenerateEntry;
import se.esss.ics.iocfactory.model.IOC;
import se.esss.ics.iocfactory.model.IOCConsistencyStatus;
import se.esss.ics.iocfactory.service.ConfigClientException;
import se.esss.ics.iocfactory.service.GenerateEntryService;
import se.esss.ics.iocfactory.service.IOCConfigsCache;
import se.esss.ics.iocfactory.service.IOCService;
import se.esss.ics.iocfactory.service.SecurityService;
import se.esss.ics.iocfactory.service.cdievents.IOCUpdated;
import se.esss.ics.iocfactory.ui.cdievents.IocsSelected;
import se.esss.ics.iocfactory.ui.cdiqualifiers.ConfigDirty;
import se.esss.ics.iocfactory.util.UiUtility;

/**
 * A JSF backing bean for handling the IOCs on the Configure screen
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 *
 */
@Named
@ViewScoped
public class IocsManager implements Serializable {
    private static final Logger LOG = Logger.getLogger(IocsManager.class.getCanonicalName());

    @Inject private transient IOCService iocService;
    @Inject private transient IOCConfigsCache iocCache;
    @Inject private transient SecurityService securityService;
    @Inject private transient GenerateEntryService generateEntryService;
   
    @Inject
    @ConfigDirty
    private transient MutableBoolean configDirty;

    @Inject
    @IocsSelected
    private transient Event<List<IOC>> iocsSelectedEvent;

    private List<IOC> iocs = null;
    private List<IOC> selectedIOCs = Collections.emptyList();

    private boolean canConfigureIOCs;

    /**
     * Initializes the backing bean.
     */
    @PostConstruct
    public void init() {
        canConfigureIOCs = securityService.canConfigureIOCs();
    }

    public List<IOC> getSelectedIOCs() { return selectedIOCs; }

    /** *  Handles the selection of IOCs in the IOCs table
     *
     * Fires the {@link IocsSelected} event for consumers.
     *
     * @param selectedIOCs
     */
    public void setSelectedIOCs(List<IOC> selectedIOCs) {
        if (configDirty.isFalse()) {
            this.selectedIOCs = selectedIOCs;
            iocsSelectedEvent.fire(selectedIOCs);
        }
    }


    public List<IOC> getIocs() {
        if (iocs == null) {
            retrieveAndSortIocs();
        }
        return iocs;

    }


    public boolean isSingleSelected() { return selectedIOCs!= null && selectedIOCs.size()==1; }
    public IOC getSelectedIOC() { return selectedIOCs != null && selectedIOCs.size()==1 ? selectedIOCs.get(0) : null; }

    /**
     * @return true when "Yes" button is enabled, false when not.
     */
    public boolean isYesButtonEnabled() {
        final IOC selIoc = getSelectedIOC();
        if (selIoc == null || !selIoc.getConsistencyStatus().contains(IOCConsistencyStatus.IOC_NOT_IN_CCDB)) {
            return false;
        }

        return generateEntryService.getGenerateEntriesPresentInFileSystem(selIoc.getName()).isEmpty();
    }

    /**
     * Renames the selected saved IOC
     */
    public void renameSavedIoc() {
        // ToDo Implement renameSavedIoc
        LOG.info("renameSavedIoc called");
        UiUtility.showInfoMessage("Rename saved IOC invoked " + getSelectedIOC().getName());
    }

    /**
     * Deletes the selected saved IOCs
     */
    public void deleteIoc() {
        if (getSelectedIOC() == null) {
            return;
        }

        final IOC selIoc = getSelectedIOC();
        if (!selIoc.getConsistencyStatus().contains(IOCConsistencyStatus.IOC_NOT_IN_CCDB)){
            return;
        }

        if (iocService.deleteIOC(selIoc)) {
            UiUtility.showInfoMessage("Successfully deleted the IOC " + selIoc.getName()
                    + " from the database.");

            // reload the ioc list and clear selection
            retrieveAndSortIocs();
            selectedIOCs = Collections.emptyList();
            iocsSelectedEvent.fire(selectedIOCs);
        } else {
            UiUtility.showWarningMessage("IOC was not deleted. Only IOCs not reported by the CCDB can be deleted.");
        }
    }

    public String getDelIocMessage() {
        final String errMsg = "Only single IOC that no longer exists in CCDB can be deleted";
        if (getSelectedIOC() == null) {
            return errMsg;
        }

        final IOC selIoc = getSelectedIOC();
        if (!selIoc.getConsistencyStatus().contains(IOCConsistencyStatus.IOC_NOT_IN_CCDB)) {
            return errMsg;
        }

        final List<GenerateEntry> generateEntryList
                = generateEntryService.getGenerateEntriesPresentInFileSystem(selIoc.getName());

        if (generateEntryList.isEmpty()) {
            return String.format("There are %d configurations saved for the IOC %s.<br/>"
                                         + "Are you sure to delete all from the storage ?",
                                 iocService.countIOCDbConfigs(selIoc.getName()),
                                 selIoc.getName());
        } else {
            return String.format("The selected IOC has deployed configurations.<br/>"
                                     + "Please undeploy them in \"Generate IOC\".");
        }
    }

    public boolean canDeleteIoc() {
        return getSelectedIOC() != null && getSelectedIOC().getConsistencyStatus().
                contains(IOCConsistencyStatus.IOC_NOT_IN_CCDB) && canConfigureIOCs;
    }


    /**
     * Reloads the IOCs maintaining user selection, and fires the IOCSelected event to update dependants
     */
    public void reload() {
        retrieveAndSortIocs();
        iocsSelectedEvent.fire(selectedIOCs);
    }

    public void handleIOCUpdated(@Observes @IOCUpdated final IOC ioc) {
        iocs = null;
    }

    /**
     * Reloads IOCs keeping selection
     */
    private void retrieveAndSortIocs() {
        try {
            final List<IOC> prevSelected = selectedIOCs;

            iocs = new ArrayList<>(iocCache.getIOCs());

            selectedIOCs = iocs.stream().
                filter(ioc -> prevSelected.contains(ioc)).collect(Collectors.toList());
        } catch (ConfigClientException ce) {
            LOG.log(Level.SEVERE, "Failed accessing the CCDB in init()", ce);
            UiUtility.showErrorMessage("Failed to access CCDB", ce);
            return;
        }
        // Using null safe compares
        iocs.sort((l, r) -> Objects.compare(l.getName(), r.getName(), (o1, o2) -> {
            if (o1 == null && o2 == null) {
                return 0;
            } else if (o1 == null) {
                return -1;
            } else if (o2 == null) {
                return 1;
            } else {
                return o1.compareTo(o2);
            }
        }));
    }
}
