/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.ui;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.google.common.collect.ImmutableList;

import se.esss.ics.iocfactory.model.DeviceConfiguration;
import se.esss.ics.iocfactory.service.ConfigClientException;
import se.esss.ics.iocfactory.service.ConfigurationService;
import se.esss.ics.iocfactory.util.UiUtility;

/**
 * Handles the Rename device dialog functionality on the Configure screen
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
@ViewScoped
@Named
public class RenameDevManager implements Serializable {
    private static final Logger LOG = Logger.getLogger(RenameDevManager.class.getName());

    private static final String DEV_MENU = "renameDevForm:devMenu";

    @Inject
    private transient DetailsManager detailsManager;
    @Inject
    private transient ConfigurationService configService;

    private DeviceConfiguration newDevice;

    /**
     * Retrieves the valid devices for the configuration as a {@link List}
     * 
     * @return list of device configurations
     */
    public List<DeviceConfiguration> getValidDeviceConfigs() {
        if (detailsManager.getConfig() == null) {
            return Collections.emptyList();
        }

        return detailsManager.getConfig().getDevices().stream().filter(dev -> !dev.getConsistencyStatus().isError())
                .collect(Collectors.toList());
    }

    /**
     * Handles the "Rename" button click on hte dialog
     */
    public void rename() {
        if (newDevice == null) {
            UiUtility.invalidateComponent(DEV_MENU, "No device selected !");
            return;
        } else if (detailsManager.getSelectedDevice() == null) {
            UiUtility.invalidateComponent(DEV_MENU, "No device to rename selected !");
            return;
        } else if (!detailsManager.getSelectedDevice().getConsistencyStatus().isError()) {
            UiUtility.invalidateComponent(DEV_MENU, "The device to rename must have errors !");
            return;
        }

        try {
            configService.renameInvalidDevice(detailsManager.getSelectedDevice(), newDevice);
            detailsManager.makeConfigDirty();

            detailsManager.renumerateAndPrepareConfig();

            detailsManager.setSelectedDevices(ImmutableList.of(newDevice));
        } catch (ConfigClientException ex) {
            LOG.log(Level.SEVERE, "Failed accessing the CCDB in rename()", ex);
            UiUtility.showErrorMessage("Failed to access CCDB", ex);
            return;
        } catch (Exception e) {
            LOG.log(Level.SEVERE, e.getMessage(), e);
            UiUtility.showUnexpectedErrorMessage(
                    "Renaming failed. Internal error. Please consult the product owners.", e);
            return;
        }
        UiUtility.showInfoMessage("Device renamed successfully");
    }

    public DeviceConfiguration getNewDevice() {
        return newDevice;
    }

    public void setNewDevice(DeviceConfiguration selectedDevice) {
        this.newDevice = selectedDevice;
    }
}
