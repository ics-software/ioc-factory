/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.ui;

import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.apache.commons.io.FileUtils;

import se.esss.ics.iocfactory.model.DeviceConfiguration;
import se.esss.ics.iocfactory.model.Module;
import se.esss.ics.iocfactory.model.GenerateEntry;

/**
 * Backing bean for holding the textual representation of a Configuration when
 * viewing the details of a generate entry
 *
 * in the Browse screen
 * 
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
@Named
@ViewScoped
public class TextManager implements Serializable {
    private static final Logger LOG = Logger.getLogger(TextManager.class.getName());

    private String header = "";
    private String text = "";

    /**
     * Initializes the text from a generate entry YAML dump
     * 
     * @param ge
     */
    public void initFromGenerateEntry(GenerateEntry ge) {
        text = ge.getDump();
    }

    public void initFromDevice(DeviceConfiguration dev, String snippetName) {
        text = "";
        header = "";
        
        if (dev == null) {
            return;
        } else {
            final Module module = dev.getSupportModule();
            dev.getSnippets().stream().filter(sn -> Objects.equals(snippetName,
                    sn.getName())).findAny().ifPresent(snippet -> {
                header = String.format("Module %s ver. %s - Snippet %s", module != null ? module.getName() : "",
                        module != null ? module.getModuleVersion() : "", snippet.getName());
                try {
                    text = snippet != null ? 
                            FileUtils.readFileToString(snippet.getFile(), StandardCharsets.US_ASCII) : "";
                } catch (IOException ex) {
                    LOG.log(Level.WARNING, "Failed to load snippet for display", ex);
                    text = "";
                }
            });
        }
    }

    public String getHeader() {
        return header;
    }

    public String getText() {
        return text;
    }
}
