/*
 * Copyright (c) 2017 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package se.esss.ics.iocfactory.ui;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 * Manager for loading properties required by the UI.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Named
@ViewScoped
public class UIPropertiesManager implements Serializable {

    private static final long serialVersionUID = -8496869471465056456L;
    public static final String SUPPORT_EMAIL_PROPERTY = "iocfactory.supportEmail";
    private static final Logger LOGGER = Logger.getLogger(UIPropertiesManager.class.getCanonicalName());

    /** @return the support email */
    public String getSupportEmail() {
        final String supportEmail = System.getProperty(SUPPORT_EMAIL_PROPERTY, null);
        LOGGER.log(Level.FINE, "Support email: " + supportEmail);
        return supportEmail;
    }
}
