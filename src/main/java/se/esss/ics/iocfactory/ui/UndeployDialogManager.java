/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.ui;

import org.primefaces.PrimeFaces;
import se.esss.ics.iocfactory.configuration.IOCEnvironment;
import se.esss.ics.iocfactory.model.AuditEntry;
import se.esss.ics.iocfactory.model.ConfigVerifyStatus;
import se.esss.ics.iocfactory.model.ConfigVerifyUIStatus;
import se.esss.ics.iocfactory.model.ConsistencyMsgLevel;
import se.esss.ics.iocfactory.model.GenerateEntry;
import se.esss.ics.iocfactory.model.IOC;
import se.esss.ics.iocfactory.service.AuditService;
import se.esss.ics.iocfactory.service.GenerateEntryService;
import se.esss.ics.iocfactory.service.GenerateService;
import se.esss.ics.iocfactory.service.IOCConfigsCache;
import se.esss.ics.iocfactory.util.UiUtility;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A JSF backing bean for handling the Undeploy configuration dialog
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 *
 */
@Named
@ViewScoped
public class UndeployDialogManager extends GenerateDialogBase implements Serializable {
    private static final Logger LOG = Logger.getLogger(UndeployDialogManager.class.getCanonicalName());

    private static final String TITLE = "Undeploy configurations";
    private static final String ERROR_MESSAGE = "Undeploy configurations failed";

    private static final String CONFIG_LIST_TITLE = "Following configurations will be undeployed:";

    @Inject
    private transient GenerateEntryService generateEntryService;
    @Inject
    private transient AuditService auditService;
    @Inject
    private transient IOCConfigsCache configCache;

    /**
     * The non-private, no-arg constructor is needed for the implementation to create a proxy of the managed bean:
     * https://stackoverflow.com/questions/46543657/cdi-object-not-proxyable-with-injected-constructor
     */
    protected UndeployDialogManager() {
        super();
    }

    @Inject
    public UndeployDialogManager(GenerateManager generateManager) {
        super(generateManager);
    }

    /**
     * Initializes the Undeploy Configuration Dialog backing bean.
     */
    @PostConstruct
    public void init() {
        super.init();
    }

    /**
     * @return title of displayed configuration list.
     */
    public String getConfigListTitle() {
        return CONFIG_LIST_TITLE;
    }

    /**
     * @return list of messages to display in configuration list.
     */
    public List<String> getGenerateMessageList() {
        return generateMessageList;
    }

    /**
     * Check the configurations and prepare the data for the configuration-undeployment dialog.
     */
    public void prepareAndShow() {
        generateManager.loadLastGenerateEntries();

        verifyForUndeployConfigurations();

        PrimeFaces.current().executeScript("PF('confirmUndeployDlg').show()");
    }

    /**
     * A callback that is invoked when the user selects or de-selects the environment row.
     */
    public void onEnvRowSelectionChange() {
        verifyForUndeployConfigurations();
    }

    private void verifyForUndeployConfigurations() {
        final String CATEGORY_GENERATED_CONFIG = "Generated configuration";
        if (generateManager.getSelectedIocs() == null) {
            return;
        }
        // Update message with the list of configurations that will be undeployed.
        List<GenerateEntry> generateEntryesToUndeploy = getGenerateEntriesToUndeploy();
        generateMessageList = new ArrayList<String>();
        generateEntryesToUndeploy.stream().forEach(generateEntry -> {
            String undeployMessage = String.format("%s, rev: %d, env: %s", generateEntry.getIocName(),
                    generateEntry.getConfigRevision(), generateEntry.getEnvironment());
            if (!generateMessageList.contains(undeployMessage)) {
                // The undeploy list should contain no duplicate messages.
                generateMessageList.add(undeployMessage);
            }
        });

        // Update "Verification messages" table.
        generateManager.clearVerifyMessages();
        for (final IOC ioc : generateManager.getSelectedIocs()) {
            for (final IOCEnvironment env : generateManager.getSelectedEnvs()) {
                List<GenerateEntry> generateEntriesForIocAndEnv =
                        generateEntryService.getGenerateEntriesPresentInFileSystem(ioc.getName(), env.getName());
                if (!generateEntriesForIocAndEnv.isEmpty()) {
                    for (GenerateEntry generateEntry : generateEntriesForIocAndEnv) {
                        final AuditEntry auditEntry = new AuditEntry(ioc, env.getName(), generateEntry);
                        auditService.checkDiffers(auditEntry);
                        for (final ConfigVerifyStatus msg :
                                auditEntry.getConfigVerifyStatus(CATEGORY_GENERATED_CONFIG)) {
                            generateManager.getVerifyMessages().add(
                                    new ConfigVerifyUIStatus(ioc.getName(), generateEntry.getConfigRevision(),
                                            msg));
                        }
                    }
                } else {
                    final ConfigVerifyStatus noGenerateConfigVerifyStatus = new ConfigVerifyStatus(
                            ConsistencyMsgLevel.WARN,
                            String.format("There are no configuration directories generated for environment %s",
                                    env.getName()),
                            CATEGORY_GENERATED_CONFIG
                    );
                    generateManager.getVerifyMessages().add(
                            new ConfigVerifyUIStatus(ioc.getName(), ioc.getLastConfigRevision(),
                                    noGenerateConfigVerifyStatus));
                }
            }
        }
    }

    private List<GenerateEntry> getGenerateEntriesToUndeploy() {
        List<GenerateEntry> generateEntriesToUndeploy = new ArrayList<>();
        for (IOCEnvironment env : generateManager.getSelectedEnvs()) {
            for (IOC ioc : generateManager.getSelectedIocs()) {
                List<GenerateEntry> generateEntries =
                        generateEntryService.getGenerateEntriesPresentInFileSystem(ioc.getName(), env.getName());
                generateEntriesToUndeploy.addAll(generateEntries);
            }
        }
        return generateEntriesToUndeploy;
    }

    /**
     * A callback that is invoked when the user confirms the undeploy action.
     */
    public void onConfirm() {
        undeployConfigurations();
        generateManager.clearSelectedEnvs();
    }

    /**
     * Undeploy configurations for the selected IOC's. The generated configuration files are deleted.
     */
    private void undeployConfigurations() {
        if (!canDeployToAny()) {
            UiUtility.showErrorMessage("You don't have permissions to undeploy IOCs.");
            return;
        }

        try {
            final CompleteMessageBuilder generatedMessage = new CompleteMessageBuilder("undeployed");
            // Undeploy configurations
            List<GenerateEntry> generateEntriesToUndeploy = getGenerateEntriesToUndeploy();
            final List<IOCEnvironment> selectedEnvs = generateManager.getSelectedEnvs();

            for (GenerateEntry generateEntry : generateEntriesToUndeploy) {
                final boolean isProductionEnv = generateEntry.getProduction();
                if (canDeploy(isProductionEnv)) {

                    final boolean isGitEnv = selectedEnvs.stream()
                                                    .filter(env -> env.getName().equals(generateEntry.getEnvironment()))
                                                    .findFirst()
                                                    .orElseThrow(() -> new NoSuchElementException())
                                                    .isGitEnvironment();

                    generateService.undeployConfiguration(generateEntry, isGitEnv);

                    generatedMessage.add(generateEntry.getIocName(), generateEntry.getConfigRevision(),
                            generateEntry.getTargetPath(), isProductionEnv, false);

                    if (isProductionEnv) {
                        IOC ioc = configCache.getIOCByName(generateEntry.getIocName(), false);
                        iocUpdated.fire(ioc);
                    }
                }
            }
            generatedMessage.end();

            // Clear table selection
            generateManager.clearSelectedIocs();
            generateManager.clearSelectedEnvs();

            // In order to recalculate the value for the "Consistency Check" column, we have to clear the cache.
            configCache.clear();
            generateManager.loadIocsAndEnvs();

            UiUtility.showInfoMessageDialog(TITLE, generatedMessage.toString());
        } catch (GenerateService.GenerationException ge) {
            LOG.log(Level.SEVERE, ge.getMessage(), ge);
            UiUtility.showErrorMessage(ERROR_MESSAGE,
                    "Error while undeploying generated configuration: " + ge.getMessage(), ge);
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Unknown exception during the undeployment of the configuration.", e);
            UiUtility.showErrorMessage(ERROR_MESSAGE,
                    "Unexpected error happened during the undeployment of the configuration, " +
                            "please notify administrator.\nThe exception has been logged.", e);
        }
    }
}
