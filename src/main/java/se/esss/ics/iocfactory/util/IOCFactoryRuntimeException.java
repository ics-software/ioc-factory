package se.esss.ics.iocfactory.util;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public class IOCFactoryRuntimeException extends RuntimeException {

    /**
     * @param message the message
     * @see RuntimeException#RuntimeException(String)
     */
    public IOCFactoryRuntimeException(String message) {
        super(message);
    }
}
