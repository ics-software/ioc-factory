/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package se.esss.ics.iocfactory.util;

import java.util.regex.Pattern;

import org.apache.commons.lang3.ObjectUtils;

import se.esss.ics.iocfactory.model.VersionConverters;

/**
 * Provides utility function to compare module versions
 */
public class ModuleVersionsComparator {

    private ModuleVersionsComparator() {}

    private static Pattern MODULE_PATTERN = VersionConverters.VersionConverter.OPTIONAL_MODULE_PATTERN;
    private static VersionConverters.VersionConverter MODULE_CONVERTER = VersionConverters.getModuleConverter();

    /**
     * Compares two module versions with the following algorithm:
     *
     * If both convert to numeric representation, compare the numeric representation, if one of them is named module,
     * assume it has bigger version (a newer one) if both are named modules do a String.compare
     *
     * @param astr
     * @param bstr
     * @return the value astr equals bstr; a value less than 0 if astr < bstr; and a value greater than 0 if astr > bstr
     */
    public static int compare(final String astr, final String bstr) {
        final Long a = moduleStringToLong(astr);
        final Long b = moduleStringToLong(bstr);

        if (a != null && b != null) {
            return Long.compare(a, b);
        } else if (a == null && b != null) {
            return 1;
        } else if (a != null && b == null) {
            return -1;
        } else {
            return ObjectUtils.compare(astr, bstr);
        }
    }

    /**
     *
     * Compares two module versions with the following algorithm:
     *
     * If both convert to numeric representation, compare the numeric representation, if one of them is named module,
     * assume it has lower version (an older one) if both are named modules do a String.compare
     *
     * This method can be used when sorting module versions to prioritize on the most recent numbered version.
     *
     * @param astr
     * @param bstr
     * @return the value astr equals bstr; a value less than 0 if astr < bstr; and a value greater than 0 if astr > bstr
     */
    public static int comparePrioritizeNumbered(final String astr, final String bstr) {
        final Long a = moduleStringToLong(astr);
        final Long b = moduleStringToLong(bstr);

        if (a != null && b != null) {
            return Long.compare(a, b);
        } else if (a == null && b != null) {
            return -1;
        } else if (a != null && b == null) {
            return 1;
        } else {
            return ObjectUtils.compare(astr, bstr);
        }
    }

    private static Long moduleStringToLong(String moduleVersion) {
        return moduleVersion != null && MODULE_PATTERN.matcher(moduleVersion).matches()
                ? MODULE_CONVERTER.fromString(moduleVersion, false) : null;
    }

    @FunctionalInterface
    public static interface ModuleComparator {
        int compare(String astr, String bstr);
    }
}
