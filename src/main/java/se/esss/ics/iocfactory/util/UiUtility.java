package se.esss.ics.iocfactory.util;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.omnifaces.util.Components;
import org.omnifaces.util.Messages;
import org.primefaces.PrimeFaces;
import se.esss.ics.iocfactory.ui.ErrorDialogController;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public class UiUtility {

    public enum ErrorType {
        HANDLED, UNHANDLED
    }

    /**
     * This class is not to be instantiated.
     */
    private UiUtility() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Used in UI controller code to invalidate a UIInput component with a given error message
     *
     * @param clientId
     *            the component id
     * @param message
     *            the error message
     */
    public static void invalidateComponent(final String clientId, final String message) {
        final UIInput input = (UIInput) Components.findComponent(clientId);
        if (input != null) {
            input.setValid(false);
            Messages.add(FacesMessage.SEVERITY_ERROR, clientId, Util.escapeNewlinesAndTabsForHTML(message));
            FacesContext.getCurrentInstance().validationFailed();
            updateGrowl();
        }
    }

    /**
     * Utility method used to display an warning message to the user.
     *
     * @param message warning message
     */
    public static void showWarningMessage(final String message) {
        addGlobalWarn(message);
    }

    /**
     * Utility method used to display an error message to the user.
     *
     * @param explanation the explanation of the message
     */
    public static void showErrorMessage(final String explanation) {
        showErrorMessage(explanation, (Throwable) null);
    }

    /**
     * Utility method used to display an error message to the user.
     *
     * @param explanation the explanation of the message
     * @param throwable cause
     */
    public static void showErrorMessage(final String explanation, final Throwable throwable) {
        showErrorMessage(ErrorType.HANDLED, explanation, throwable);
    }

    /**
     * Utility method used to display an error message to the user.
     *
     * @param summary summary of the problem
     * @param details detailed explanation
     * @param throwable cause
     */
    public static void showErrorMessage(final String summary, final String details, final Throwable throwable) {
        showErrorMessage(ErrorType.HANDLED, summary + "\n"+ details, throwable);
    }

    /**
     * Utility method used to display an unexpected error message to the user.
     * (Behavior can be reported to support team from error dialog.)
     *
     * @param explanation the explanation of the message
     * @param throwable cause
     */
    public static void showUnexpectedErrorMessage(final String explanation, final Throwable throwable) {
        showErrorMessage(ErrorType.UNHANDLED, explanation, throwable);
    }

    /**
     * Utility method used to display an information message to the user after modified entity successfully.
     *
     * @param summary summary of the message
     */
    public static void showInfoMessage(final String summary) {
        addGlobalInfo(summary);
    }

    /**
     * Utility method used to display an information message to the user in dialog.
     *
     * @param header title
     * @param message details
     */
    public static void showInfoMessageDialog(final String header, final String message) {
        showDialog(FacesMessage.SEVERITY_INFO, header, message);
    }

    /**
     * Show faces error message to user with given content.
     *
     * @param message message
     *
     * @see FacesMessage#FacesMessage(javax.faces.application.FacesMessage.Severity, String, String)
     */
    public static void showErrorFacesMessage(final String message) {
        addGlobalError(message);
    }

    /**
     * Utility method used to display an error message to the user.
     *
     * @param errorType type of error
     * @param details error detail message to display
     * @param throwable exception if any occurred
     */
    public static void showErrorMessage(final ErrorType errorType, final String details, final Throwable throwable) {
        String stackTrace = throwable != null ? ExceptionUtils.getStackTrace(throwable) : "";

        ErrorDialogController errorDialogController = getErrorDialogController();
        errorDialogController.setDetails(splitDetails(details));
        errorDialogController.setStackTrace(stackTrace);
        errorDialogController.setUnexpectedError(ErrorType.UNHANDLED.equals(errorType));
        PrimeFaces.current().ajax().update("errorDialogForm:errorDialog");
        PrimeFaces.current().executeScript("PF('errorDialog').show();");
    }

    /**
     * Closes error dialog.
     */
    public static void dismissErrorMessage() {
        PrimeFaces.current().executeScript("PF('errorDialog').hide();");
    }

    private static List<String> splitDetails(final String details) {
        final List<String> split = Arrays.asList(StringUtils.split(details, "\n"));
        return split.stream().map(String::trim).filter(StringUtils::isNotEmpty).
                map(p -> p.replaceAll("\t", " ")).collect(Collectors.toList());
    }

    private static void addGlobalInfo(String message, Object... params) {
        Messages.addGlobalInfo(Util.escapeNewlinesAndTabsForHTML(message), params);
        updateGrowl();
    }

    private static void addGlobalWarn(String message, Object... params) {
        Messages.addGlobalWarn(Util.escapeNewlinesAndTabsForHTML(message), params);
        updateGrowl();
    }

    private static void addGlobalError(String message, Object... params) {
        Messages.addGlobalError(Util.escapeNewlinesAndTabsForHTML(message), params);
        updateGrowl();
    }

    private static void showDialog(FacesMessage.Severity severity, String header, String message) {
        PrimeFaces.current().dialog().showMessageDynamic(new FacesMessage(severity, escape(header), escape(message)));
    }

    private static ErrorDialogController getErrorDialogController() {
        FacesContext context = FacesContext.getCurrentInstance();
        Application application = context.getApplication();
        return application.evaluateExpressionGet(context, "#{errorDialogController}", ErrorDialogController.class);
    }

    private static void updateGrowl() {
        PrimeFaces.current().ajax().update("growl");
    }

    private static String escape(String s) {
        return s.replaceAll("\\\\", "\\\\\\\\").replaceAll("'", "\\\\'").
                replaceAll("\n", "\\\\n");
    }
}
