/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.util;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import javax.ejb.EJBAccessException;
import javax.enterprise.inject.spi.CDI;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.commons.codec.digest.DigestUtils;

import se.esss.ics.iocfactory.configuration.EEEType;
import se.esss.ics.iocfactory.configuration.IOCFactSetup;
import se.esss.ics.iocfactory.model.Module;
import se.esss.ics.iocfactory.model.ModuleDependency;
import se.esss.ics.iocfactory.model.Repository;
import se.esss.ics.iocfactory.service.RepositoryProducer;

/**
 * A project wide utility function class.
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 *
 */
public class Util {

    @FunctionalInterface
    public interface EJBProcessor {
        boolean execute();
    }

    private static final Logger LOG = Logger.getLogger(Util.class.getName());

    /**
     * Private constructor to prevent class instantiation
     */
    private Util() {}

    /**
     * {@link EntityManager} helper for getting a single result without exception handling
     *
     * @param query
     *            a {@link Query} object
     * @return the single value or <code>null</code> if not present
     */
    public static Object getSingleResult(Query query) {
        query.setMaxResults(1);
        List<?> list = query.getResultList();
        if (list == null || list.isEmpty()) {
            return null;
        }

        return list.get(0);
    }

    public static Repository getRepository() {
        return CDI.current().select(RepositoryProducer.class).get().getRepository();
    }

    public static void checkRepoChanged() {
        CDI.current().select(RepositoryProducer.class).get().checkRepoChanged();
    }

    public static String ensureTrailingSlash(String str) {
        return str.endsWith("/") ? str : str + "/";
    }

    public static String urlEncode(String input) {
        try {
            return input != null ? URLEncoder.encode(input, "UTF-8") : null;
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException("Cataclysm UTF-8 is not a standard charset on the platform", ex);
        }
    }

    /**
     * Pretty prints the dependencies of a module
     *
     * @param depList
     * @return String representation of dependency list
     */
    public static String formatModuleDependencies(List<ModuleDependency> depList) {
        return depList.stream()
                .map(dep -> String.format("%s-%s%s", dep.getName(), dep.getVersion(), dep.isGreater() ? "+" : ""))
                .collect(Collectors.joining(", "));
    }

    /**
     * Pretty prints the dependencies of a module
     *
     * @param module
     * @return String representation of dependency
     */
    public static String formatModuleDependencies(Module module) {
        if (module == null) {
            return null;
        }
        return formatModuleDependencies(module.getDependencies());
    }

    /**
     * Tests that a File is a readable directory
     *
     * @param dir
     *            the file to test
     * @return true if it is a readable directory
     */
    public static boolean testIfReadableDir(File dir) {
        return dir != null && dir.exists() && dir.isDirectory() && dir.canRead();
    }

    public static MessageDigest getSHA1MessageDigest() {
        return DigestUtils.getDigest("SHA-1");
    }

    public static OptionalProcedure callSecuredEJB(Logger log, Runnable processor) {
        try {
            processor.run();
        } catch (EJBAccessException accessException) {
            log.log(Level.SEVERE, "Access denied", accessException);
            UiUtility.showErrorMessage("Access denied for the operation", accessException);
            return new OptionalProcedure(false);
        }
        return new OptionalProcedure(true);
    }

    public static OptionalProcedure callSecuredEJB(Logger log, EJBProcessor processor) {
        boolean success = false;
        try {
            success = processor.execute();
        } catch (EJBAccessException accessException) {
            log.log(Level.SEVERE, "Access denied", accessException);
            UiUtility.showErrorMessage("Access denied for the operation", accessException);
        }
        return new OptionalProcedure(success);
    }

    public static String escapeNewlinesAndTabsForHTML(String input) {
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < input.length(); i++) {
            final char val = input.charAt(i);
            if (val == '\n') {
                sb.append("<br />");
            } else if (val == '\t') {
                sb.append("&nbsp&nbsp&nbsp&nbsp");
            } else {
                sb.append(val);
            }
        }
        return sb.toString();
    }

    public static String getViewId() {
        return FacesContext.getCurrentInstance().getViewRoot().getViewId();
    }

    public static String getBaseDir(EEEType eeeType, final IOCFactSetup setup) {
        switch (eeeType) {
        case PRODUCTION:
            return setup.getEpicsBaseDirectory();
        case SANDBOX:
            return setup.getSandboxBaseDirectory();
        case PRODUCTION_E3:
            return setup.getEpicsE3BaseDirectory();
        default:
            throw new RuntimeException("Unhandeled enum value");
        }
    }

    public static <T> Stream<T> streamFromOptional(final Optional<T> opt) {
        return opt.map(Stream::of).orElseGet(Stream::empty);
    }

    public static <T> Stream<T> streamFromIterator(final Iterator<T> iter) {
        final Iterable<T> iterable = () -> iter;
        return StreamSupport.stream(iterable.spliterator(), false);
    }
}
