ALTER TABLE iocenvironment ADD COLUMN git_environment BOOLEAN NOT NULL DEFAULT FALSE;
ALTER TABLE iocenvironment ADD COLUMN git_repository_path character varying(255);
