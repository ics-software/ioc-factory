CREATE TABLE os_alias (
                                 id bigserial NOT NULL,
                                 eee_os_name varchar(64) NOT NULL,
                                 e3_os_name varchar(64) NOT NULL,
                                 CONSTRAINT os_alias_un UNIQUE (eee_os_name, e3_os_name)
);
CREATE UNIQUE INDEX os_alias_id_idx ON os_alias USING btree (id);