--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.0
-- Dumped by pg_dump version 9.5.0

-- Started on 2016-04-12 22:43:28

SET default_with_oids = false;

--
-- TOC entry 180 (class 1259 OID 33691)
-- Name: configuration; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE configuration (
    id bigint NOT NULL,
    edit_comment character varying(255),
    commited boolean NOT NULL,
    env_version bigint,
    epics_version bigint,
    config_hash character varying(40),
    last_edit timestamp without time zone,
    optimistic_version bigint,
    os character varying(255),
    output_hash character varying(40),
    proc_serv_port integer NOT NULL,
    revision integer NOT NULL,
    username character varying(255),
    ioc_name character varying(255) NOT NULL
);


--
-- TOC entry 181 (class 1259 OID 33699)
-- Name: device_configuration; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE device_configuration (
    id bigint NOT NULL,
    device_name character varying(255),
    optimistic_version bigint,
    snippet_name character varying(255),
    epics_version bigint,
    module_version bigint,
    module_name character varying(255),
    os character varying(255),
    configuration_id bigint NOT NULL,
    item_no integer
);


--
-- TOC entry 182 (class 1259 OID 33707)
-- Name: device_parameter; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE device_parameter (
    id bigint NOT NULL,
    param_name character varying(255),
    optimistic_version bigint,
    param_type character varying(255) NOT NULL,
    param_value character varying(255),
    configuration_id bigint,
    device_configuration_id bigint,
    device_item_no integer,
    global_item_no integer
);


--
-- TOC entry 183 (class 1259 OID 33715)
-- Name: generate_entry; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE generate_entry (
    id bigint NOT NULL,
    config_id bigint,
    config_rev integer,
    dump oid,
    environment character varying(255),
    gen_timestamp timestamp without time zone,
    gen_hash character varying(40),
    hostname character varying(255),
    ioc_name character varying(255),
    os character varying(255),
    production boolean,
    target_path character varying(255),
    gen_user character varying(255)
);


--
-- TOC entry 188 (class 1259 OID 33779)
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 184 (class 1259 OID 33723)
-- Name: ioc; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE ioc (
    ioc_name character varying(255) NOT NULL,
    last_config_revision integer,
    optimistic_version bigint
);


--
-- TOC entry 185 (class 1259 OID 33728)
-- Name: iocenvironment; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE iocenvironment (
    id bigint NOT NULL,
    directory character varying(255),
    env_name character varying(255),
    optimistic_version bigint,
    production boolean NOT NULL
);


--
-- TOC entry 186 (class 1259 OID 33736)
-- Name: iocfact_setup; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE iocfact_setup (
    id bigint NOT NULL,
    ccdb_dependency_propery character varying(255),
    ccdb_os_property character varying(255),
    ccdb_snippet_property character varying(255),
    epics_base_directory character varying(255),
    ioc_device_types character varying(255),
    optimistic_version bigint
);


--
-- TOC entry 187 (class 1259 OID 33744)
-- Name: log_entry; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE log_entry (
    id bigint NOT NULL,
    details text,
    entry_timestamp timestamp without time zone,
    entry_type character varying(255),
    entry_user character varying(40)
);


--
-- TOC entry 2017 (class 2606 OID 33698)
-- Name: configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY configuration
    ADD CONSTRAINT configuration_pkey PRIMARY KEY (id);


--
-- TOC entry 2020 (class 2606 OID 33706)
-- Name: device_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY device_configuration
    ADD CONSTRAINT device_configuration_pkey PRIMARY KEY (id);


--
-- TOC entry 2023 (class 2606 OID 33714)
-- Name: device_parameter_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY device_parameter
    ADD CONSTRAINT device_parameter_pkey PRIMARY KEY (id);


--
-- TOC entry 2027 (class 2606 OID 33722)
-- Name: generate_entry_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY generate_entry
    ADD CONSTRAINT generate_entry_pkey PRIMARY KEY (id);


--
-- TOC entry 2030 (class 2606 OID 33727)
-- Name: ioc_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY ioc
    ADD CONSTRAINT ioc_pkey PRIMARY KEY (ioc_name);


--
-- TOC entry 2032 (class 2606 OID 33735)
-- Name: iocenvironment_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY iocenvironment
    ADD CONSTRAINT iocenvironment_pkey PRIMARY KEY (id);


--
-- TOC entry 2036 (class 2606 OID 33743)
-- Name: iocfact_setup_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY iocfact_setup
    ADD CONSTRAINT iocfact_setup_pkey PRIMARY KEY (id);


--
-- TOC entry 2038 (class 2606 OID 33751)
-- Name: log_entry_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY log_entry
    ADD CONSTRAINT log_entry_pkey PRIMARY KEY (id);


--
-- TOC entry 2034 (class 2606 OID 33758)
-- Name: uk_dsml5wlt9f7sx0kuql9i2tb91; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY iocenvironment
    ADD CONSTRAINT uk_dsml5wlt9f7sx0kuql9i2tb91 UNIQUE (env_name);


--
-- TOC entry 2018 (class 1259 OID 33752)
-- Name: idx_configuration_ioc; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_configuration_ioc ON configuration USING btree (ioc_name);


--
-- TOC entry 2021 (class 1259 OID 33753)
-- Name: idx_device_configuration_configuration; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_device_configuration_configuration ON device_configuration USING btree (configuration_id);


--
-- TOC entry 2024 (class 1259 OID 33754)
-- Name: idx_device_parameter_configuration; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_device_parameter_configuration ON device_parameter USING btree (configuration_id);


--
-- TOC entry 2025 (class 1259 OID 33755)
-- Name: idx_device_parameter_device_configuration; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_device_parameter_device_configuration ON device_parameter USING btree (device_configuration_id);


--
-- TOC entry 2028 (class 1259 OID 33756)
-- Name: uk_e8favssketo6a8ee7f7krwjbr; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX uk_e8favssketo6a8ee7f7krwjbr ON generate_entry USING btree (config_id);


--
-- TOC entry 2042 (class 2606 OID 33774)
-- Name: fk_86d7jqc3h9l7h43sjdjv90xtt; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY device_parameter
    ADD CONSTRAINT fk_86d7jqc3h9l7h43sjdjv90xtt FOREIGN KEY (device_configuration_id) REFERENCES device_configuration(id);


--
-- TOC entry 2041 (class 2606 OID 33769)
-- Name: fk_c3g72rvjb0q1ts1yvgvuo8kfp; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY device_parameter
    ADD CONSTRAINT fk_c3g72rvjb0q1ts1yvgvuo8kfp FOREIGN KEY (configuration_id) REFERENCES configuration(id);


--
-- TOC entry 2040 (class 2606 OID 33764)
-- Name: fk_lk87pogbx7xt6wwatlx94l9kj; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY device_configuration
    ADD CONSTRAINT fk_lk87pogbx7xt6wwatlx94l9kj FOREIGN KEY (configuration_id) REFERENCES configuration(id);


--
-- TOC entry 2039 (class 2606 OID 33759)
-- Name: fk_qesll6ojt6kuicl590lvnwr4i; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY configuration
    ADD CONSTRAINT fk_qesll6ojt6kuicl590lvnwr4i FOREIGN KEY (ioc_name) REFERENCES ioc(ioc_name);


-- Completed on 2016-04-12 22:43:28

--
-- PostgreSQL database dump complete
--

