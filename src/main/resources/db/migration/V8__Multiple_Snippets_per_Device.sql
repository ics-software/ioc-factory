ALTER TABLE device_parameter DROP COLUMN device_item_no;
ALTER TABLE device_parameter ADD COLUMN snippet_name character varying(255);
UPDATE device_parameter 
    SET snippet_name = (  SELECT device_configuration.snippet_name FROM device_configuration WHERE 
        device_configuration.id = device_parameter.device_configuration_id );
ALTER TABLE device_configuration DROP COLUMN snippet_name;
