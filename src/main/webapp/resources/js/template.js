/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */

showBusyDlg = false;
lastBusyCheck = 0;

function handleAjaxStart() {
    var TIMEOUT = 1000;

    showBusyDlg = true;
    lastBusyCheck = (new Date()).getTime();
    window.setTimeout(function (){
        thisBusyCheck = (new Date()).getTime();
        if (showBusyDlg && (thisBusyCheck - lastBusyCheck) > TIMEOUT) {
            PF('busyDlg').show();
            showBusyDlg = false;
        }
    }, TIMEOUT);
}

function handleAjaxStop() {
    showBusyDlg = false;
    lastBusyCheck = 0;
    PF('busyDlg').hide();
}

function handleRepoChange(repo) {
    var currentRepo = $('#menuform\\:repoSelector_label').text();

    var detailMsg;

    if ( repo.localeCompare(currentRepo) === 0 ) {
        detailMsg = 'To use the updated data, please refresh the EEE.';
    } else {
        detailMsg = '';
        refreshOtherRepo();
    }

    var msg = {
        summary :  repo + ' EEE was updated.',
        detail : detailMsg,
        severity : 'info'
    };

    PF('repoChangeGrowl').show([msg]);
}

//workaround for lazy live scroll overlay panel command link bug
function showOverlayData(clientID){        
    var overpannel = PF('op');
    overpannel.show(clientID); 
    overpannel.content.html($('#'+clientID.replace('opLink', 'overlayDetails').replace(/:/g, '\\:')).html());
}

//Live scroll broken js fix code
function resetTableAllLoadedState() {
    var datatable = PF('entriesTbl');
    datatable.scrollOffset=0;    
    datatable.allLoadedLiveScroll = false;  
    datatable.scrollBody.scrollTop(1);  
}
