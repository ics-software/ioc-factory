/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Set;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.jupiter.api.Test;

public class RepositoryTest {
    private EEERepository repo;

    @Before
    public void init() {
        repo = new EEERepository();

        repo.addEPICSVersion(VersionConverters.getEpicsConverter().fromString("3.14.2"));
        repo.addOsVersion("SCILIN-X86_64");
        repo.addModule(
                new Module("asyn", "1.0.0", VersionConverters.getEpicsConverter().fromString("3.14.2"), "SCILIN-X86_64",
                        Lists.newArrayList(new ModuleDependency("someother", "2.0.0", true)), ImmutableList.of()));

    }

    @Test
    public void testAddModule() {
        repo.addModule(
                new Module("asyn", "1.0.0", VersionConverters.getEpicsConverter().fromString("3.14.2"), "SCILIN-X86_64",
                        Lists.newArrayList(new ModuleDependency("someother", "2.0.0", true)), ImmutableList.of()));
    }

    @Test
    public void testGetEpicsVersions() {
        final Set<Long> vers = repo.getEpicsVersions();
        assertEquals(vers.size(), 1);
        assertTrue(vers.contains(VersionConverters.getEpicsConverter().fromString("3.14.2")));
    }

    @Test
    public void testGetOsVersions() {
        final Set<String> osVers = repo.getOsVersions();
        assertEquals(osVers.size(), 1);
        assertTrue(osVers.contains("SCILIN-X86_64"));
    }

    @Test
    public void testAddOSVersion() {
        repo.addOsVersion("SCILIN-X86");
    }

    @Test
    public void testAddEPICSVersion() {
        repo.addEPICSVersion(VersionConverters.getEpicsConverter().fromString("3.14.3"));
    }
}
