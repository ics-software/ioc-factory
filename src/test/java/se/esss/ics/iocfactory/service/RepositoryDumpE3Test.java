package se.esss.ics.iocfactory.service;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import org.apache.commons.io.output.NullOutputStream;
import org.jboss.weld.junit5.EnableWeld;
import org.junit.Before;
import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.rules.TemporaryFolder;
import se.esss.ics.iocfactory.model.E3Repository;
import se.esss.ics.iocfactory.model.Repository;
import se.esss.ics.iocfactory.model.RepositoryStats;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.File;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
@EnableWeld
public class RepositoryDumpE3Test {
    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Inject
    @Named("e3RepositoryScanner")
    private RepositoryScanner<E3Repository> scannerE3;

    @Before
    public void init() throws URISyntaxException, ZipException {
        URL zipUrl = RepositoryScannerEEETest.class.getResource("/epics_e3_dir_example.zip");
        ZipFile zipFile = new ZipFile(new File(zipUrl.toURI()));
        zipFile.extractAll(folder.getRoot().getAbsolutePath());
    }

    @Test
    public void testDumpE3Repository() {
        scannerE3.setRepositoryBaseDir(folder.getRoot());
        final Repository repo = scannerE3.scanRepository();

        PrintWriter writer = new PrintWriter(new NullOutputStream());
        RepositoryStats repoStats = new RepositoryStats();
        RepositoryDump.dumpRepositoryStats(repo, repoStats);
        writer.flush();
    }
}
