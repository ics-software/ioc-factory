/*
 * Copyright (c) 2015-2016 European Spallation Source
 * Copyright (c) 2015-2016 Cosylab d.d.
 *
 * This file is part of IOC Factory.
 *
 * IOC Factory is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.iocfactory.service;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import org.jboss.weld.junit5.EnableWeld;


import org.junit.Before;
import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.rules.TemporaryFolder;
import se.esss.ics.iocfactory.model.EEERepository;
import se.esss.ics.iocfactory.model.Repository;

import javax.inject.Inject;
import javax.inject.Named;

@EnableWeld
public class RepositoryScannerEEETest {

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Inject
    @Named("eeeRepositoryScanner")
    private RepositoryScanner<EEERepository> scannerEEE;

    @Before
    public void init() throws URISyntaxException, ZipException {
        URL zipUrl = RepositoryScannerEEETest.class.getResource("/epics_base_dir_example.zip");
        ZipFile zipFile = new ZipFile(new File(zipUrl.toURI()));
        zipFile.extractAll(folder.getRoot().getAbsolutePath());
    }

    @Test
    public void testScanEEERepository() {
        scannerEEE.setRepositoryBaseDir(folder.getRoot());
        final Repository repo = scannerEEE.scanRepository();
    }
}
